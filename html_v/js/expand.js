
/*function stopEvent(e) {
	if(!e) var e = expand;
	
	//e.cancelBubble is supported by IE - this will kill the bubbling process.
	e.cancelExpand = true;
	e.returnValue = false;

	//e.stopPropagation works only in Firefox.
	if (e.stopPropagation) {
		e.stopPropagation();
		e.preventDefault();
	}
	return false;
}*/




function expandCollapseBoxes(){
	if(!document.getElementsByTagName){return;}
	var divs = document.getElementsByTagName("div");
	for (var i=0; i< divs.length; i++){
		if(divs[i].className == "expand"){
			divs[i].className ="collapse";
			
			
			//Build close link
			var closeLink = document.createElement("a");
			closeLink.className = "close-box";
			closeLink.href = "#";
			closeLink.innerHTML = "Expand";
			divs[i].parentNode.insertBefore(closeLink,divs[i]);
			
			//Create link Behavior
			closeLink.onclick =function()
			{
				var displayBox = this.parentNode.getElementsByTagName("div")[0];
					if(this.innerHTML == "Expand"){
						this.innerHTML = "Collapse";
						displayBox.className ="expand";}
					else{
						this.innerHTML = "Expand";
						displayBox.className ="collapse";
					}
			};
		}
	};
};