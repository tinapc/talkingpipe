<div class="menu">                

    <div class="breadLine">            
        <div class="arrow"></div>
        <div class="adminControl active">
            Hi, <?= $this->session->userdata('fullname') ?>
        </div>
    </div>

    <div class="admin">
        <div class="image">
            <?php if ($this->session->userdata('sess_avatar')) : ?>
                <img src="<?= base_url() . $this->session->userdata('sess_avatar') ?>" class="img-polaroid"/>                
            <?php else : ?>
                <img src="<?= themes_admin ?>img/users/aqvatarius.jpg" class="img-polaroid"/>                
            <?php endif ?>
        </div>
        <ul class="control">                
            <li><span class="icon-cog"></span> <a href="<?= URL_ADMIN ?>user/edit">Account</a></li>
            <li><span class="icon-share-alt"></span> <a href="<?= base_url() ?>admin/logout">Logout</a></li>
        </ul>
        <!-- <div class="info">
            <span>Welcom back! Your last visit: 24.10.2012 in 19:55</span>
        </div> -->
    </div>

    <ul class="navigation">
        <?php //if ($this->session->userdata('sess_group') == 'admin') : ?>
            <li class="active">
                <a href="<?php echo URL_ADMIN ?>dashboard">
                    <span class="isw-grid"></span><span class="text">Dashboard</span>
                </a>
            </li>
            <li class="openable">
                <a href="#"><span class="isw-lock"></span><span class="text">System</span></a>
                <ul>
                    <li>
                        <a href="<?php echo URL_ADMIN ?>setting">
                            <span class="icon-wrench"></span><span class="text">General</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo URL_ADMIN ?>user_system">
                            <span class="icon-user"></span><span class="text">Administrator</span>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="<?php echo URL_ADMIN ?>m_language">
                            <span class="icon-flag"></span><span class="text">Languages</span>
                        </a>
                    </li> -->
                </ul>
            </li>
            
            <li class="openable">
                <a href="#"><span class="isw-text_document"></span><span class="text">Blogs</span></a>
                <ul>
                    <li><a href="<?php echo URL_ADMIN ?>blog"><span class="icon-th"></span><span class="text">Blog manager</span></a>                  
                    </li> 
                    <li><a href="<?php echo URL_ADMIN ?>blog/add"><span class="icon-plus"></span><span class="text">Add blog</span></a></li>              
                </ul>

            </li>           
            
            <li class="openable">
                <a href="#">
                    <span class="isw-grid"></span><span class="text">Products - Categories</span>
                </a>
                <ul>
                    <li><a href="<?php echo URL_ADMIN ?>product"><span class="icon-th"></span><span class="text">Products</span></a>                  
                    </li> 
                    <li><a href="<?php echo URL_ADMIN ?>product/category"><span class="icon-plus"></span><span class="text">Category</span></a>
                    <li>
                        <a href="<?php echo URL_ADMIN ?>order">
                            <span class="icon-shopping-cart"></span><span class="text">Orders</span>
                        </a>
                    </li> 

                </ul>
            </li>
            <li class="openable">
                <a href="#">
                    <span class="isw-grid"></span><span class="text">Books</span>
                </a>
                <ul>
                    <li><a href="<?php echo URL_ADMIN ?>book"><span class="icon-th"></span><span class="text">All books</span></a>                  
                    </li> 
                    <li><a href="<?php echo URL_ADMIN ?>book/add"><span class="icon-plus"></span><span class="text">Add book</span></a>
                </ul>
            </li> 
                                   
            <li class="openable">
                <a href="#"> <span class="isw-picture"></span><span class="text">Slider</span> </a>
                <ul>
                    <li><a href="<?php echo URL_ADMIN ?>slider"><span class="icon-th"></span><span class="text">All sliders</span></a>                  
                    </li> 
                    <!-- <li><a href="<?php echo URL_ADMIN ?>advertise"><span class="icon-plus"></span><span class="text">Advertise</span></a> -->

                </ul>

            </li> 
            <li class="openable">
                <a href="#"> <span class="isw-users"></span><span class="text">Pages</span> </a>
                <ul>
                    <li><a href="<?php echo URL_ADMIN ?>page"><span class="icon-th"></span><span class="text">All pages</span></a>                  
                    </li> 
                    <li><a href="<?php echo URL_ADMIN ?>page/add"><span class="icon-plus"></span><span class="text">Add new page</span></a></li>
                    
                </ul>

            </li> 
            <li class="openable">
                <a href="#"> <span class="isw-users"></span><span class="text">Menus</span> </a>
                <ul>
                    <li><a href="<?php echo URL_ADMIN ?>menu"><span class="icon-th"></span><span class="text">All menus</span></a>                  
                    </li> 
                    <li><a href="<?php echo URL_ADMIN ?>menu/add"><span class="icon-plus"></span><span class="text">Add new menu</span></a></li>
                    
                </ul>

            </li>
            <li class="openable">
                <a href="#"> <span class="isw-users"></span><span class="text">Dynamic text</span> </a>
                <ul>
                    <li><a href="<?php echo URL_ADMIN ?>d_text/?type=header"><span class="icon-th"></span><span class="text">Text for header</span></a>                  
                    </li> 
                    <li><a href="<?php echo URL_ADMIN ?>d_text/?type=footer"><span class="icon-plus"></span><span class="text">Text for footer</span></a></li>
                    
                </ul>

            </li>
            <li class="openable">
                <a href="#"> <span class="isw-users"></span><span class="text">Tools</span> </a>
                <ul>
                    <li><a href="<?php echo base_url() ?>tool/admin/general_sitemap"><span class="icon-th"></span><span class="text">General sitemap</span></a>                  
                    </li> 
                    <!-- <li><a href="<?php echo base_url() ?>tool/admin/backup"><span class="icon-plus"></span><span class="text">Backup database</span></a></li> -->
                    
                </ul>

            </li> 
             
        <?php //endif ?>

    </ul>

    <div class="dr"><span></span></div>
    <div class="widget">
        <div class="input-append">
            <select style="width:95%; margin-left:5px;" onchange="switchL(this.value)">
                <?php foreach($this->config->item('language_support') as $key => $value) : ?>
                    <?php if($this->admin_lang == $value) :?>
                        <option value="<?=URL_ADMIN .'home/s_lang/?l='.$value.'&url='.current_url()?>" selected="selected"><?=$key?></option>
                    <?php else : ?>
                        <option value="<?=URL_ADMIN .'home/s_lang/?l='.$value.'&url='.current_url()?>"><?=$key?></option>
                    <?php endif ?>
                <?php endforeach ?>
            </select>  
        </div>
    </div>
    
    <div class="dr"><span></span></div>

</div>
<script>
    function switchL(url)
    {
        window.location.href= url;
    }
</script>