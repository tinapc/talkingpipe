<?php if($this->session->flashdata('success') != null) : ?>
    <div class="alert alert-success">                
        <h4>Success!</h4>
        <?php echo $this->session->flashdata('success')?>
    </div>
<?php endif ?>
<?php if($this->session->flashdata('error') != null) : ?>
        <div class="alert alert-error">                
            <h4>Error!</h4>
            <?php echo $this->session->flashdata('error')?>
        </div>
<?php endif ?>