<div class="breadLine">
	<?php if($this->breadcrumb != "") : ?>
	<?php $countCrumb = count($this->breadcrumb); ?>
	<?php $i = 0; ?>
    <ul class="breadcrumb">
    	<?php foreach($this->breadcrumb as $key => $value) : ?>
    		<?php $i++; ?>            
        	<?php if($i == $countCrumb) : ?>
        		<li class="active"><?=$key?></li>
        	<?php else : ?>
        		<li><a href="<?=$value?>"><?=$key?></a> <span class="divider">></span></li> 
        	<?php endif ?>
        <?php  endforeach ?>
    </ul>
	<?php endif ?>
</div>