<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    
    <title>Dead site :: Admin pannel</title>

    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="<?=themes_admin?>css/stylesheets.css" rel="stylesheet" type="text/css" />  
    <link href="<?=base_url()?>public/prettyPhoto/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 8]>
        <link href="css/ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->            
    <link rel='stylesheet' type='text/css' href='<?=themes_admin?>css/fullcalendar.print.css' media='print' />
    
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js'></script>
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/jquery/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='<?=base_url()?>public/js/jquery.form.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/excanvas.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.js'></script>    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.stack.js'></script>    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.pie.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.resize.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/sparklines/jquery.sparkline.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/fullcalendar/fullcalendar.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/select2/select2.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/uniform/uniform.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/tagsinput/jquery.tagsinput.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/validation/languages/jquery.validationEngine-en.js' charset='utf-8'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/validation/jquery.validationEngine.js' charset='utf-8'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/qtip/jquery.qtip-1.0.0-rc3.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/cleditor/jquery.cleditor.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/dataTables/jquery.dataTables.min.js'></script>    
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/fancybox/jquery.fancybox.pack.js'></script>
    <script type="text/javascript" src="<?=base_url('tinymce/jscripts/tiny_mce/tiny_mce.js');?>"></script>
    <script type='text/javascript' src='<?=themes_admin?>js/cookies.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/actions.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/charts.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/settings.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/common.js'></script>
    <script type='text/javascript' src='<?=base_url()?>public/prettyPhoto/js/jquery.prettyPhoto.js'></script>
    <script type="text/javascript">
        var baseJs = "<?=$this->config->item('index_url')?>admin/";
        var basePathTheme = '<?=base_url()?>public/';
        var statusLogin = '<?=$this->session->userdata('logined')?>';
    </script>
</head>
<body>
    <div class="wrapper"> 
            
        <?=$template['partials']['header'];?>

        <?=$template['partials']['menu']?>
        <div class="content">
            <?=$template['partials']['crumb'];?>
            <?=$template['body']?> 
        </div>
        
    </div>
</body>
</html>
