function confirm_del(url, idRemove) {
    var conf = confirm('Are you sure that You want to delete this item?');
    if (conf == true) {
        $('#loading_ajax').show();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            success: function(data) {
                if (data.status) {
                    $('#loading_ajax').hide();
                    window.location.reload();
                } else {
                    $('#loading_ajax').hide();
                }
            }
        });
    }
}

function delMulti(urlAction, classCheck) {
    var listID = $('input:checkbox:checked.' + classCheck).map(function() {
        return this.value;
    }).get();

    if (listID == '')
    {
        alert('Please select a item to delete');
    } else {

        if (confirm('Are you sure that you want to delete??'))
        {
            //alert(listID);
            $('#loading_ajax').show();
            $.ajax({
                type: "POST",
                dataType: "json",
                data: "listID=" + listID,
                url: urlAction,
                success: function(data)
                {
                    if (data.status)
                    {
                        $('#loading_ajax').hide();
                        window.location.reload();
                    } else {
                        $('#loading_ajax').hide();
                        //alert(data.message);
                        window.location.reload();
                    }
                }
            });
        }
    }
}

function removeColor(id) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {"id": id},
        url: baseJs + "product/removeTempColor",
        success: function(data) {
            if (data.status) {
                $('#colordiv_' + id).remove();
                //window.location.reload();
            } else {
                //$('#loading_ajax').hide();  
                alert(data.message);
            }
        }
    });
}

function removePhoto(id) {
    if (!statusLogin) {
        window.location.href = baseJs + "admin/login";
    } else {
        $.ajax({
            type: "POST",
            dataType: "json",
            data: {"id": id},
            url: baseJs + "product/removeTempPhoto",
            success: function(data) {
                if (data.status) {
                    $('#photodiv_' + id).remove();
                    //$(".scroll").mCustomScrollbar();
                    //window.location.reload();
                } else {
                    //$('#loading_ajax').hide();  
                    alert(data.message);
                }
            }
        });
    }

}

function loadListColors(code_con, pid) {
    $.ajax({
        type: "POST",
        dataType: "html",
        data: {"code": code_con, "pid": pid},
        url: baseJs + "product/loadListColor",
        success: function(data) {
            if (data) {
                $('#load_list_color').html(data);
            }
        }
    });
}

function loadListPhoto(code_con, pid) {
    $.ajax({
        type: "POST",
        dataType: "html",
        data: {"code": code_con, "pid": pid},
        url: baseJs + "product/loadListPhoto",
        success: function(data) {
            if (data) {
                $('#load_list_photo').html(data);
            }
        }
    });
}

