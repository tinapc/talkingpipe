<!doctype html>
<html lang="en" class="no-js">
<!--HEADER SECTION-->
<head>
<!--CHAR SET AND VIEWPORT-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial scale=1">
<!--TITLE-->
<title><?php echo $template['title']; ?></title>
<!--META TAGS-->
<?php echo $template['metadata'] ?>
<meta name="msvalidate.01" content="80593AFA22A23B834399053DA12FB74B"/>
<meta name="google-site-verification" content="iYg7OcYox5WQbsvOzyi-ji5g1fNYaMAZjkbUZqesqoo"/>
<meta name="abstract" content="21st Century, Americana, Indigenous Culture, Global Society"/>
<meta name="copyright" content="2010-2013 Talkingpipe.com, All Rights Reserved. Duplication of any content on his site is prohibited without written permission."/>
<meta name="design by" content="SNRWORX.COM | Front End Developer-Richard Dick Arnold | Back End Developer-Hung Nguyen"/>
<meta name="robots" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="distribution" content="global" />
<meta name="document-classification" content="general" />
<meta name="rating" content="general" />
<meta name="language" content="EN"/>
<meta name="no-email-collection" content="http://www.metatags.info/nospamharvesting"/>
<!--JQ & JS SLIDER LOAD-->
<script src="<?php echo theme_path?>js/jquery.min-1.8.3.js"></script>
<script src="<?php echo theme_path?>js/jquery.bxslider.min.js"></script>
<link href="<?php echo theme_path?>css/jquery.bxslider.css" rel="stylesheet" />

<!--JS LINKS-->
<script type="text/javascript" src="<?php echo theme_path?>js/modernizr.js"></script>

<!--CSS LINKS-->
<link href="<?php echo theme_path?>css/reset.css" rel="stylesheet" media="all"/>
<link href="<?php echo theme_path?>css/vert.css" rel="stylesheet" media="screen"/>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
</head><!--END HEAD-->

<!--BODY-->
<body>
<div class="wrap">
    <!--HEADER-->
    <div class="vert10" align="center">
        <header class="vert8 desktop" align="center">
            <strong style="font-size: 2.8rem;color: rgba(255,255,255,.3); text-shadow: rgba(255, 255, 255, 0.3) 0px 0px 15px, rgba(255, 255, 255, 0.5) 0px 0px 10px;font-family: Impact, Charcoal, sans-serif;"><?php echo $this->text_for_header?></strong>
        </header> <!--banner image-->
    </div>
<!--DESKTOP NAV MENU--> 
<div class="vert10 clear" align="center">
    <nav class="vert8">
        <!--MOBILE NAV MENU "TOUCH"-->
        <div id="mobile-nav-trigger" class="mobile">Touch
    </div>
        <?php get_menu(3, $this->front_lang, FALSE)?>
        <!-- <ul class="desktop">
            <li><a href="index.html">TalkingPipe</a></li>
            <li><a href="html/empire-1-art-gallery-and-studio.html">Empire 1</a></li>
            <li><a href="html/century-blogs.html">C Blogs</a></li>
            <li><a href="html/bloc-b-books.html">Bloc B</a></li>
            <li><a href="html/cart.html">CART</a></li>
            <li class="mobile"><a href="html/contact-talkingpipe.html">Contact TalkingPipe</a></li>
        </ul>  -->  
    </nav>
</div>