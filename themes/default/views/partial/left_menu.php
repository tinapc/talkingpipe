<div class="container" style="position:relative; z-index:10; background-color:#fff">
    <?php
    if ($this->_show_menu_service === TRUE) {
        widget::run('block_menu_service');
    }
    ?>

    <?php
    if ($this->_show_menu_product === TRUE) {
        widget::run('block_menu_product');
    }
    ?>

    <?php
    if ($this->_show_menu_news === TRUE) {
        widget::run('block_menu_news');
    }
    ?>

    <?php
    if ($this->_show_menu_guide === TRUE) {
        widget::run('block_page', array('haha' => $this->type_page));
    }
    ?>

    <?php
    if ($this->_show_menu_relax === TRUE) {
        widget::run('block_menu_relax');
    }
    ?>

    <?php if ($this->_show_menu_tool === TRUE) : ?>

        <?php
        $guides = $this->db->get_where('pages', array('type' => 'guide', 'status' => 1))->result();
        $policys = $this->db->get_where('pages', array('type' => 'policy', 'status' => 1))->result();
        $coms = $this->db->get_where('pages', array('type' => 'company', 'status' => 1))->result();
        ?>

        <div class="span2" style="margin:0px">
            <h5 class="btn-block title_home" id="head_menu" style="position:relative;">
                <a href="<?= $this->fullPath ?>product">REACH Us <i class="icon-angle-down"></i></a>
            </h5>

            <div class="left-col submenu" id="submenu">         
                <ul class="nav nav-pills nav-stacked menu-product">
                    <li><a href="#" style="font-size:12px"><i class="icon-angle-right"></i> Company</a>
                        <?php if (count($coms)) : ?>
                            <ul id="submenupro">
                                <?php foreach ($coms as $p) : ?>
                                    <?php if ($p->link !== NULL) : ?>
                                        <li><a href="<?= $p->link ?>"><?= $p->title ?></a></li>
                                    <?php else : ?>
                                        <li><a href="<?= $this->fullPath ?>page/<?= $p->id ?>-<?= $p->alias ?>"><?= $p->title ?></a></li>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </ul>
                        <?php endif ?>
                    </li>
                    <li><a href="#" style="font-size:12px"><i class="icon-angle-right"></i> Chính sách</a>
                        <?php if (count($policys)) : ?>
                            <ul id="submenupro">
                                <?php foreach ($policys as $p) : ?>
                                    <li><a href="<?= $this->fullPath ?>page/<?= $p->id ?>-<?= $p->alias ?>"><?= $p->title ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        <?php endif ?>
                    </li>
                    <li><a href="#" style="font-size:12px"><i class="icon-angle-right"></i> Hướng dẫn</a>
                        <?php if (count($guides)) : ?>
                            <ul id="submenupro">
                                <?php foreach ($guides as $p) : ?>
                                    <li><a href="<?= $this->fullPath ?>page/<?= $p->id ?>-<?= $p->alias ?>"><?= $p->title ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        <?php endif ?>
                    </li>
                    <li><a href="#" style="font-size:12px"><i class="icon-angle-right"></i> Follow us</a>
                        <ul id="submenupro">
                            <li><a href="<?= $this->setting->get_setting('page_google') ?>" target="_blank">Google+</a></li>
                            <li><a href="<?= $this->setting->get_setting('page_fb') ?>" target="_blank">Facebook</a></li>
                            <li><a href="<?= $this->setting->get_setting('page_tw') ?>" target="_blank">Twitter</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>


        <?php
    
     endif ?>