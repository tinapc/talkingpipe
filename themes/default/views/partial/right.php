<div class="span3" style="padding-top:6px">
    <div class="left-col margin-bottom" style="padding:10px 0px; border:none; box-shadow:none">
        <!-- <p style="text-transform: uppercase;" class="title_home">đăng ký nhận tin khuyến mãi</p> -->
        <div class="title-menu-right">Nhận tin <span>khuyến mãi</span></div>
        <div class="c-menu">
            <em>Hãy đắng ký ngay bây giờ để nhận ưu đãi sớm nhất</em>
            <p id="status_reg" style="color:#ff0000"></p>
            <input type="text" id="reg_email" class="form-control span7" style="width:95%" placeholder="Email của bạn">
            <button class="btn btn-default" style="box-shadow:0 0 3px #888; border-radius:5px; color:#fff; background:#ff0000" type="button" onclick="reg_receive_news()"><i class="icon-refresh icon-spin" style="display: none;" id="show_load"></i> Đăng ký</button>
        </div>
        
        
    </div>

    <?php 
        if($this->session->userdata('logged_status'))  {
            $url = $this->setting->get_setting('ebook');
            $book_name = $this->setting->get_setting('book_name');
            $book_image = $this->setting->get_setting('book_image');
        } else {
            $book_name = $this->setting->get_setting('book_name');
            $book_image = $this->setting->get_setting('book_image');
            $url = $this->fullPath . 'login';
        }
    ?>

    <div class="left-col margin-bottom" style="padding:10px 0px; border:none; box-shadow:none">
        <!-- <p style="text-transform: uppercase;" class="title_home">đăng ký nhận tin khuyến mãi</p> -->
        <div class="title-menu-right">Tải Ebook <span>Free</span></div>
        <div class="c-menu">
            <?php if($this->session->userdata('logged_status')) : ?>
            <em>Sách hay nên đọc hiện nay:</em><br>
            <p class="name-book"><em><?=$book_name?></em></p>
            <img src="<?=base_url() . $book_image?>" alt="Download book">
            <a href="<?=$url?>" class="btn btn-default" style="box-shadow:0 0 3px #888; border-radius:5px; color:#fff; background:#ff0000">Download Now</a>
            <?php else : ?>
                <em>Sách hay nên đọc hiện nay:</em><br>
                <p class="name-book"><em><?=$book_name?></em></p>
                <img src="<?=base_url() . $book_image?>" alt="Download book">
                <a href="<?=base_url()?>login" class="btn btn-default" style="box-shadow:0 0 3px #888; border-radius:5px; color:#fff; background:#ff0000">Đăng nhập để download</a>
            <?php endif ?>
            
        </div>
    </div>


    <?php if ($this->_show_banner_right == TRUE) : ?>
        <?php widget::run('block_banner_right') ?>
    <?php endif ?>

    <?php if ($this->_show_top_news == TRUE) : ?>
        <?php widget::run('block_news_right') ?>
    <?php endif ?>
    <?php if ($this->_show_top_service == TRUE) : ?>
        <?php widget::run('block_service_right') ?>
    <?php endif ?>
    <?php if ($this->_show_top_product == TRUE) : ?>
        <?php widget::run('block_product_right') ?>
    <?php endif ?>
    
    <div class="left-col margin-bottom" style="background-color:#fff">
        <iframe src="//www.facebook.com/plugins/likebox.php?href=<?= $this->setting->get_setting('page_like_box') ?>&amp;width=270&amp;height=590&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=true&amp;show_border=true&amp;appId=309587472506780" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:590px;" allowTransparency="true"></iframe>     
    </div>
</div>
</div>