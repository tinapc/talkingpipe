    <!--lowNav  | Terms DMCA Privacy Espanol-->
    <div id="lowNav" class="desktop vert10  clear" align="center">
        <div class="vert8">
            <!-- <ul class="desktop">
                <li><a href="html/terms-of-use.html">Terms</a></li>
                <li><a href="html/dmca.html">DMCA </a></li>
                <li><a href="html/privacy.html">Privacy</a></li>
                <li><a href="html/contact-talkingpipe.html">Contact</a></li>
                <li><a href="html/spanish-talkingpipe.html">Espanol</a></li>
            </ul> -->
            <?php get_menu(4, $this->front_lang, TRUE)?>
        </div>  
    </div>


    <div id="MobLowNav" class="mobile vert10 street060">
        <!-- <ul>
            <li><a href="html/terms-of-use.html">Terms</a></li>
            <li><a href="html/dmca.html">&copy</a></li>
            <li><a href="html/privacy.html">Privacy</a></li>
            <li><a href="html/contact-talkingpipe.html">Contact</a></li>
            <li><a href="html/spanish-talkingpipe.html">Espanol</a></li>
        </ul> -->
        <?php get_menu_on_mobile(4, $this->front_lang, TRUE)?>
    </div>  
</div><!--END WRAPPER-->

<!--FOOTER SECTION-->
<div class="desktop vert10 street060 clear" align="center">
    <footer class="vert8 desktop">
        <div class="vert8 clear">
            <?php echo $this->text_for_footer?>
        </div>
    </footer>
</div>
<script src="<?php echo theme_path?>js/main.js"></script>
<script type="text/javascript" src="<?php echo theme_path?>js/slider.js"></script>
</body>
</html>