<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//button text
$lang['btn_del'] = 'Xóa bản tin đã chọn';
$lang['btn_published'] = 'Published';
$lang['btn_create_new'] = 'Tạo mới';
$lang['btn_cancel'] = 'Quay lại';
$lang['btn_save'] = 'Lưu lại';
$lang['btn_del_del'] = 'Xóa';
$lang['btn_close'] = 'Đóng lại';
$lang['action'] = 'Hành động';
$lang['order'] = 'Sắp xếp';
$lang['status'] = 'Kích hoạt';
$lang['language'] = 'Ngôn ngữ';
$lang['btn_upload_pic'] = 'Upload hình';

//For menu
$lang['create_new'] = 'Tạo mới bản tin';
$lang['edit_item'] = 'Chỉnh sửa bản tin';
$lang['create_article'] = 'Tạo mới bài viết';
$lang['manager_user'] = 'Người dùng và Phân quyền';

//General
$lang['setting'] = 'Cấu hình chung';
$lang['support'] = 'Hổ trợ online';
$lang['gallery'] = 'Hình ảnh';
$lang['welcome'] = 'Chào';
$lang['what_you_like_todo'] = 'Bạn muốn làm cái gì?';
$lang['service'] = 'Dịch vụ';
$lang['sign_out'] = 'Thoát';
$lang['create_album'] = 'Tạo mới album';
$lang['active'] = 'Hiển thị';
$lang['unactive'] = 'Không hiển thị';
$lang['please_choose_a_item'] = 'Vui lòng chọn ít nhất một bản tin để xóa';
$lang['confirm_delete'] = 'Bạn chắc rằng muốn xóa những bản tin này?';
$lang['loading'] = 'Đang xử lý dử liệu...';
$lang['image'] = 'Hình ảnh';
$lang['note_upload_size'] = 'Chọn file của bạn để upload và dung lượng không quá 2Mb';
$lang['yes'] = 'Có';
$lang['no'] = ' Không';
$lang['short_desc'] = 'Mô tả ngắn';
$lang['long_desc'] = 'Chi tiết bài viết';
$lang['description'] = 'Mô tả';
$lang['address_company'] = 'Địa chỉ công ty';
$lang['email_company'] = 'Địa chỉ Email công ty';


//Message process
$lang['add_success'] = 'Thêm mới bản tin thành công';
$lang['add_faild'] = 'Không thể thêm mới bản tin';
$lang['update_success'] = 'Cập nhật thông tin thành công';
$lang['update_faild'] = 'Cập nhật không thành công';
$lang['delete_success'] = 'Xóa bản tin thành công';
$lang['delete_faild'] = 'Không thể xóa bản tin';
$lang['denied_access'] = 'Bạn không có quyền thao tác chức năng này';


//Message for login
$lang['pass_invalid'] = 'The password is wrong';
$lang['acc_not_active'] = 'Your account is not already to active';
$lang['acc_not_exists'] = 'The username is not valid';
$lang['user_pass_required'] = 'Tài khoản hoặc mật khẩu không được để trống';
$lang['logout_success'] = 'Thoát khỏi phiên làm việc thành công';

//For page article
$lang['title'] = 'Tiêu đề bài viết';
$lang['manager_article'] = 'Quản lý bài viết';
$lang['title_article'] = 'Tiêu đề bài viết';
$lang['title_seo'] = 'Tiêu đề dùng để tạo link đẹp';

//For support
$lang['group_support'] = 'Nhóm hổ trợ';
$lang['create_support'] = 'Tạo mới thông tin';
$lang['create_group'] = 'Tọa mới nhóm';
$lang['manager_nick'] = 'Danh sách thông tin';
$lang['group_support_name'] = 'Tên nhóm';
$lang['position_support'] = 'Vị trí hổ trợ';

//For language
$lang['m_language'] = 'Danh sách ngôn ngữ';
$lang['create_language'] = 'Thêm mới ngôn ngữ';
$lang['edit_global_lang'] = 'Sửa biến ngôn ngữ';
$lang['language_name'] = 'Tên ngôn ngữ';
$lang['language_code'] = 'Mã ngôn ngữ';

//For page setting
$lang['mail_type'] = 'Kiểu gởi mail';
$lang['site_name'] = 'Site name';
$lang['address_receive_email'] = 'Địa chỉ nhận mail';
$lang['default_language'] = 'Ngôn ngữ mặc định của website';
$lang['turn_on_email_secury'] = 'Bật captcha khi gởi email';

//For page User
$lang['users'] = 'Người dùng';
$lang['permission'] = 'Phân quyền';
$lang['user_name'] = 'Tên đăng nhập';
$lang['group_name'] = 'Nhóm quyền';
$lang['full_name'] = 'Tên đầy đủ';
$lang['user_email'] = 'Địa chỉ Email';
$lang['permission_view'] = 'Quyền xem';
$lang['permission_edit'] = 'Quyền xóa sửa';

//For page album
$lang['m_album'] = 'Danh sách Album';
$lang['album_name'] = 'Tên Album';
$lang['select_album'] = 'Chọn Album cần upload hình';
$lang['add_more_image'] = 'Thêm hình';
