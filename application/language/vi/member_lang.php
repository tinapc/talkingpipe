<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

$lang['lb_cty'] = 'Tỉnh/Thành phố';
$lang['lb_email'] = 'Địa chỉ Email';
$lang['lb_password'] = 'Mật khẩu';
$lang['lb_repassword'] = 'Xác nhận mật khẩu';
$lang['lb_fullname'] = 'Tên đầy đủ';
$lang['lb_phone'] = 'Điện thoại';
$lang['lb_address'] = 'Địa chỉ';
$lang['title_register_page'] = 'Đăng ký thành viên';
$lang['title_login_page'] = 'Đăng nhập';
$lang['title_active_page'] = 'Kích hoạt tài khoản';

$lang['active_success'] = 'Tài khoản của bạn đã được kích hoạt';
$lang['active_error'] = 'Bạn đã có lỗi trong quá trình thực hiện chức năng này';