<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['global:btn_submit'] = 'Submetu';


//General
$lang['global:contact_name'] = 'Nomo: (deviga)';
$lang['global:contact_email'] = 'Retpoŝto: (deviga)';
$lang['global:contact_message'] = 'Mesaĝo: (deviga)';
$lang['global:contact_btn_submit'] = 'Sendu retpoŝton';
$lang['contact:success'] = '<h4>Dankon por kontakti nin, ni respondos al via mesaĝo baldaŭ.</h4>';
$lang['contact:error'] = '<h4>Vi havas eraro</h4>
			<p>Via kontakto malsukcesis. Bonvolu reprovi</p>';


//Cart
$lang['cart:success'] = '<h4>Dankon pro via mendo.</h4>
		<p>Via ordo sxipo en 24hrs. Se via mendo postulas
postulas specialajn uzado ni kontaktos vin per telefono aŭ retpoŝto informi de shipping tempo. Ni estos ankaŭ kontakti vin, se internacia
shipping kotizoj apliki.</p>';
$lang['cart:continue_shopping'] = 'Daŭrigu komercaj veturilon';
$lang['cart:btn_buynow'] = 'Acxetu nun';
$lang['cart:btn_addtocart'] = 'Aldoni al veturilon';
$lang['cart:btn_viewcart'] = 'Montru veturilon';
$lang['cart:btn_freedownload'] = 'Senpaga Cifereca elŝuti!';
$lang['cart:yourshoppingcart'] = 'Via Korbo';
$lang['cart:text_image'] = 'Dosiero';
$lang['cart:text_productname'] = 'Produkto nomo';
$lang['cart:text_qty'] = 'Qty';
$lang['cart:text_price'] = 'Prezo';
$lang['cart:text_subtotal'] = 'Subtotal';
$lang['cart:text_delete'] = 'Forigi';
$lang['cart:text_total'] = 'Sumo';
$lang['cart:text_action'] = 'Agado';
$lang['cart:text_ship_fee'] = 'ŝipo kotizo';
$lang['cart:text_ship_include'] = 'Hejma Shipping Included';
$lang['cart:cart_is_empty'] = 'Via Korbo estas malplena';


$lang['cart:checkout'] = 'Checkout paŝo 1/2';
$lang['cart:billinginfo'] = 'Billing info';
$lang['cart:billtofirstname'] = 'Antaŭnomo';
$lang['cart:billtolastname'] = 'Familinomo';
$lang['cart:billtoaddress'] = 'Adreso';
$lang['cart:billtocity'] = 'Urbo';
$lang['cart:billtozipcode'] = 'Afiŝu kodo';
$lang['cart:billtocountry'] = 'Lando';
$lang['cart:billtostate'] = 'ŝtato';
$lang['cart:billtoemail'] = 'retpoŝto';
$lang['cart:billtophone'] = 'telefono';



