<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//button text
$lang['btn_del'] = 'Delete all';
$lang['btn_published'] = 'Published';
$lang['btn_create_new'] = 'Create new';
$lang['btn_cancel'] = 'Back';
$lang['btn_save'] = 'Save';
$lang['btn_del_del'] = 'Delete';
$lang['btn_close'] = 'Close';
$lang['action'] = 'Action';
$lang['order'] = 'Order';
$lang['status'] = 'Active';
$lang['language'] = 'Language';
$lang['btn_upload_pic'] = 'Upload image';

//For menu
$lang['create_new'] = 'Add new';
$lang['edit_item'] = 'Edit';
$lang['create_article'] = 'Add new';
$lang['manager_user'] = 'User and Permission';

//General
$lang['setting'] = 'Configuaration general';
$lang['support'] = 'Support online';
$lang['gallery'] = 'Gallery';
$lang['welcome'] = 'Welcome';
$lang['what_you_like_todo'] = 'What do you want to do?';
$lang['service'] = 'Dịch vụ';
$lang['sign_out'] = 'Logout';
$lang['create_album'] = 'Tạo mới album';
$lang['active'] = 'Published';
$lang['unactive'] = 'UnPublished';
$lang['please_choose_a_item'] = 'Please select an item';
$lang['confirm_delete'] = 'Are you sure that you want to delete this item?';
$lang['loading'] = 'Processing...';
$lang['image'] = 'Image';
$lang['note_upload_size'] = 'Please choose your file to upload';
$lang['yes'] = 'Yes';
$lang['no'] = ' No';
$lang['short_desc'] = 'Short description';
$lang['long_desc'] = 'Long description';
$lang['description'] = 'Description';
$lang['address_company'] = 'Address for company';
$lang['email_company'] = 'Email address for company';


//Message process
$lang['add_success'] = 'You has been created new item successfuly';
$lang['add_faild'] = 'Can not creat new item';
$lang['update_success'] = 'You has been updated item successfuly';
$lang['update_faild'] = 'Can not update item ';
$lang['delete_success'] = 'You has been deleted item successfuly';
$lang['delete_faild'] = 'Can not delete this item';
$lang['denied_access'] = 'You dont have access to this function';


//Message for login
$lang['pass_invalid'] = 'Wrong password';
$lang['acc_not_active'] = 'Your account is not already active';
$lang['acc_not_exists'] = 'Your account is not exists';
$lang['user_pass_required'] = 'Username or Password is not empty';
$lang['logout_success'] = 'You had just logout successful';

//For page article
$lang['title'] = 'Title';
$lang['manager_article'] = 'Manager news';
$lang['title_article'] = 'Title';
$lang['title_seo'] = 'Tiêu đề dùng để tạo link đẹp';

//For support
$lang['group_support'] = 'Nhóm hổ trợ';
$lang['create_support'] = 'Add new';
$lang['create_group'] = 'Tọa mới nhóm';
$lang['manager_nick'] = 'Manager nick support';
$lang['group_support_name'] = 'Tên nhóm';
$lang['position_support'] = 'Position';

//For language
$lang['m_language'] = 'Danh sách ngôn ngữ';
$lang['create_language'] = 'Thêm mới ngôn ngữ';
$lang['edit_global_lang'] = 'Sửa biến ngôn ngữ';
$lang['language_name'] = 'Tên ngôn ngữ';
$lang['language_code'] = 'Mã ngôn ngữ';

//For page setting
$lang['mail_type'] = 'Mail type';
$lang['site_name'] = 'Site name';
$lang['address_receive_email'] = 'Email address to received';
$lang['default_language'] = 'Ngôn ngữ mặc định của website';
$lang['turn_on_email_secury'] = 'Bật captcha khi gởi email';

//For page User
$lang['users'] = 'User';
$lang['permission'] = 'Permission';
$lang['user_name'] = 'Username';
$lang['group_name'] = 'Group permisson';
$lang['full_name'] = 'Fullname';
$lang['user_email'] = 'Email';
$lang['permission_view'] = 'View permisson';
$lang['permission_edit'] = 'Modify permisson';

//For page album
$lang['m_album'] = 'Danh sách Album';
$lang['album_name'] = 'Tên Album';
$lang['select_album'] = 'Chọn Album cần upload hình';
$lang['add_more_image'] = 'Add more image';
