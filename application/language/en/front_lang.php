<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$lang['global:btn_submit'] = 'Submit';

//Contact page
$lang['global:contact_name'] = 'Name: (required)';
$lang['global:contact_email'] = 'Email: (required)';
$lang['global:contact_message'] = 'Message: (required)';
$lang['global:contact_btn_submit'] = 'Send email';
$lang['contact:success'] = '<h4>Thank you for contacting us, we will respond to your message shortly.</h4>';
$lang['contact:error'] = '<h4>You have a error</h4>
			<p>Your contact have failed. Please try again</p>';



//Cart
$lang['cart:success'] = '<h4>Thank you for your order.</h4>
		<p>Your order will ship in 24hrs. If your order requires 
requires special handling we will contact you by phone or email to inform of shipping time. We will also contact you if international 
shipping fees apply.</p>';
$lang['cart:continue_shopping'] = 'Continue shopping';
$lang['cart:btn_buynow'] = 'Buy now';
$lang['cart:btn_addtocart'] = 'Add to cart';
$lang['cart:btn_viewcart'] = 'View cart';
$lang['cart:btn_freedownload'] = 'Free Digital DownLoad!';
$lang['cart:yourshoppingcart'] = 'Your shopping cart';
$lang['cart:text_image'] = 'Image';
$lang['cart:text_productname'] = 'Product name';
$lang['cart:text_qty'] = 'Qty';
$lang['cart:text_price'] = 'Price';
$lang['cart:text_subtotal'] = 'Subtotal';
$lang['cart:text_delete'] = 'Delete';
$lang['cart:text_total'] = 'Total';
$lang['cart:text_action'] = 'Action';
$lang['cart:text_ship_fee'] = 'Ship fee';
$lang['cart:text_ship_include'] = 'Domestic Shipping Included';
$lang['cart:cart_is_empty'] = 'Your shopping cart is empty';


$lang['cart:checkout'] = 'Checkout step 1/2';
$lang['cart:billinginfo'] = 'Billing info';
$lang['cart:billtofirstname'] = 'First name';
$lang['cart:billtolastname'] = 'Last name';
$lang['cart:billtoaddress'] = 'Address';
$lang['cart:billtocity'] = 'City';
$lang['cart:billtozipcode'] = 'Post code';
$lang['cart:billtocountry'] = 'Country';
$lang['cart:billtostate'] = 'State';
$lang['cart:billtoemail'] = 'Email';
$lang['cart:billtophone'] = 'Phone';
