<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Region_Model
 * Extends CI_Model
 * Author: Nguyen Duc Hung
 * Email: tinapc@gmail.com
 * */
class Region_Model extends CI_Model {

    var $_tbl = '';
    var $_subRegion = '';

    public function __construct() {
        parent:: __construct();
        $this->_tbl = 'regions';
        $this->_subRegion = 'subregions';
    }

    public function getRegion($where = null) {
        if ($where !== null) {
            foreach ($where as $key => $val) {
                $this->db->where($key, $val);
            }
        }

        $query = $this->db->get($this->_tbl);

        return $query->result_array();
        $query->free_result();
    }

    public function getSubRegion($parentId) {
        $sql = $this->db->select('id, name')->get_where($this->_subRegion, array('region_id' => $parentId));
        if ($sql->num_rows() > 0) {
            return $sql->result_array();
        } else {
            return NULL;
        }
    }

    public function getNameRegion($id) {
        $sql = $this->db->select('name')->get_where($this->_subRegion, array('id' => $id));
        if ($sql->num_rows() > 0) {
            $re = $sql->row();
            return $re->name;
        } else {
            return NULL;
        }
    }

    public function getIsoRegion($id) {
        $sql = $this->db->select('iso')->get_where('regions', array('id' => $id));
        if ($sql->num_rows() > 0) {
            $re = $sql->row();
            return $re->iso;
        } else {
            return NULL;
        }
    }


}
