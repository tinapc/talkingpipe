<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Text_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function get_text($page, $type, $lang_code){
		$this->db->select('text_for_page_language.text, text_for_page.id');
		$this->db->where(array('text_for_page.page' => $page, 'text_for_page.type' => $type, 'text_for_page_language.lang_code' => $lang_code));
		$this->db->join('text_for_page_language', 'text_for_page.id = text_for_page_language.text_id', 'left');
		$info = $this->db->get('text_for_page');
		if($info->num_rows() > 0 ) {
			$re = $info->row();
			return $re->text;
		} else {
			return '';
		}
	}

}

/* End of file text_model.php */
/* Location: ./application/models/text/text_model.php */