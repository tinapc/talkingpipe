<?php

class Setting_model extends CI_Model
{
	function __construct()
	{
		parent:: __construct();
	}
	
	function get_setting($key)
	{
		$this->db->select('value');
		$this->db->where('key', $key);
		$sql = $this->db->get('config_site');
		$sql = $sql->row();
		return $sql->value;
	}
}
