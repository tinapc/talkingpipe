<?php 
class Advertise_Model extends CI_Model {
	var $_cate = '';
	public function __construct()
	{
		parent::__construct();
		$this->table = 'banner';
	}


	public function add($data) {
		if(!empty($_FILES['photo']['name']))
	            {
	                    $config['upload_path'] = './uploads/banner/';
	                    $config['allowed_types'] = 'gif|jpg|png';
	                    $config['encrypt_name'] = TRUE;

	                    $this->load->library('upload', $config);

	                    if ( ! $this->upload->do_upload('photo'))
	                    {
	                        //echo $this->upload->display_errors();
	                        $image = null;
	                        $file_ext = "";
	                    }
	                    else
	                    {
	                        $up = $this->upload->data();
	                        $image = 'uploads/banner/' . $up['file_name'];
	                        $file_ext = $up['file_ext'];
	                        // $imageWidth = $up['image_height'];
	                        // if($imageWidth > 700)
	                        // {
	                        //     $this->load->library('image_lib');
	                        //     $config['image_library'] = 'GD2';
	                        //     $config['source_image'] = $up['full_path'];
	                        //     $config['quality'] = 75;
	                        //     $config['maintain_ratio'] = TRUE;
	                        //     $config['master_dim'] = 'height';
	                        //     $config['width'] = 700;
	                        //     $config['height'] = 700;

	                        //     $this->image_lib->initialize($config);
	                        //     $this->image_lib->resize();
	                        // }

	                    }
	            } else {
	                $image = null;
	            }  
				
				$arr = array(
					'link'	=> $this->input->post('link'),
					'desc'	=> $this->input->post('desc'),
					'status'		=> (int)$this->input->post('status'),
					'photo'	=> $image,
					'added_date' => time(),
					'order'	=> (int)$this->input->post('order'),
					'file_ext' => $file_ext,
					'position'	=> $this->input->post('position')
				);
		if($this->db->insert($this->table, $arr)) return TRUE; else return FALSE;
	}


	public function edit($data, $id){
        $oldPhoto = $this->input->post('oldPhoto');

        $arr = array(
					'link'	=> $this->input->post('link'),
					'desc'	=> $this->input->post('desc'),
					'status'		=> (int)$this->input->post('status'),
					'photo'	=> $image,
					'added_date' => time(),
					'order'	=> (int)$this->input->post('order'),
					'file_ext' => $file_ext,
					'position'	=> $this->input->post('position')
				);

        if(!empty($_FILES['photo']['name']))
        {
            $config['upload_path'] = './uploads/banner/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo'))
            {
                //echo $this->upload->display_errors();
                $image = $oldPhoto;
            } else {
                $up = $this->upload->data();
                $image = 'uploads/banner/' . $up['file_name'];
                if($oldPhoto !=='') @unlink($oldPhoto);

                // $imageWidth = $up['image_width'];
                // if($imageWidth > 140)
                // {
                //     $this->load->library('image_lib');
                //     $config['image_library'] = 'GD2';
                //     $config['source_image'] = $up['full_path'];
                //     $config['quality'] = 75;
                //     $config['maintain_ratio'] = TRUE;
                //     $config['master_dim'] = 'height';
                //     $config['width'] = 140;
                //     $config['height'] = 140;

                //     $this->image_lib->initialize($config);
                //     $this->image_lib->resize();
                // }
            }
        } else {
            $image = $oldPhoto;
        }

        $arr['photo'] = $image;

        if($this->db->update($this->table, $arr, array('id' => $id))) return TRUE; else return FALSE;
    }

	public function getList($field = null, $where=null, $order=null, $limit=null) {
		if($field !== null) {
			foreach($field as $key) {
				$this->db->select($key);
			}	
		}
		if($where !== null) {
			foreach($where as $key => $val) {
				$this->db->where($key, $val);
			}
		}
		if($order !== null) {
			foreach($order as $key => $val) {
				$this->db->order_by($key, $val);
			}
		}

		if($limit !== null) {
			$this->db->limit($limit['max'], $limit['begin']);
		}

		$query = $this->db->get($this->table);

		return $query->result_array();

		$query->free_result();
	}

	public function delete($id) {
		$getImg = $this->db->select('photo')->get_where($this->table, array('id' => $id))->row();
		if($getImg->photo !=='') @unlink($getImg->photo);

		if($this->db->delete($this->table, array('id' => $id))) return TRUE; else return FALSE;
	}

	public function read($id) {
        $query = $this->db->get_where($this->table, array('id' => $id))->row_array();
        return $query;
    }
}