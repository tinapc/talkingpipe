<?php

class Product_Model extends CI_Model {

    var $_cate = '';
    var $tbl_color;
    var $tbl_photo;
    var $tbl_size;
    var $tbl_product;

    public function __construct() {
        parent::__construct();
        $this->_cate = 'service_category';
        $this->tbl_color = 'product_color';
        $this->tbl_size = 'product_size';
        $this->tbl_photo = 'product_photo';
        $this->tbl_product = 'product';
    }

    /* ////////////////////FOR CATEGORY NEWS ///////////////////////////* */

    public function add_cate($data) {
        $arr = array(
            //'title' => $this->input->post('title'),
            'alias' => $this->util->alias($this->input->post('title')),
            'order' => (int) $this->input->post('order'),
            'status' => (int) $this->input->post('status'),
        );

        if (!empty($_FILES['photo']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo')) {
                $photo = NULL;
            } else {
                $up = $this -> upload -> data();
                $photo = 'uploads/product/' . $up['file_name'];
                $imageWidth = $up['image_width'];
                // if ($imageWidth > 200) {
                //     $this -> load -> library('image_lib');
                //     $config['image_library'] = 'GD2';
                //     $config['source_image'] = $up['full_path'];
                //     $config['quality'] = 75;
                //     $config['maintain_ratio'] = TRUE;
                //     $config['master_dim'] = 'auto';
                //     $config['width'] = 200;
                //     $config['height'] = 250;

                //     $this -> image_lib -> initialize($config);
                //     $this -> image_lib -> resize();
                // }
                //@unlink($oldLogo);
            }
        } else {
            $photo = NULL;
        }

        $arr['photo'] = $photo;

        if ($this->db->insert('catelog', $arr)) {
            $cid = $this->db->insert_id();

            foreach($this->config->item('language_support') as $key => $value) {
                $info = array(
                    'catelog_id'    => $cid,
                    'catelog_name'  => $this->input->post('title'),
                    'catelog_desc'  => $this->input->post('desc'),
                    'lang_code'     => $value
                );

                $this->db->insert('catelog_language', $info);
                //return $this->db->affected_rows();
            }
        } else {
            return FALSE;
        }
    }

    public function edit_cate($data, $id) {
        $arr = array(
            //'title' => $this->input->post('title'),
            'alias' => $this->util->alias($this->input->post('title')),
            'order' => (int) $this->input->post('order'),
            'status' => (int) $this->input->post('status'),
        );

        $oldPhoto = $this->input->post('oldPhoto');

        if (!empty($_FILES['photo']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo')) {
                $photo = $oldPhoto;
            } else {
                $up = $this -> upload -> data();
                $photo = 'uploads/product/' . $up['file_name'];
                $imageWidth = $up['image_width'];
                // if ($imageWidth > 200) {
                //     $this -> load -> library('image_lib');
                //     $config['image_library'] = 'GD2';
                //     $config['source_image'] = $up['full_path'];
                //     $config['quality'] = 75;
                //     $config['maintain_ratio'] = TRUE;
                //     $config['master_dim'] = 'auto';
                //     $config['width'] = 200;
                //     $config['height'] = 250;

                //     $this -> image_lib -> initialize($config);
                //     $this -> image_lib -> resize();
                // }
                @unlink($oldPhoto);
            }
        } else {
            $photo = $oldPhoto;
        }

        $arr['photo'] = $photo;

        if ($this->db->update('catelog', $arr, array('id' => $id))) {
                $info = array(
                    'catelog_name'  => $this->input->post('title'),
                    'catelog_desc'  => $this->input->post('desc'),
                );

                $this->db->update('catelog_language', $info, array('catelog_id' => $id, 'lang_code' => $this->admin_lang));
        } else {
            return FALSE;
        }
    }

    public function getListCate($field = null, $where = null, $order = null, $limit = null) {
        if ($field !== null) {
            foreach ($field as $key) {
                $this->db->select($key);
            }
        }
        if ($where !== null) {
            foreach ($where as $key => $val) {
                $this->db->where($key, $val);
            }
        }
        if ($order !== null) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($limit !== null) {
            $this->db->limit($limit['max'], $limit['begin']);
        }

        $query = $this->db->get($this->_cate);

        return $query->result_array();

        $query->free_result();
    }

    public function checkCategory($title) {
        $sql = $this->db->select('*')->get_where('catelog_language', array('catelog_name' => $title));
        if ($sql->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function read_category($id) {
        $this->db->where(array('catelog_language.lang_code'=> $this->admin_lang, 'catelog.id' => $id));
        $this->db->join('catelog_language', 'catelog.id = catelog_language.catelog_id');
        $query = $this->db->get('catelog')->row_array();
        return $query;
    }

    public function delete_cate($id) {
        $getImg = $this->db->select('photo')->get_where('catelog', array('id' => $id))->row();
        if($getImg->photo !== NULL) @unlink($getImg->photo);
        $this->db->delete('catelog_language', array('catelog_id' => $id));
        if ($this->db->delete('catelog', array('id' => $id)))
            return TRUE;
        else
            return FALSE;
    }

    public function getAttributeCategory($attribute = 'title', $id) {
        $this->db->select('catelog_language.catelog_name');
        $this->db->where('catelog.id', $id);
        $this->db->join('catelog_language', 'catelog_language.catelog_id = catelog.id', 'left');
        $re = $this->db->get('catelog')->row();
        return $re->catelog_name;
    }

    /*     * *///////////////////////////////////////////////
    //////////////////// FOR PRODUCT //////////////////
    //////////////////////////////////////////////////*/

    public function add($data) {
        $arr = array(
            'title' => $this->input->post('title'),
            'slug' => $this->util->alias($this->input->post('title')),
            'price' => $this->input->post('price'),
            'cate_id' => (int) $this->input->post('cate_id'),
        );

        if (!empty($_FILES['photo']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo')) {
                $photo = NULL;
            } else {
                $up = $this -> upload -> data();
                $photo = 'uploads/product/' . $up['file_name'];
                $imageWidth = $up['image_width'];
                // if ($imageWidth > 300) {
                //     $this -> load -> library('image_lib');
                //     $config['image_library'] = 'GD2';
                //     $config['source_image'] = $up['full_path'];
                //     $config['quality'] = 75;
                //     $config['maintain_ratio'] = TRUE;
                //     $config['master_dim'] = 'auto';
                //     $config['width'] = 300;
                //     $config['height'] = 500;

                //     $this -> image_lib -> initialize($config);
                //     $this -> image_lib -> resize();
                // }
                //@unlink($oldLogo);
            }
        } else {
            $photo = NULL;
        }

        if (!empty($_FILES['photo_big']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo_big')) {
                $photo_big = NULL;
            } else {
                $up = $this -> upload -> data();
                $photo_big = 'uploads/product/' . $up['file_name'];
                
                //@unlink($oldLogo);
            }
        } else {
            $photo_big = NULL;
        }

        $arr['photo'] = $photo;
        $arr['big_photo'] = $photo_big;

        if ($this->db->insert($this->tbl_product, $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function edit($data, $id) {
        $arr = array(
            'title' => $this->input->post('title'),
            'slug' => $this->util->alias($this->input->post('title')),
            'price' => $this->input->post('price'),
            'cate_id' => (int) $this->input->post('cate_id'),
        );

        $old_photo = $this->input->post('oldphoto');
        $old_photo_big = $this->input->post('oldphotobig');

        if (!empty($_FILES['photo']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo')) {
                $photo = $old_photo;
            } else {
                $up = $this -> upload -> data();
                $photo = 'uploads/product/' . $up['file_name'];
                $imageWidth = $up['image_width'];
                // if ($imageWidth > 300) {
                //     $this -> load -> library('image_lib');
                //     $config['image_library'] = 'GD2';
                //     $config['source_image'] = $up['full_path'];
                //     $config['quality'] = 75;
                //     $config['maintain_ratio'] = TRUE;
                //     $config['master_dim'] = 'auto';
                //     $config['width'] = 300;
                //     $config['height'] = 500;

                //     $this -> image_lib -> initialize($config);
                //     $this -> image_lib -> resize();
                // }
                @unlink($old_photo);
            }
        } else {
            $photo = $old_photo;
        }

        if (!empty($_FILES['photo_big']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo_big')) {
                $photo_big = $old_photo_big;
            } else {
                $up = $this -> upload -> data();
                $photo_big = 'uploads/product/' . $up['file_name'];
                
                @unlink($old_photo_big);
            }
        } else {
            $photo_big = $old_photo_big;
        }

        $arr['photo'] = $photo;
        $arr['big_photo'] = $photo_big;

        if ($this->db->update($this->tbl_product, $arr, array('id' => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function checkCodePro($code) {
        $sql = $this->db->select('id')->get_where('product', array('title' => $code));
        if ($sql->num_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    public function getList($field = null, $where = null, $order = null, $limit = null) {
        if ($field !== null) {
            foreach ($field as $key) {
                $this->db->select($key);
            }
        }
        if ($where !== null) {
            foreach ($where as $key => $val) {
                $this->db->where($key, $val);
            }
        }
        if ($order !== null) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($limit !== null) {
            $this->db->limit($limit['max'], $limit['begin']);
        }

        $query = $this->db->get($this->tbl_product);

        return $query->result_array();

        $query->free_result();
    }

    public function getListByCate($field = null, $cate = 0, $cate_child = 0, $order = null, $limit = null) {
        if ($field !== null) {
            foreach ($field as $key) {
                $this->db->select($key);
            }
        }
        if ($cate > 0) {
            $this->db->where('cateid', $cate);
        }
        if ($cate_child > 0) {
            $this->db->where('cate_child', $cate_child);
        }

        if ($order !== null) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($limit != null) {
            $this->db->limit($limit['max'], $limit['begin']);
        }

        $query = $this->db->get($this->tbl_product);

        return $query->result_array();

        $query->free_result();
    }

    public function total_by_cate($cate = 0, $cate_child = 0) {
        if ($cate > 0) {
            $this->db->where('cateid', $cate);
        }
        if ($cate_child > 0) {
            $this->db->where('cate_child', $cate_child);
        }
        $this->db->where('status', 1);
        $this->db->from('product');
        return $this->db->count_all_results();
    }

    public function read_pro($id) {
        //$qr = "SELECT * FROM product p JOIN product_photo pp ON p.id = pp.pid WHERE pid=$id";
        $query = $this->db->get_where($this->tbl_product, array('id' => $id))->row_array();
        //$sql = $this->db->query($qr);
        return $query;
    }

    public function getRandomPhoto($id) {
        //$qr = "SELECT * FROM product p JOIN product_photo pp ON p.id = pp.pid WHERE pid=$id";
        $query = $this->db->select('photo')->get_where($this->tbl_photo, array('pid' => $id))->row();
        if ($query) {
            if ($query->photo != "") {
                return $query->photo;
            } else {
                return "";
            }
        }
    }

    public function getRandomPhotoByColor($id) {
        //$qr = "SELECT * FROM product p JOIN product_photo pp ON p.id = pp.pid WHERE pid=$id";
        $query = $this->db->select('photo')->get_where($this->tbl_photo, array('colorid' => $id))->row();
        if ($query) {
            if ($query->photo != "") {
                return $query->photo;
            } else {
                return "";
            }
        }
    }

    public function getAttributeProduct($attribute = 'title', $id) {
        $sql = $this->db->select($attribute)->get_where($this->tbl_product, array('id' => $id));
        $re = $sql->row();
        return $re->$attribute;
    }

    public function delete($id) {

        //Delete photos from product_photo
        $qr2 = $this->db->select('photo, big_photo')
                ->get_where('product', array('id' => $id))
                ->row();
        if($qr2->photo !== NULL) @unlink($qr2->photo);
        if($qr2->big_photo !== NULL) @unlink($qr2->big_photo);
        if ($this->db->delete('product', array('id' => $id)))
            return TRUE;
        else
            return FALSE;
    }

}

/*
 * End file
 * Local: application/models/product_model.php
 */