<?php

class Book_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function add($data) {
        $arr = array(
            'price' => $this->input->post('price'),
            'is_free' => (int)$this->input->post('is_free'),
            'link_download' => $this->input->post('link_download'),
            'author' => $this->input->post('author'),
            'order'	=> (int)$this->input->post('order'),
            'status' => (int)$this->input->post('status')
        );

        if (!empty($_FILES['photo']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo')) {
                $photo = NULL;
            } else {
                $up = $this -> upload -> data();
                $photo = 'uploads/product/' . $up['file_name'];
                $imageWidth = $up['image_width'];
                if ($imageWidth > 300) {
                    $this -> load -> library('image_lib');
                    $config['image_library'] = 'GD2';
                    $config['source_image'] = $up['full_path'];
                    $config['quality'] = 75;
                    $config['maintain_ratio'] = TRUE;
                    $config['master_dim'] = 'auto';
                    $config['width'] = 300;
                    $config['height'] = 500;

                    $this -> image_lib -> initialize($config);
                    $this -> image_lib -> resize();
                }
                //@unlink($oldLogo);
            }
        } else {
            $photo = NULL;
        }

        $arr['photo'] = $photo;

        if ($this->db->insert('books', $arr)) {

        	$bookid = $this->db->insert_id();
        	foreach (config_item('language_support') as $key => $value) {
        		$_data = array(
	        		'book_id' => $bookid,
	        		'title'	=> $this->input->post('title'),
	        		'desc' => $this->input->post('desc'),
	        		'lang_code' => $value
        		);
        		$this->db->insert('books_language', $_data);
        	}
        	

            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function edit($data, $id) {
        $arr = array(
            'price' => $this->input->post('price'),
            'is_free' => (int)$this->input->post('is_free'),
            'link_download' => $this->input->post('link_download'),
            'author' => $this->input->post('author'),
            'order'	=> (int)$this->input->post('order'),
            'status' => (int)$this->input->post('status')
        );
        $oldPhoto = $this->input->post('old_photo');

        if (!empty($_FILES['photo']['name'])) {
            $config['upload_path'] = './uploads/product/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this -> load -> library('upload', $config);

            if (!$this -> upload -> do_upload('photo')) {
                $photo = $oldPhoto;
            } else {
                $up = $this -> upload -> data();
                $photo = 'uploads/product/' . $up['file_name'];
                $imageWidth = $up['image_width'];
                if ($imageWidth > 300) {
                    $this -> load -> library('image_lib');
                    $config['image_library'] = 'GD2';
                    $config['source_image'] = $up['full_path'];
                    $config['quality'] = 75;
                    $config['maintain_ratio'] = TRUE;
                    $config['master_dim'] = 'auto';
                    $config['width'] = 300;
                    $config['height'] = 500;

                    $this -> image_lib -> initialize($config);
                    $this -> image_lib -> resize();
                }
               @unlink($oldPhoto);
            }
        } else {
            $photo = $oldPhoto;
        }

        $arr['photo'] = $photo;

        if ($this->db->update('books', $arr, array('id' => $id))) {
        	$_data = array(
	        		'title'	=> $this->input->post('title'),
	        		'desc' => $this->input->post('desc'),
        	);
        	$this->db->update('books_language', $_data, array('book_id' => $id, 'lang_code' => $this->admin_lang));
        	
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function checkCodePro($code) {
        $sql = $this->db->select('book_id')->get_where('books_language', array('title' => $code));
        if ($sql->num_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    public function getList($field = null, $where = null, $order = null, $limit = null) {
        if ($field !== null) {
            foreach ($field as $key) {
                $this->db->select($key);
            }
        }
        if ($where !== null) {
            foreach ($where as $key => $val) {
                $this->db->where($key, $val);
            }
        }
        if ($order !== null) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($limit !== null) {
            $this->db->limit($limit['max'], $limit['begin']);
        }

        $query = $this->db->get($this->tbl_product);

        return $query->result_array();

        $query->free_result();
    }

    public function getListByCate($field = null, $cate = 0, $cate_child = 0, $order = null, $limit = null) {
        if ($field !== null) {
            foreach ($field as $key) {
                $this->db->select($key);
            }
        }
        if ($cate > 0) {
            $this->db->where('cateid', $cate);
        }
        if ($cate_child > 0) {
            $this->db->where('cate_child', $cate_child);
        }

        if ($order !== null) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($limit != null) {
            $this->db->limit($limit['max'], $limit['begin']);
        }

        $query = $this->db->get($this->tbl_product);

        return $query->result_array();

        $query->free_result();
    }

    public function total_by_cate($cate = 0, $cate_child = 0) {
        if ($cate > 0) {
            $this->db->where('cateid', $cate);
        }
        if ($cate_child > 0) {
            $this->db->where('cate_child', $cate_child);
        }
        $this->db->where('status', 1);
        $this->db->from('product');
        return $this->db->count_all_results();
    }

    public function read_pro($id) {
        //$qr = "SELECT * FROM product p JOIN product_photo pp ON p.id = pp.pid WHERE pid=$id";
        $query = $this->db->get_where($this->tbl_product, array('id' => $id))->row_array();
        //$sql = $this->db->query($qr);
        return $query;
    }

    public function getRandomPhoto($id) {
        //$qr = "SELECT * FROM product p JOIN product_photo pp ON p.id = pp.pid WHERE pid=$id";
        $query = $this->db->select('photo')->get_where($this->tbl_photo, array('pid' => $id))->row();
        if ($query) {
            if ($query->photo != "") {
                return $query->photo;
            } else {
                return "";
            }
        }
    }

    public function getRandomPhotoByColor($id) {
        //$qr = "SELECT * FROM product p JOIN product_photo pp ON p.id = pp.pid WHERE pid=$id";
        $query = $this->db->select('photo')->get_where($this->tbl_photo, array('colorid' => $id))->row();
        if ($query) {
            if ($query->photo != "") {
                return $query->photo;
            } else {
                return "";
            }
        }
    }

    public function getAttributeProduct($attribute = 'title', $id) {
        $sql = $this->db->select($attribute)->get_where($this->tbl_product, array('id' => $id));
        $re = $sql->row();
        return $re->$attribute;
    }

    public function delete($id) {

        //Delete photos from product_photo
        $qr2 = $this->db->select('photo')
                ->get_where('books', array('id' => $id))
                ->row();
        if($qr2->photo !== NULL) @unlink($qr2->photo);

        $this->db->delete('books_language', array('book_id' => $id));

        if ($this->db->delete('books', array('id' => $id)))
            return TRUE;
        else
            return FALSE;
    }

}

/*
 * End file
 * Local: application/models/product_model.php
 */