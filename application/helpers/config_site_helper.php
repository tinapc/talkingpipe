<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

if( !function_exists('get_setting') ) {

	function get_setting($key)
	{
		$CI = & get_instance();
		$CI->load->database();
		$row = $CI->db->select('value')->get_where('config_site', array('key' => $key));
		$row = $row->row();
		return $row->value;
	}

}
