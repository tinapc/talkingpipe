<?php

// Lay quang cao ben trai
function get_adv_left()
{
	$CI = & get_instance();

	$CI->db->order_by('order', 'asc');
	$advs = $CI->db->get_where('advertise', array('status' => 1, 'position' => 'left', 'post_to_day >=' => date('Y-m-d')))->result_array();

	if(count($advs)) {
		foreach($advs as $adv) {
			if($adv['file_type'] == 'jpg' OR $adv['file_type'] == 'gif' OR $adv['file_type'] == 'jpeg' OR $adv['file_type'] == 'png') { ?>
				<li>
					<a href="<?=$adv['link']?>" target="_blank"><img src="<?=base_url() . $adv['image']?>" alt="<?=$adv['desc']?>"></a>
				</li>
			<? } else { ?>
				<li>
					<embed src="<?=base_url() . $adv['image']?>" width="130"></embed>
				</li>
			<?php } ?>
		<?php }

	}
}

// Lay quang cao ben phai

function get_adv_right()
{
	$CI = & get_instance();

	$CI->db->order_by('order', 'asc');
	$advs = $CI->db->get_where('advertise', array('status' => 1, 'position' => 'right', 'post_to_day >=' => date('Y-m-d')))->result_array();

	if(count($advs)) {
		foreach($advs as $adv) {
			if($adv['file_type'] == 'jpg' OR $adv['file_type'] == 'gif' OR $adv['file_type'] == 'jpeg' OR $adv['file_type'] == 'png') { ?>
				<li>
					<a href="<?=$adv['link']?>" target="_blank"><img src="<?=base_url() . $adv['image']?>" alt="<?=$adv['desc']?>"></a>
				</li>
			<? } else { ?>
				<li>
					<embed src="<?=base_url() . $adv['image']?>" width="130"></embed>
				</li>
			<?php } ?>
		<?php }

	}
}

// Lay quang cao giua

function get_adv_center()
{
    $CI = & get_instance();

    $CI->db->order_by('order', 'asc');
    $advs = $CI->db->get_where('advertise', array('status' => 1, 'position' => 'center', 'post_to_day >=' => date('Y-m-d')))->result_array();

    if(count($advs)) {
        foreach($advs as $adv) {
            if($adv['file_type'] == 'jpg' OR $adv['file_type'] == 'gif' OR $adv['file_type'] == 'jpeg' OR $adv['file_type'] == 'png') { ?>
                <li>
                    <a href="<?=$adv['link']?>" target="_blank"><img src="<?=base_url() . $adv['image']?>" alt="<?=$adv['desc']?>"> <span><?=$adv['desc']?></span></a>
                </li>
            <? } else { ?>
                <li>
                    <embed src="<?=base_url() . $adv['image']?>" width="858" height="250"></embed>
                </li>
            <?php } ?>
        <?php }

    }
}


// Lay nhung tin la Tieu diem
function get_tin_tieudiem()
{
	$CI = & get_instance();

	$CI->db->order_by('id', 'desc');
	$CI->db->limit(5);
	$advs = $CI->db->select('image, title, cat_id, id')->get_where('article', array('status' => 1, 'hot' => 1))->result_array();

	if(count($advs)) {
		foreach($advs as $adv) { ?>
				<li>
					<div class="img-thumb-td">
						<img src="<?=base_url() . $adv['image']?>" alt="<?=$adv['title']?>">
					</div>
					<a href="<?=site_url('tin-tuc/xem/' . $adv['id'] .'-' . $adv['cat_id'] . '-'. $CI->util->alias(rtrim($adv['title'])))?>"> <?=$adv['title']?></a>
				</li>

		<?php }

	}
}


// Lay danh muc Mau nha dep + Cong trinh
function getCategoryLeft($type) {
    $CI = & get_instance();
    
    $CI->db->order_by('order', 'asc');
    $sql = $CI->db->select('id, cat_name')->get_where('category', array('status' => 1, 'type' => $type))->result();
    
    if( count($sql) ) {
        if($type == "nhadep") { ?>
            <li><a href="#"><?=$CI->lang->line('txt_maunha')?></a>
                <ul class="sub_sub">
                    <?php foreach( $sql as $item ) { ?>
                    <li>
                        <a href="<?=site_url('danh-muc/' . $item->id . '-' . $CI->util->alias($item->cat_name))?>"><?=$item->cat_name?></a>
                    </li>
                <?php } ?>
               </ul>
            </li>
        <?php }
        if($type == "ct") { ?>
            <li><a href="#"><?=$CI->lang->line('txt_ct')?></a>
                <ul class="sub_sub">
                    <?php foreach( $sql as $item ) { ?>
                    <li>
                        <a href="<?=site_url('danh-muc/' . $item->id . '-' . $CI->util->alias($item->cat_name))?>"><?=$item->cat_name?></a>
                    </li>
                <?php } ?>
               </ul>
            </li>
        <?php }
    }
}









