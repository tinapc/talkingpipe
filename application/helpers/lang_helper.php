<?php

if ( ! function_exists('t_lang'))
{
	function t_lang($line)
	{
		$CI =& get_instance();
		$line = $CI->lang->line($line);
		if(!$line OR !isset($line)) {
			return "No text";
		} else {
			return $line;
		}

	}
}