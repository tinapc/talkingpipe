<?php 

/*---------------------------
Class general function
---------------------------*/

// General menu from database
//Return menu item
//Param: $interger
function get_menu($menu_id, $lang_code, $is_lang = FALSE) {

	$ci = & get_instance();
	$ci->load->model('setting/setting_model');
	$d_lang = $lang_code;

	$ci->db->where(array('menus_item.menu_id' => $menu_id, 'menus_item.menu_status' => 1,  'menu_item_language.lang_code' => $lang_code));
	$ci->db->order_by('menus_item.menu_order', 'asc');
	$ci->db->join('menu_item_language', 'menus_item.id = menu_item_language.menu_item_id');
	$results = $ci->db->get('menus_item')->result_array();

	if(count($results)) { ?>
		<ul class="desktop">
		<?php foreach($results as $item) { ?>
	        <li><a href="<?=$item['menu_link']?>"><?=$item['item_name']?></a></li>	
		<?php } ?>
			<?php if($is_lang) : ?>
				<?php if($d_lang == 'en') : ?>
					<li><a href="<?=base_url()?>home/s_lang/?l=es&url=<?=current_url()?>">Espanol</a></li>
				<?php else : ?>
					<li><a href="<?=base_url()?>home/s_lang/?l=en&url=<?=current_url()?>">English</a></li>
				<?php endif ?>
			<?php endif?>
		</ul>
	<?php }
}

function get_menu_on_mobile($menu_id, $lang_code, $is_lang = FALSE) {

	$ci = & get_instance();
	$d_lang = $lang_code;
	
	$ci->db->where(array('menus_item.menu_id' => $menu_id, 'menus_item.menu_status' => 1, 'menu_item_language.lang_code' => $lang_code));
	$ci->db->order_by('menus_item.menu_order', 'asc');
	$ci->db->join('menu_item_language', 'menus_item.id = menu_item_language.menu_item_id');
	$results = $ci->db->get('menus_item')->result_array();

	if(count($results)) { ?>
		<ul>
		<?php foreach($results as $item) { ?>
	        <li><a href="<?=$item['menu_link']?>"><?=$item['item_name']?></a></li>
		<?php } ?>
			<?php if($is_lang) : ?>
				<?php if($d_lang == 'en') : ?>
					<li><a href="<?=base_url()?>home/s_lang/?l=es&url=<?=current_url()?>">Espanol</a></li>
				<?php else : ?>
					<li><a href="<?=base_url()?>home/s_lang/?l=en&url=<?=current_url()?>">English</a></li>
				<?php endif ?>
			<?php endif?>
		</ul>
	<?php }
}

/*
------------------------------
Get slider from database
------------------------------
*/
function get_slider($slider_id) {

}

function get_slider_mobile($slider_id) {

}


/*-----------------------
Get current uri segment
Param: $int
Return: $interger
------------------------*/
function get_uri($uri) {
	$ci = & get_instance();
	return $ci->uri->segment($uri);
}

/**End file */
/* Local file: /application/helpers/function_helper.php */