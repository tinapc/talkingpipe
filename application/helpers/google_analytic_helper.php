<?php
/*
* Function google_analytic
* Param $google_analytic ID
* Return script 
*/

if(! function_exists('analytic'))
{
	function analytic($id)
	{
		echo '<script type="text/javascript">';
		echo 'var _gaq = _gaq || [];';
		echo '_gaq.push(["_setAccount", "' . $id .'"]);';
		echo '_gaq.push(["_trackPageview"]);';
		 
		echo '(function() {';
		echo 'var ga = document.createElement("script"); ga.type = "text/javascript"; ga.async = true;';
		echo 'ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";';
		echo 'var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ga, s);';
		echo '})();';
		echo '</script>';
	}
}
