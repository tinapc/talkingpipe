<?php

/* 
 * This function using to get Lat and Lng from a address via google map api
 */

if(!function_exists('get_latlng')) {
    function get_latlng($address) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";
        //get the content from the api using file_get_contents
        $getmap = file_get_contents($url);
        //the result is in json format. To decode it use json_decode
        $googlemap = json_decode($getmap);
        //get the latitute, longitude from the json result by doing a for loop

        foreach($googlemap->results as $res){
            $address = $res->geometry;
            $latlng = $address->location;
            //$formattedaddress = $res->formatted_address;
                //echo "Latitude: ". $latlng->lat ."<br />". "Longitude:". $latlng->lng;
        }
        
        return $latlng;
    }
}

