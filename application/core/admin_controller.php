<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->template->set_theme('admin');
        $this->template->set_partial('header', 'partial/header');
        $this->template->set_partial('menu', 'partial/menu');
        $this->template->set_partial('crumb', 'partial/crumb');
        $this->template->set_partial('note', 'partial/note');
        $this->template->set_partial('loading_ajax', 'partial/loading_ajax');
        	
    
        $this->load->helper('cookie');
        //Get cookie lang
        $this->admin_lang = '';

        $lang_default = get_cookie('language_admin');	

        if($lang_default != '')
        {
        	$this->admin_lang = $lang_default;
        } else {
        	$this->admin_lang = 'en';
        }
		
        define('URL_ADMIN', $this->config->item('index_url').'admin/');
        define('themes_admin', base_url().'themes/admin/');
        
        $this->lang->load('admin');
        
        $this->check_login();
        
		$this->form_valid = $this->form_validation;
		$this->breadcrumb = "";
        $this->form_validation->set_error_delimiters('<span class="input-notification error png_bg">', '</span>');
    }
	
    /*
     * Function check status user login
     * 
    */
    public function check_login() {
        if(!$this->isLogin()) redirect(URL_ADMIN . 'login');
    }

    public function isLogin() {
        return ($this->session->userdata('logined') && $this->session->userdata('user_id'));
    }

    private function is_admin() {
        if(strtolower($this->session->userdata('username') == 'admin')) {
            return TRUE;
        } else {
            return FALSE;
        }	
    }
	
    public function message($type = 'info', $mess = NULL)
    {
        if($mess == NULL)
        {
            return FALSE;
        } else {
            return $this->session->set_flashdata($type, $mess);
        }
    }
	
    public function permission_view($controller) {

        //$pers = array();

        if($this->is_admin()) 
        {
            return TRUE;
        } else {		
            $user_group_id = $this->session->userdata('group');
            $query  = $this->db->select('permission_view')
                               ->where('user_group_id', $user_group_id)
                               ->get('user_group');

            //$arr = array('setting','gllaery');
            //print_r($arr);

            if($query->num_rows() > 0)
            {
                $re = $query->row();
                $pers = unserialize($re->permission_view);
                if(in_array($controller, $pers)) {
                        return TRUE;
                } else {
                        redirect(URL_ADMIN .'permission/error');
                }
            }	
        } 
    }
	
	
    public function permission_modifi($controller) 
    {
        $quyen_xoa = array();

        if($this->is_admin()) 
        {
                return TRUE;
        } else {

            $user_group_id = $this->session->userdata('group');
            $query  = $this->user_group->read($user_group_id);
            if(count($query) > 0 ) {
                $quyen_xoa = unserialize($query->permission_edit_del);
            }	

            if(count($quyen_xoa) != NULL) {
                if(!in_array($controller, $quyen_xoa)) {
	                return FALSE;
                } else {
    				return TRUE;                   
                }
            }	

        } 
    }
	
	function loadMail() 
	{
            $mail_config = array(	
                'protocol'		=> 'smtp', 
                'smtp_host'		=> $this->config->item('smtp_host'), 
                'smtp_user'		=> $this->config->item('smtp_user'), 
                'smtp_pass'		=> $this->config->item('smtp_pass'), 
                'smtp_port'		=> $this->config->item('smtp_port'), 
                'smtp_timeout'          => $this->config->item('smtp_timeout'), 
                'charset'		=> $this->config->item('mail_charset'),
                'wordwrap'		=> $this->config->item('mail_wordwrap'),
                'mailtype'		=> $this->config->item('mailtype')
            );	
            $this->load->library('email', $mail_config);
		
	}
	
} 
/* End file of file Admin_Controller.php */
/* Local: application/core/Admin_Controller.php */