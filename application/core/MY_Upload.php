<?php
    class MY_Upload extends CI_Upload
    {
         var $allow_flash = FALSE;

                function initialize($config = array()){
                     parent::initialize($config);
                     if (array_key_exists('allow_flash', $config)) $this->allow_flash = $config['allow_flash'];
                }
            /**
         * Verify that the filetype is allowed
         *
         * @access    public
         * @param    allow_flash = FALSE
         * @return    bool
         */   
        function is_allowed_filetype()
        {
            if (count($this->allowed_types) == 0 OR ! is_array($this->allowed_types))
            {
                $this->set_error('upload_no_file_types');
                return FALSE;
            }
   
            $image_types = array('gif', 'jpg', 'jpeg', 'png', 'jpe', 'mp3');
   
            foreach ($this->allowed_types as $val)
            {               
                $mime = $this->mimes_types(strtolower($val));
                if ($this->allow_flash)
                    if (is_array($mime)) {
                        $mime[] = 'application/octet-stream'; 
                    } else {
                        $mime = array($mime, 'application/octet-stream');
                    }
                   
               
                // Images get some additional checks
                if (in_array($val, $image_types))
                {
                    if (getimagesize($this->file_temp) === FALSE)
                    {
                        return FALSE;
                    }
                }
   
                if (is_array($mime))
                {
                    if (in_array($this->file_type, $mime, TRUE))
                    {
                        return TRUE;
                    }
                }
                else
                {
                    if ($mime == $this->file_type)
                    {
                        return TRUE;
                    }   
                }       
            }
           
            return FALSE;
        }
    }

/* End of file : application/libraries/MY_Upload.php */