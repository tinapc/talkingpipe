<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

class Public_Controller extends MY_Controller
{
    //Set variable global
    public $_image_fb = '';
    public $_desc_seo_fb = '';
    public $no_image = '';
    public $fullPath = '';
    public $_show_menu_service = FALSE;
    public $_phone_contact_header = '';
    public $_email_contact_header = '';

    public function __construct()
    {
        parent::__construct();

        $this->load->model('setting/setting_model', 'setting');
        $this->load->model('text/text_model', 'text');
        $this->load->helper('function');
        //Set timezone default
        date_default_timezone_set('Asia/Ho_Chi_Minh');	    
        
        $this->template->enable_parser(FALSE);
        $this->template->set_partial('header', 'partial/header');
        $this->template->set_partial('left_menu', 'partial/left_menu');
        $this->template->set_partial('right', 'partial/right');
        $this->template->set_partial('footer', 'partial/footer');

        $this->rout = $this->router->fetch_class();
        
        /* PATH TO THEMES
        ----------------------------*/
        define('theme_path', base_url() . 'public/');
        $this->output->enable_profiler(FALSE);

        //Set default value for varible gobal;
        $this->no_image = base_url() .'public/img/no_image.jpg';
        $this->fullPath = $this->config->item('index_url');

        $this->load->helper('cookie');
        //Get cookie lang
        $this->front_lang = '';

        $lang_default = get_cookie('language_front');   

        if($lang_default != '')
        {
            $this->front_lang = $lang_default;
        } else {
            $this->front_lang = 'en';
        }
        $this->lang->load('front', $this->front_lang);

        $this->text_for_header = '';
        $this->text_for_footer = '';
    }

    function loadMail()
    {
        $mail_config = array(
            'protocol'		=> 'sendmail',
            'smtp_host'		=> $this->config->item('smtp_host'),
            'smtp_user'		=> $this->config->item('smtp_user'),
            'smtp_pass'		=> $this->config->item('smtp_pass'),
            'smtp_port'		=> $this->config->item('smtp_port'),
            'smtp_timeout'	=> $this->config->item('smtp_timeout'),
            'charset'		=> $this->config->item('mail_charset'),
            'wordwrap'		=> $this->config->item('mail_wordwrap'),
            'mailtype'		=> $this->config->item('mailtype')
        );

        $this->load->library('email', $mail_config);
        $this->email->set_newline("\r\n");
    }

}
/* End of file Pbulic_controller.php */
/* Local: application/core/Public_Controller.php */
