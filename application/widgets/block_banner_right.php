<?php
class BLock_banner_right extends Widget {
	function run($status = 1) {
		$ci = & get_instance();
		$data['query'] = $this->db->order_by('order', 'asc')->get_where('banner', array('status' => 1, 'position' => 'r'))->result();
		$this->render('block_banner_right', $data);
	}
}