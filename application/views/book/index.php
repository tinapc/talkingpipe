<link href="<?=theme_path?>css/books.css" rel="stylesheet" media="screen"/>

<div class="desktop vert10" align="center">
	<section class="vert8">
	<?php if(count($books)) : ?>
		<?php foreach($books as $book) : ?>	
			<div id="top" class="desktop vert8 street3 clear">
				<section>
				<img src="<?=base_url() . $book['photo']?>" width="219" class="desktop vert3"	alt="<?=$book['title']?>" longdesc="<?=$book['title']?>">	
				</section>
				<div style="clear:both"></div>
				<article>
					<h2><?=$book['title']?></h2>	
					<h4>by <?=$book['author']?></h4>		
					<div>
						<?=$book['desc']?>
					</div>
						
				</article>	
				<div style="clear:both"></div>
				<?php if($book['is_free'] == 0) : ?>
					<div class="desktop vert8 clear floatL">
						<a href="<?=base_url()?>cart/add/book/<?=$book['id']?>"><button class="css_button floatL zeroL"><?=$this->lang->line('cart:btn_buynow')?></button></a>
						<a href="javascript:void(0)" onclick="add_to_cart('book', '<?=$book['id']?>')"><button class="css_button floatL zeroL"><?=$this->lang->line('cart:btn_addtocart')?></button></a>
						<a href="<?=base_url()?>cart"><button class="css_button floatL"><?=$this->lang->line('cart:btn_viewcart')?></button></a>
					</div>
				<?php else : ?>
					<a href="<?=$book['link_download']?>"><button class="css_button_download floatL"><?=$this->lang->line('cart:btn_freedownload')?></button></a>
				<?php endif ?>
			</div><!--END DIV ID TOP-->	
		<?php endforeach ?>
	<?php endif ?>
	</section>
</div>

<?php if(count($books)) : ?>
	<div id="mobileBooks" class="mobile vert10">
	<?php foreach($books as $book) : ?>
		<?php if($book['is_free'] == 0) : ?>
				<div id="mobileBuy" class="mobile vert10">
					<a href="<?=base_url()?>cart/add/book/<?=$book['id']?>"><button class="css_button"><?=$this->lang->line('cart:btn_buynow')?></button></a>
					<a href="javascript:void(0)" onclick="add_to_cart('book', '<?=$book['id']?>')"><button class="css_button"><?=$this->lang->line('cart:btn_addtocart')?></button></a>
					<a href="<?=base_url()?>cart"><button class="css_button"><?=$this->lang->line('cart:btn_viewcart')?></button></a>
				</div>

				<div id="mobileAmericana" class="mobile vert10 clearfix">		
					<h2><?=$book['title']?></h2>
					<!-- <h2>the Proclamation Wars!</h2>
					<h3>in the Shadow of Raven Mockers...</h3> -->
					<h4>by <?=$book['author']?></h4>
					<img src="<?=base_url() . $book['photo']?>" width="219" class="mobile vert2"
								alt="<?=$book['title']?>" longdesc="<?=$book['photo']?>">
					<img src="<?=base_url()?>public/img/page_bloc_b/dick_filter_opacity.jpg" width="219" class="mobile vert2" longdesc="Picture%20Richard%20Arnold">
				</div>
		<?php else : ?>
			<a href="<?=$book['link_download']?>"><button class="css_button_download floatR"><?=$this->lang->line('cart:btn_freedownload')?></br> <?=$book['title']?></br> by <?=$book['author']?></button></a>
		<?php endif ?>
	<?php endforeach?>
	</div>
<?php endif ?>


<script type="text/javascript">
function add_to_cart(type, pid) {
	$.ajax({
		type: "POST",
		dataType: "json",
		url:"<?=base_url()?>cart/addCartAjax/" + type + "/" + pid,
		success:function(data) {
			if(data.status) {
				alert('The Item has been added to cart');
			} else {
				alert('You have a error. Pls try again');
			}
		}
	})
}
</script>
	