<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>
    <div class="row-fluid">
    <div class="span12">
        <div class="head clearfix">
            <div class="isw-list"></div>
            <h1><?php echo $this->_page_header?></h1>
            <ul class="buttons">                                                  
                <li>
                    <a href="#" class="isw-settings"></a>
                    <ul class="dd-list">
                        <li><a href="<?php echo $link_add?>"><span class="isw-plus"></span>Create new</a></li>
                        <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                        <li><a href="javascript:void(0)" onclick="delMulti('<?=$link_delMul?>', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li>
                    </ul>
                </li>
            </ul> 
        </div>
        <div class="block-fluid tabs">
            <ul>
                <!--<li><a href="#tabs-1"><strong>All Comments</strong></a></li>-->
                <li><a href="#tabs-2"><strong><?php echo $this->_page_header?></strong></a></li>
                <!-- <li><a href="#tabs-3">Tab 3</a></li> -->
            </ul>                                            

            <div id="tabs-2">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="block-fluid table-sorting clearfix">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                                <thead>
                                    <tr> 
                                        <th><input type="checkbox" name="checkall"/></th>                             
                                        <th width="10%">ID</th>
                                        <th width="30%">Mô tả</th>
                                        <th width="30%">Url</th>
                                        <th width="10%">Vị trí</th>
                                        <th width="10%">Trạng thái</th>
                                        <th width="40%">Action</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(count($body)) : ?>
                                        <?php foreach($body as $item) : ?>
                                            <tr id="remove2_<?=$item['id']?>">  
                                            <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>                                  
                                                <td><?=$item['id']?></td>
                                                <td><?php echo $item['desc'];?></td>
                                                <td><a href="<?=base_url() . $item['photo']?>" rel="prettyPhoto" class="tip" title="Xem hinh"><?=$item['photo']?></a></td>
                                                <td>
                                                   <?php if($item['position'] == 'r') echo "Phải"; else echo "Banner chính";
                                                   ?>
                                                </td>
                                                <td>
                                                    <?php if($item['status'] == 1) : ?>
                                                        <a href="<?=$link_change_status?><?=$item['id']?>/<?=$item['status']?>"><i class="icon-ok"></i></a>
                                                    <?php else : ?>
                                                        <a href="<?=$link_change_status?><?=$item['id']?>/<?=$item['status']?>"><i class="icon-remove"></i></a>
                                                    <?php endif ?>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" onclick="confirm_del('<?=$link_del . $item['id']?>', 'remove2_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a>
                                                    <a href="<?=$link_edit . $item['id']?>" class="icon-edit tip" title="Edit this item"></a>
                                                </td>                                    
                                            </tr>
                                        <?php endforeach ?>
                                    <?php endif ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>     
    </div>
    <div class="dr"><span></span></div>

</div>

<script type="text/javascript">
	$(function(){
		$("a[rel^='prettyPhoto']").prettyPhoto();
	})
</script>