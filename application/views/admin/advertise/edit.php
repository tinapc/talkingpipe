<div class="workplace">  
	
	<?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
		<div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span3">Link:</div>
                        <div class="span9"><input value="<?=$info['link']?>" type="text" name="link" id="link"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Order:</div>
                        <div class="span9"><input value="<?=$info['order']?>" type="text" name="order" id="order"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div> 
                    <div class="row-form clearfix">
                        <div class="span3">Public:</div>
                        <div class="span9">
                        	<select name="status">
                                <?php if($info['status'] == 1) : ?>
                        			<option value="1" selected="selected">Public</option>
                        		<?php else : ?>
                        			<option value="1" selected="selected">Public</option>
                        		<?php endif ?>
                        		<?php if($info['status'] == 0) : ?>
                        			<option value="0" selected="selected">No Public</option>
                        		<?php else : ?>
                        			<option value="0">No Public</option>
                        		<?php endif ?>
                        	</select>
                        </div>
                    </div>                       
                    <div class="row-form clearfix">
                        <div class="span3">Position:</div>
                        <div class="span9">
                            <select name="position">
                            	<?php if($info['position'] == 'r') : ?>
                        			<option value="r" selected="selected">Phải</option>
                        		<?php else : ?>
                        			<option value="r" selected="selected">Phải</option>
                        		<?php endif ?>
                        		<?php if($info['position'] == 0) : ?>
                        			<option value="c" selected="selected">Banner Chính</option>
                        		<?php else : ?>
                        			<option value="c">Banner Chính</option>
                        		<?php endif ?>
                            </select>
                        </div>
                    </div> 
                    <div class="row-form clearfix">
                        <div class="span3">Description:</div>
                        <div class="span9"><textarea id="desc" style="height:150px" name="desc"><?=$info['desc']?></textarea></div>
                    </div>                                                                                                 
                    <div class="row-form clearfix">
                        <div class="span3">Hình ảnh:</div>
                        <div class="span9">        
                            <input type="file" name="photo">
                            <input type="hidden" name="oldPhoto" value="<?=$info['photo']?>">
                           	<?php if($info['photo'] !=="") : ?>
                           		<img src="<?=base_url() . $info['photo']?>" style="width:200px">
                           <?php endif ?>
                        </div>
                    </div>                               
                    <div class="footer tar">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>