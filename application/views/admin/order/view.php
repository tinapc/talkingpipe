
<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note']; ?>
    <div class="row-fluid">
        <div class="span6">
            <div class="head clearfix">
                <div class="isw-list"></div>
                <h1>Order</h1>
            </div>
            <div class="block-fluid">
                <div class="row-form clearfix">
                    <div class="span3">ID:</div>
                    <div class="span9">
                        <?=$order['id']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Invoice:</div>
                    <div class="span9">
                        <?=$order['invoice']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Transaction:</div>
                    <div class="span9">
                        <?=$order['transaction']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Total:</div>
                    <div class="span9">
                        $<?=number_format($order['total'], 2, '.', ',')?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Status:</div>
                    <div class="span9">
                        <?=$order['status']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Created date:</div>
                    <div class="span9">
                        <?=$order['created_date']?>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="head clearfix">
                <div class="isw-list"></div>
                <h1>Bill to</h1>
            </div>
            <div class="block-fluid">
                <div class="row-form clearfix">
                    <div class="span3">First name:</div>
                    <div class="span9">
                        <?=$payer['first_name']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Last name:</div>
                    <div class="span9">
                        <?=$payer['last_name']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Email:</div>
                    <div class="span9">
                        <?=$payer['email']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Phone:</div>
                    <div class="span9">
                        <?=$payer['phone']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Address:</div>
                    <div class="span9">
                        <?=$payer['address']?>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span3">Post code:</div>
                    <div class="span9">
                        <?=$payer['zipcode']?>
                    </div>
                </div>
            </div>
        </div>      
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-list"></div>
                <h1>Shoping cart</h1>
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table">
                    <thead>
                        <tr>
                            <th width="25%">Product name</th>
                            <th width="25%">Qty</th>
                            <th width="25%">Price</th>
                            <th width="25%">Subtotal</th>                               
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($order_info)) : ?>
                        <?php foreach($order_info as $item) : ?>
                            <tr id="per_<?=$item['id']?>">
                                <td><?=$item['product_name']?></td>                                 
                                <td><?=$item['product_quality']?></td> 
                                <td>$<?=number_format($item['price'], 2, '.', ',')?></td>
                                <td>$<?=number_format($item['subtotal'], 2, '.', ',')?></td> 
                                
                            </tr>
                            <?php endforeach ?>
                            <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    
