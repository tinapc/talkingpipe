<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>
    <div class="row-fluid">
    <div class="span12">
        <div class="head clearfix">
            <div class="isw-list"></div>
            <h1><?=$this->_page_header?></h1>
            <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            
                            <li><a href="javascript:void(0)" onclick="delMulti('<?=$url_delMul?>', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li>
                        </ul>
                    </li>
                </ul> 
        </div>
        <div class="block-fluid tabs">

            <ul>
                <!--<li><a href="#tabs-1"><strong>All Comments</strong></a></li>-->
                <li><a href="#tabs-2"><strong><?=$this->_page_header?></strong></a></li>
                                <!-- <li><a href="#tabs-3">Tab 3</a></li> -->
            </ul>                                             

            <div id="tabs-2">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="block-fluid table-sorting clearfix">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                                <thead>
                                    <tr> 
                                        <th><input type="checkbox" name="checkall"/></th>                                        
                                        <th width="5%">ID</th>
                                        <th width="15%">Invoice</th>                
                                        <th width="15%">Transaction</th>
                                        <th width="20">Total</th>
                                        <th width="15%">Status</th>
                                        <th width="20%">Created date</th>
                                        <th width="15%"></th>                                  
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php foreach ($billings as $item) : ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="delPerson" name="checkbox" value="<?= $item['id'] ?>"/>
                                            </td>
                                            <td><?=$item['id']?></td> 
                                            <td><?=$item['invoice']?></td>     
                                            <td><?=$item['transaction']?></td> 
                                            <td>$<?=number_format($item['total'], 2, '.', ',')?></td>
                                            <td><?=$item['status']?></td>
                                            <td><?=$item['created_date']?></td> 
                                            <td>
                                                <a href="<?=URL_ADMIN?>order/view/<?=$item['id']?>">View detail</a>
                                                <a href="<?=URL_ADMIN?>order/delete/<?=$item['id']?>">Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>     
    </div>
    <div class="dr"><span></span></div>

</div>
<script type="text/javascript">
function changeStatusCart(status, id) {
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {"status" : status, "id": id},
        url: baseJs + "cart/changeStatusCart",
        success: function(data) {
            window.location.reload(true);
        }    
    })
    
}
</script>
 