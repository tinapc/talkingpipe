<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span2">Title:</div>
                        <div class="span10"><input value="<?=$info['title']?>" class="validate[required]" type="text" name="title" id="title"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Author:</div>
                        <div class="span10"><input value="<?=$info['author']?>" class="validate[required]" type="text" name="author" id="author"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Price:</div>
                        <div class="span10"><input value="<?=$info['price']?>" class="validate[required] " type="text" name="price" id="price"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Is free:</div>
                        <div class="span10">
                            <select name="is_free" id="is_free" onchange="change_status_free(this.value)">
                                <?php if($info['status'] == 1): ?>
                                    <option value="1" selected="selected">Yes</option>
                                <?php else : ?>
                                    <option value="1">Yes</option>
                                <?php endif?>
                                <?php if($info['status'] == 0): ?>
                                    <option value="0" selected="selected">No </option>
                                <?php else : ?>
                                    <option value="0">No </option>
                                <?php endif?>
                            </select>
                        </div>
                    </div>
                    <div class="row-form clearfix" id="link_down" style="display:none">
                        <div class="span2">Link download:</div>
                        <div class="span10"><input value="<?=$info['link_download']?>" type="text" name="link_download" id="link_download"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Order:</div>
                        <div class="span10"><input value="<?=$info['order']?>" type="text" name="order" id="order"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div> 
                    <div class="row-form clearfix">
                        <div class="span2">Public:</div>
                        <div class="span10">
                            <select name="status">
                                <?php if($info['status'] == 1): ?>
                                    <option value="1" selected="selected">Public</option>
                                <?php else : ?>
                                    <option value="1">Public</option>
                                <?php endif?>
                                <?php if($info['status'] == 0): ?>
                                    <option value="0" selected="selected">No Public</option>
                                <?php else : ?>
                                    <option value="0">No Public</option>
                                <?php endif?>
                            </select>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Photo:</div>
                        <div class="span10"><input type="file" name="photo" id="photo"/> 
                            <input type="hidden" name="old_photo" value="<?=$info['photo']?>">
                            <?php if($info['photo'] !== NULL) : ?> 
                                <img src="<?=base_url() . $info['photo']?>" width="50" height="50">
                            <?php endif ?> 
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Description:</div>
                        <div class="span10"><textarea id="desc" style="height:300px" name="desc" class="validate[required]"><?=$info['desc']?></textarea></div>
                    </div>                             
                    <div class="footer tar" style="text-align:left">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		var free = $('#is_free').val();
		if(free == 1) {
			$('#link_down').show();
		}
	});

	function change_status_free(status){
		if(status == 1) {
			$('#link_down').show();
		} else {
			$('#link_down').hide();
		}
	}
</script>
<script type="text/javascript">

    tinyMCE.init({
    // General options
    mode : "exact",
    elements : "desc",
    theme : "advanced",
    skin : "o2k7",
    relative_urls : false,
	convert_urls: false,
    plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,pastetext,pasteword,bullist,numlist,|,formatselect,fontselect,fontsizeselect, forecolor,backcolor,fullscreen",
    theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,fontselect,blockquote,|,undo,redo,|,link,unlink,image,media,|,code,preview",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    file_browser_callback: "ajaxfilemanager",

    /* Example content CSS (should be your site CSS)
    content_css : "css/content.css",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js",
    */
    // Replace values for the template plugin
    template_replace_values : {
        username : "Some User",
        staffid : "991234"
    }
    });

function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    switch (type) {
        case "image":
                break;
        case "media":
                break;
        case "flash":
                break;
        case "file":
                break;
        default:
            return false;
    }
    tinyMCE.activeEditor.windowManager.open({
        url: "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
        width: 782,
        height: 400,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });

}


</script>