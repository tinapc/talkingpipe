<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>
    <div class="row-fluid">
    <div class="span12">
        <div class="head clearfix">
            <div class="isw-list"></div>
            <h1><?=$this->_page_header?></h1>
            <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <li><a href="<?=$url_add?>"><span class="isw-plus"></span>Create new</a></li>
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                            <li><a href="javascript:void(0)" onclick="delMulti('<?=$url_delMul?>', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li>
                        </ul>
                    </li>
                </ul> 
        </div>
        <div class="block-fluid tabs">

            <ul>
                <!--<li><a href="#tabs-1"><strong>All Comments</strong></a></li>-->
                <li><a href="#tabs-2"><strong><?=$this->_page_header?></strong></a></li>
                                <!-- <li><a href="#tabs-3">Tab 3</a></li> -->
            </ul>                                             

            <div id="tabs-2">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="block-fluid table-sorting clearfix">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                                <thead>
                                    <tr> 
                                        <th><input type="checkbox" name="checkall"/></th>                             
                                        <th width="5%">ID</th>
                                        <th width="25%">Photo</th>
                                        <th width="15%">Name</th>
                                        <th width="10%">Price</th>
                                        <th width="10%">Is free</th>
                                        <th width="10%">Status</th>
                                        <th width="25%">Action</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(count($body)) : ?>
                                        <?php foreach($body as $item) : ?>
                                            <tr id="remove2_<?=$item['id']?>">  
                                            <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>                                  
                                                <td><?=$item['id']?></td>
                                                <td>
                                                	<a href="<?=base_url() . $item['photo']?>" class="fancybox" rel="group">
                                                		<img src="<?=base_url() . $item['photo']?>" width="50" height="50">
                                                	</a>
                                                </td>
                                                <td><?php echo $item['title'];?></td>
                                                <td>$<?=number_format($item['price'], 2, '.', ',')?></td>
                                                <td>
                                                	<?php if($item['is_free'] == 1) : ?>
                                                        <a href="#"><i class="icon-ok"></i></a>
                                                    <?php else : ?>
                                                        <a href="#"><i class="icon-remove"></i></a>
                                                    <?php endif ?>
                                                </td>
                                                <td>
                                                    <?php if($item['status'] == 1) : ?>
                                                        <a href="<?=URL_ADMIN . $url_change_status?><?=$item['id']?>/<?=$item['status']?>"><i class="icon-ok"></i></a>
                                                    <?php else : ?>
                                                        <a href="<?=URL_ADMIN . $url_change_status?><?=$item['id']?>/<?=$item['status']?>"><i class="icon-remove"></i></a>
                                                    <?php endif ?>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" onclick="confirm_del('<?=$url_del . $item['id']?>', 'remove2_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a>
                                                    <a href="<?=$url_edit . $item['id']?>" class="icon-edit tip" title="Edit this item"></a>
                                                </td>                                    
                                            </tr>
                                        <?php endforeach ?>
                                    <?php endif ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>     
    </div>
    <div class="dr"><span></span></div>

</div>
 