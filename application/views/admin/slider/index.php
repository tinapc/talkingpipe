<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="row-fluid">
        
        <div class="span6">                    
            <div class="head clearfix">
                <div class="isw-grid"></div>
                <h1><?=$this->_page_header?></h1>      
                <!-- <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <li><a href="<?=$link_add?>"><span class="isw-plus"></span> Create new page</a></li>
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                            <!-- <li><a href="javascript:void(0)" onclick="delMulti('<?=current_url()?>/delMulti', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li>
                        </ul>
                    </li>
                </ul>  -->                   
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="checkall"/></th>
                            <th width="5%">ID</th>
                            <th width="50%">Slider name</th>
                            <th width="30%">Action</th>                                   
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($body)) : ?>
                        <?php foreach($body as $item) : ?>
                            <tr id="per_<?=$item['id']?>">
                                <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>
                                <td><?=$item['id']?></td>                                 
                                <td><?=$item['slider_name']?></td> 
                                
                                <td>
                                    <!-- <a href="javascript:void(0)" onclick="confirm_del('<?=URL_ADMIN?>slider/delete/<?=$item['id']?>', 'per_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a> -->
                                    <a href="javascript:void(0)" onclick="getItemEdit('<?=$link_edit?><?=$item['id']?>')" class="icon-edit tip" title="Edit this item"></a>
                                    <a href="<?=$link_add_item?><?=$item['id']?>" class="icon-th-list tip" title="Add image for this slider"></a>
                                </td>
                            </tr>
                            <?php endforeach ?>
                            <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>                                
        
        
		<div class="span6">
			<form method="post">
				<input type="hidden" name="old_id" id="old_id">
		    		<div class="head clearfix">
		                <div class="isw-plus"></div>
		                <h1>Add slider</h1>
		            </div>
		            <div class="block-fluid">                        
		            	<div class="row-form clearfix">
		                    <div class="span2">Slider name:</div>
		                    <div class="span10">
		                    	<input type="text" name="slider_name" id="slider_name" value=""/>
		                    </div>
		                </div>
		                <div class="footer tar" style="text-align:left">
		                    <button class="btn" onclick="$('#frm_add').submit()">Save</button>
		                </div>       
		    		</div>
		    </form>
		</div>
    </div>
    <div class="dr"><span></span></div>

</div>
 
 <script type="text/javascript">
function getItemEdit(url) {
	$.ajax({
		type: "POST",
		dataType: "json",
		url: url,
		success: function(data) {
			$('#old_id').val(data.sliderid);
			$('#slider_name').val(data.slider_name);
		}
	})
}
 </script>