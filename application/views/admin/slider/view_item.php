<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="row-fluid">
        
        <div class="span6">                    
            <div class="head clearfix">
                <div class="isw-grid"></div>
                <h1><?=$this->_page_header?></h1>      
                <!-- <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <li><a href="<?=$link_add?>"><span class="isw-plus"></span> Create new page</a></li>
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                            <!-- <li><a href="javascript:void(0)" onclick="delMulti('<?=current_url()?>/delMulti', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li>
                        </ul>
                    </li>
                </ul>  -->                   
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="checkall"/></th>
                            <th width="5%">ID</th>
                            <th width="50%">Photo</th>
                            <th width="30%">Action</th>                                   
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($body)) : ?>
                        <?php foreach($body as $item) : ?>
                            <tr id="per_<?=$item['id']?>">
                                <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>
                                <td><?=$item['id']?></td>                                 
                                <td>
                                	<a href="<?=base_url() . $item['image']?>" class="fancybox" rel="group">
                                	<img class="img-polaroid" src="<?=base_url() . $item['image']?>" width="75" height="75">
                                	</a>
                                </td> 
                                
                                <td>
                                    <a href="javascript:void(0)" onclick="confirm_del('<?=URL_ADMIN?>slider/delete_photo/<?=$item['id']?>', 'per_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a>
                                    
                                </td>
                            </tr>
                            <?php endforeach ?>
                            <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>                                
        
        
		<div class="span6">
			<form method="post" enctype="multipart/form-data">
				<input type="hidden" name="old_id" id="old_id">
		    		<div class="head clearfix">
		                <div class="isw-plus"></div>
		                <h1>Add photo</h1>
		            </div>
		            <div class="block-fluid">                        
		            	<div class="row-form clearfix">
		                    <div class="span2">Photo 1:</div>
		                    <div class="span10">
		                    	<input type="file" name="photo_1" id="photo_1" value=""/>
		                    </div>
		                </div>
		                <div class="row-form clearfix">
		                    <div class="span2">Photo 1:</div>
		                    <div class="span10">
		                    	<input type="file" name="photo_2" id="photo_2" value=""/>
		                    </div>
		                </div>
		                <div class="row-form clearfix">
		                    <div class="span2">Photo 1:</div>
		                    <div class="span10">
		                    	<input type="file" name="photo_3" id="photo_3" value=""/>
		                    </div>
		                </div>
		                <div class="footer tar" style="text-align:left">
		                    <button class="btn" onclick="$('#frm_add').submit()">Save</button>
		                </div>       
		    		</div>
		    </form>
		</div>
    </div>
    <div class="dr"><span></span></div>

</div>
 
 <script type="text/javascript">
function getItemEdit(url) {
	$.ajax({
		type: "POST",
		dataType: "json",
		url: url,
		success: function(data) {
			$('#old_id').val(data.sliderid);
			$('#slider_name').val(data.slider_name);
		}
	})
}
 </script>