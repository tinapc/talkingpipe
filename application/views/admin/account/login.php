<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <![endif]-->
    
    <title>Login</title>

    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="<?=themes_admin?>css/stylesheets.css" rel="stylesheet" type="text/css" />  
    <!--[if lt IE 8]>
        <link href="css/ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->            
    <link rel='stylesheet' type='text/css' href='<?=themes_admin?>css/fullcalendar.print.css' media='print' />
    
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js'></script>
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/jquery/jquery.mousewheel.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/excanvas.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.js'></script>    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.stack.js'></script>    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.pie.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/charts/jquery.flot.resize.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/sparklines/jquery.sparkline.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/fullcalendar/fullcalendar.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/select2/select2.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/uniform/uniform.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/validation/languages/jquery.validationEngine-en.js' charset='utf-8'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/validation/jquery.validationEngine.js' charset='utf-8'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/qtip/jquery.qtip-1.0.0-rc3.min.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/cleditor/jquery.cleditor.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/dataTables/jquery.dataTables.min.js'></script>    
    
    <script type='text/javascript' src='<?=themes_admin?>js/plugins/fancybox/jquery.fancybox.pack.js'></script>
    
    <script type='text/javascript' src='<?=themes_admin?>js/cookies.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/actions.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/charts.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/plugins.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/settings.js'></script>
    <script type='text/javascript' src='<?=themes_admin?>js/common.js'></script>
    
</head>
<body>
    
    <div class="loginBlock" id="login" style="display: block;">
        <h1>Sign In</h1>
        <div class="dr"><span></span></div>
        <div class="loginForm">
            <form class="form-horizontal" action="<?=$action?>" method="POST" id="validation">
                <p style="color:#ff0000; text-align:center"><?=$this->session->flashdata('error')?></p>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-envelope"></span></span>
                        <input type="text" name="username" id="inputEmail" placeholder="Username" class="validate[required]"/>
                    </div>                
                </div>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-lock"></span></span>
                        <input type="password" name="password" id="inputPassword" placeholder="Password" class="validate[required]"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="control-group" style="margin-top: 5px;">
                            <!-- <label class="checkbox"><input type="checkbox"> Remember me</label> -->                                                
                        </div>                    
                    </div>
                    <div class="span4">
                        <button type="submit" class="btn btn-block">Sign in</button>       
                    </div>
                </div>
            </form>  
            <div class="dr"><span></span></div>
            <!-- <div class="controls">
                <div class="row-fluid">
                    <div class="span6">
                        <button class="btn btn-link btn-block" onClick="loginBlock('#forgot');">Forgot your password?</button>
                    </div>
                    <div class="span2"></div>
                    <div class="span4">
                        <button class="btn btn-link btn-block" onClick="loginBlock('#reg');">Registration</button>
                    </div>
                </div>
            </div> -->
        </div>
    </div>    
    
    <div class="loginBlock" id="forgot">
        <h1>Forgot your password?</h1>
        <div class="dr"><span></span></div>
        <div class="loginForm">
            <form class="form-horizontal" action="index.html" method="POST">
                <p>This form help you return your password. Please, enter your password, and send request</p>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-envelope"></span></span>
                        <input type="text" placeholder="Your email"/>
                    </div>                
                </div>                
                <div class="row-fluid">
                    <div class="span8"></div>
                    <div class="span4">
                        <button type="submit" class="btn btn-block">Send request</button>       
                    </div>
                </div>
            </form>  
            <div class="dr"><span></span></div>
            <div class="controls">
                <div class="row-fluid">                    
                    <div class="span12">
                        <button class="btn btn-link" onClick="loginBlock('#login');">&laquo; Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="loginBlock" id="reg">
        <h1>Registration</h1>
        <div class="dr"><span></span></div>
        <div class="loginForm">
            <form class="form-horizontal" action="index.html" method="POST">
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-user"></span></span>
                        <input type="text" placeholder="Name"/>
                    </div>                
                </div>
                <div class="dr"><span></span></div>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-lock"></span></span>
                        <input type="password" placeholder="Password"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-lock"></span></span>
                        <input type="password" placeholder="Re-Password"/>
                    </div>
                </div>                
                <div class="dr"><span></span></div>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-envelope"></span></span>
                        <input type="text" placeholder="E-mail"/>
                    </div>                
                </div>                
                <div class="row-fluid">
                    <div class="span8">
                        <div class="control-group" style="margin-top: 5px;">
                            <label class="checkbox"><input type="checkbox"> I agree with terms..</label>                                                
                        </div>                                                  
                    </div>
                    <div class="span4">
                        <button type="submit" class="btn btn-block">Registration</button>       
                    </div>
                </div>
            </form>  
            <div class="dr"><span></span></div>
            <div class="controls">
                <div class="row-fluid">
                    <div class="span12">
                        <button class="btn btn-link" onClick="loginBlock('#login');">&laquo; Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    
</body>
</html>
