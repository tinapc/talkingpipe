<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span2">Item name:</div>
                        <div class="span10">
                            <input type="text" name="item_name" value="<?=$body['item_name']?>"/>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Item link:</div>
                        <div class="span10">
                            <input type="text" name="item_link" value="<?=$body['menu_link']?>"/>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Item order:</div>
                        <div class="span10">
                            <input type="text" name="item_order" value="<?=$body['menu_order']?>"/>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Item status:</div>
                        <div class="span10">
                            <select name="status">
                                <?php if($body['menu_status'] == 1) : ?>
                                    <option value="1" selected="selected">Public</option>
                                <?php else : ?>
                                    <option value="1" selected="selected">Public</option>
                                <?php endif ?>
                                <?php if($body['menu_status'] == 0) : ?>
                                    <option value="0" selected="selected">No Public</option>
                                <?php else : ?>
                                    <option value="0">No Public</option>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="footer tar" style="text-align:left">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                  
                                
                </form>
            </div>

        </div>
    </div>

</div>
