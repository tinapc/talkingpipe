<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span3">Menu name:</div>
                        <div class="span9"><input value="" class="validate[required]" type="text" name="menu_name" id="menu_name"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>                            
                    <div class="footer tar" style="text-align:left">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>
