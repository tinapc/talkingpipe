<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="row-fluid">
        <div class="span12">                    
            <div class="head clearfix">
                <div class="isw-grid"></div>
                <h1><?=$this->_page_header?></h1>      
                <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <!-- <li><a href="<?=$link_add?>"><span class="isw-plus"></span> Create new page</a></li> -->
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                            <!-- <li><a href="javascript:void(0)" onclick="delMulti('<?=current_url()?>/delMulti', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li> -->
                        </ul>
                    </li>
                </ul>                    
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="checkall"/></th>
                            <th width="5%">ID</th>
                            <th width="30%">Item name</th>
                            <th width="20%">Link</th>
                            <th width="10%">Status</th>
                            <th width="30%">Action</th>                                   
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($body)) : ?>
                        <?php foreach($body as $item) : ?>
                            <tr id="per_<?=$item['id']?>">
                                <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>
                                <td><?=$item['id']?></td>                                 
                                <td><?=$item['item_name']?></td> 
                                <td><?=$item['menu_link']?></td>
                                <td>
                                	<?php if($item['menu_status'] == 1) : ?>
                                        <a href="<?=URL_ADMIN?>menu/change_status_item/?id=<?=$item['id']?>&status=<?=$item['menu_status']?>&menu_id=<?=$item['menu_id']?>"><i class="icon-ok"></i></a>
                                    <?php else : ?>
                                        <a href="<?=URL_ADMIN?>menu/change_status_item/?id=<?=$item['id']?>&status=<?=$item['menu_status']?>&menu_id=<?=$item['menu_id']?>"><i class="icon-remove"></i></a>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" onclick="confirm_del('<?=URL_ADMIN?>menu/delete_item/<?=$item['id']?>', 'per_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a>
                                    <a href="<?=$link_edit?><?=$item['id']?>/<?=$item['menu_id']?>" class="icon-edit tip" title="Edit this item"></a>
                                    
                                </td>
                            </tr>
                            <?php endforeach ?>
                            <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>                                
    </div>
    <div class="dr"><span></span></div>
    <form method="post">
    <div class="row-fluid">
    	<div class="span6">
    		<div class="head clearfix">
                <div class="isw-plus"></div>
                <h1>Add new item</h1>
            </div>
            <div class="block-fluid">                        
            	<div class="row-form clearfix">
                    <div class="span2">Item name:</div>
                    <div class="span10">
                    	<input type="text" name="item_name" value=""/>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span2">Type url:</div>
                    <div class="span10">
                        <input type="radio" name="type_url" checked="checked" value="site_url"> Site url
                        <input type="radio" name="type_url" value="external_url"> External link
                        <input type="radio" name="type_url" value="page"> Page
                    </div>
                </div>
                <div class="row-form clearfix" id="link_normal">
                    <div class="span2">Item link:</div>
                    <div class="span10">
                    	<input type="text" name="item_link" value=""/>
                    </div>
                </div>

                <?php 
                    $this->db->where(array('pages.status' => 1, 'page_language.lang_code' => $this->admin_lang));
                    $this->db->join('page_language', 'page_language.page_id = pages.id', 'left');
                    $pages = $this->db->get('pages')->result();
                ?>

                <div class="row-form clearfix" id="link_with_page" style="display:none">
                    <div class="span2">Page:</div>
                    <div class="span10">
                        <select name="link_with_page">
                            <?php if(count($pages)) : ?>
                                <?php foreach($pages as $page) : ?>
                                    <option value="page/<?=$page->id?>/<?=$page->page_title_alias?>"><?=$page->page_title?></option>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span2">Item order:</div>
                    <div class="span10">
                    	<input type="text" name="item_order" value="1"/>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span2">Item status:</div>
                    <div class="span10">
                    	<select name="status">
                            <option value="1" selected="selected">Public</option>
                            <option value="0">No Public</option>
                        </select>
                    </div>
                </div>
                <div class="footer tar" style="text-align:left">
                    <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                </div>       
    		</div>
    	</div>
    </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("input[type='radio']").click(function(){
            var type = $(this).val();
            if(type == 'page') {
                $('#link_with_page').show();
                $('#link_normal').hide();    
            } else {
                $('#link_with_page').hide();
                $('#link_normal').show();    
            }
        })
        //var type_url = $("input[type='radio']:checked").val();
        //alert(type_url);
    });
</script>
 