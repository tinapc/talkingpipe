<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="row-fluid">
        
        <div class="span12">                    
            <div class="head clearfix">
                <div class="isw-grid"></div>
                <h1><?=$this->_page_header?></h1>      
                <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <li><a href="<?=$link_add?>"><span class="isw-plus"></span> Create new page</a></li>
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                            <!-- <li><a href="javascript:void(0)" onclick="delMulti('<?=current_url()?>/delMulti', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li> -->
                        </ul>
                    </li>
                </ul>                    
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="checkall"/></th>
                            <th width="5%">ID</th>
                            <th width="50%">Menu name</th>
                            <th width="30%">Action</th>                                   
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($body)) : ?>
                        <?php foreach($body as $item) : ?>
                            <tr id="per_<?=$item['id']?>">
                                <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>
                                <td><?=$item['id']?></td>                                 
                                <td><?=$item['menu_name']?></td> 
                                
                                <td>
                                    <a href="javascript:void(0)" onclick="confirm_del('<?=URL_ADMIN?>menu/delete/<?=$item['id']?>', 'per_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a>
                                    <a href="<?=$link_edit?><?=$item['id']?>" class="icon-edit tip" title="Edit this item"></a>
                                    <a href="<?=$link_add_item?><?=$item['id']?>" class="icon-th-list tip" title="Add item for this menu"></a>
                                </td>
                            </tr>
                            <?php endforeach ?>
                            <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>                                
        
    </div>
    <div class="dr"><span></span></div>

</div>
 