<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span2">Title:</div>
                        <div class="span10"><input value="<?=$info['title']?>" class="validate[required]" type="text" name="title" id="title"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Price:</div>
                        <div class="span10"><input value="<?=$info['price']?>" class="validate[required] " type="text" name="price" id="price"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Category:</div>
                        <div class="span10">
                            <select name="cate_id">
                                <?php if(count($cates)) : ?>
                                    <?php foreach($cates as $cate) : ?>
                                        <option value="<?=$cate['id']?>" <?php if($info['cate_id'] == $cate['id']) echo "selected=selected"?>><?=$cate['catelog_name']?></option>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Order:</div>
                        <div class="span10"><input value="<?=$info['order']?>" type="text" name="order" id="order"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div> 
                    <div class="row-form clearfix">
                        <div class="span2">Public:</div>
                        <div class="span10">
                            <select name="status">
                                <?php if($info['status'] == 1): ?>
                                    <option value="1" selected="selected">Public</option>
                                <?php else : ?>
                                    <option value="1">Public</option>
                                <?php endif?>
                                <?php if($info['status'] == 0): ?>
                                    <option value="0" selected="selected">No Public</option>
                                <?php else : ?>
                                    <option value="0">No Public</option>
                                <?php endif?>
                            </select>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Photo:</div>
                        <div class="span10"><input type="file" name="photo" id="photo"/> 
                            <input type="hidden" name="oldphoto" value="<?=$info['photo']?>">
                            <?php if($info['photo'] !== NULL) : ?> 
                                <img src="<?=base_url() . $info['photo']?>" width="50" height="50">
                            <?php endif ?> 
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Photo large:</div>
                        <div class="span10"><input type="file" name="photo_big" id="photo_big"/> 
                            <input type="hidden" name="oldphotobig" value="<?=$info['big_photo']?>">
                            <?php if($info['big_photo'] !== NULL) : ?> 
                                <img src="<?=base_url() . $info['big_photo']?>" width="50" height="50">
                            <?php endif ?> 
                        </div>
                    </div>                              
                    <div class="footer tar" style="text-align:left">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>