<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note']; ?>

    <div class="row-fluid">

        <div class="span12">                    
            <div class="head clearfix">
                <div class="isw-grid"></div>
                <h1><?= $this->_page_header ?></h1>      
                <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <li><a href="<?= URL_ADMIN ?>user_system/add"><span class="isw-plus"></span>Create new</a></li>
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
<!--                            <li><a href="javascript:void(0)" onclick="delMulti('<?= current_url() ?>/delMulti', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li>-->
                        </ul>
                    </li>
                </ul>                    
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="checkall"/></th>
                            <th width="5%">ID</th>
                            <th width="10%">Username</th>
                            <th width="10%">Email</th>
                            <th width="15%">Phone</th>
                            <th width="15%">Group</th>
                            <th width="20%">Created date</th>
                            <th width="10%">Status</th> 
                            <th width="35%">Action</th>                                   
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($body)) : ?>
                            <?php foreach ($body as $item) : ?>
                                <?php $roles = $this->config->item('role_admin'); ?>
                                <tr id="per_<?= $item['id'] ?>">
                                    <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?= $item['id'] ?>" /></td>
                                    <td><?= $item['id'] ?></td>                                 
                                    <td><?= $item['username'] ?></td>
                                    <td><?= $item['email'] ?></td>
                                    <td><?= $item['phone'] ?></td>
                                    <td><?= $roles[$item['group']] ?></td>
                                    <td><?= mdate("%d/%m/%Y", $item['created_date']) ?></td>    
                                    <td>
                                        <?php if ($item['status'] == 1) : ?>
                                            <a href="<?= URL_ADMIN . $url_change_status ?><?= $item['id'] ?>/<?= $item['status'] ?>"><i class="icon-ok"></i></a>
                                        <?php else : ?>
                                            <a href="<?= URL_ADMIN . $url_change_status ?><?= $item['id'] ?>/<?= $item['status'] ?>"><i class="icon-remove"></i></a>
                                        <?php endif ?>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="confirm_del('<?= URL_ADMIN ?>user_system/delete/<?= $item['id'] ?>', 'per_<?= $item['id'] ?>')" class="icon-trash tip" title="Delete this item"></a>
                                        <a href="<?= URL_ADMIN ?>user_system/edit/<?= $item['id'] ?>" class="icon-edit tip" title="Edit this item"></a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>                                

    </div>
    <div class="dr"><span></span></div>

</div>
