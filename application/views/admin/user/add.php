<div class="workplace">  

    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note']; ?>

    <div class="page-header">
        <h1><?= $this->_page_header ?></h1>
    </div> 

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1>Edit Profile</h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span3">Fullname:</div>
                        <div class="span9">
                            <input value="<?= @$_POST['fullname'] ?>" class="validate[required]" type="text" name="fullname" id="fullname"/> <!-- <span>Example: 2010-12-01</span> -->
                        </div>
                    </div>                         

                    <div class="row-form clearfix">
                        <div class="span3">E-mail:</div>
                        <div class="span9"><input value="<?= @$_POST['email'] ?>" class="validate[required, custom[email]]" type="text" name="email" id="email" />  <span>Example: someone@nowhere.com</span></div>
                    </div>  
                    <div class="row-form clearfix">
                        <div class="span3">Phone:</div>
                        <div class="span9">
                            <input value="<?= @$_POST['phone'] ?>" class="validate[required]" type="text" name="phone" id="phone" />
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Username:</div>
                        <div class="span9"><input value="<?= @$_POST['username'] ?>" class="validate[required]" type="text" name="username" id="username"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>   
                    <div class="row-form clearfix">
                        <div class="span3">Password:</div>
                        <div class="span9">        
                            <input value="" class="validate[maxSize[32]]" type="password" name="password" id="password"/>
                            <span>Maximum 32 characters</span>
                        </div>
                    </div> 
                    <div class="row-form clearfix">
                        <div class="span3">Passowrd Verify:</div>
                        <div class="span9">        
                            <input value="" class="validate[maxSize[32],equals[password]]" type="password" name="repassword" id="repassword"/>
                            <span>Maximum 32 characters</span>
                        </div>
                    </div>            
                    <div class="row-form clearfix">
                        <div class="span3">Public:</div>
                        <div class="span9">
                            <label class="checkbox inline">
                                <input type="radio" name="status" value="1" checked="checked"/> Yes
                            </label>
                            <label class="checkbox inline">
                                <input type="radio" name="status" value="0"/> No
                            </label>
                        </div>
                    </div>
                    
                    
                    <div class="footer tar">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 

                </form>
            </div>

        </div>
    </div>

</div>