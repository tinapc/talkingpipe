<div class="workplace">  

    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note']; ?>

    <div class="page-header">
        <h1>Edit Profile</h1>
    </div> 

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1>Edit Profile</h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span3">Fullname:</div>
                        <div class="span9"><input value="<?= $infoUser['fullname'] ?>" class="validate[required]" type="text" name="fullname" id="fullname"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>                         

                    <div class="row-form clearfix">
                        <div class="span3">E-mail:</div>
                        <div class="span9"><input value="<?= $infoUser['email'] ?>" class="validate[required,custom[email]]" type="text" name="email" id="email" />  <span>Example: someone@nowhere.com</span></div>
                    </div>                                                                                                 
                    <input type="hidden" name="oldpass" value="<?= $infoUser['password'] ?>"> 
                    <div class="row-form clearfix">
                        <div class="span3">Phone:</div>
                        <div class="span9"><input value="<?= $infoUser['phone'] ?>" class="validate[required]" type="text" name="phone" id="phone"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">New password:</div>
                        <div class="span9">        
                            <input value="" class="validate[maxSize[32]]" type="password" name="password" id="password"/>
                            <span>Maximum 32 characters</span>
                        </div>
                    </div> 
                    <div class="row-form clearfix">
                        <div class="span3">Passowrd Verify:</div>
                        <div class="span9">        
                            <input value="" class="validate[maxSize[32],equals[password]]" type="password" name="repassword" id="repassword"/>
                            <span>Maximum 32 characters</span>
                        </div>
                    </div>            
                    <div class="row-form clearfix">
                        <div class="span3">Avatar:</div>
                        <div class="span9">                                                                
                            <input type="file" name="avatar"/>
                            <input type="hidden" name="old_avatar" value="<?= $infoUser['avatar'] ?>">
                            <?php if ($infoUser['avatar'] !== '') : ?>
                                <a class="fancybox" href="img/example_full.jpg" rel="group" style="margin: 5px;">
                                    <img class="img-polaroid" src="<?= base_url() . $infoUser['avatar'] ?>">
                                </a>
                            <?php endif ?>
                        </div>
                    </div> 
                    
                    <?php $roles = $this->config->item('role_admin'); ?>
                        <div class="row-form clearfix">
                            <div class="span3">Public:</div>
                            <div class="span9">
                                <label class="checkbox inline">
                                    <input type="radio" name="status" value="1" <?php if($infoUser['status'] == 1) echo "checked=checked";?>/> Yes
                                </label>
                                <label class="checkbox inline">
                                    <input type="radio" name="status" value="0" <?php if($infoUser['status'] == 0) echo "checked=checked";?>/> No
                                </label>
                            </div>
                        </div>
                        
                    <div class="footer tar">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 

                </form>
            </div>

        </div>
    </div>

</div>