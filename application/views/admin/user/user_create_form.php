<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">

        <h3><?=t_lang('manager_user')?></h3>

        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab"><?=t_lang('create_new')?></a></li>
            <!--<li><a href="#tab2">Forms</a></li>-->
        </ul>

        <div class="clear"></div>

    </div> <!-- End .content-box-header -->
    <div class="content-box-content">

        <div class="tab-content default-tab" id="tab1">        
            <form action="<?=$txt['url_action'];?>" method="post" enctype="multipart/form-data" id="frm_add">
                <fieldset>
                	<table width="100%" class="form">
					<tbody>
						<tr>
           	  				<td width="169" align="left"><label>Username:</label></td>
          					<td width="922">
            					<input name="username" type="text" class="text-input small-input" id="username"/>
                				<?=form_error('username');?>
            				</td>
            			</tr>
			            <tr>
							<td align="left"><label>Password:</label></td>
			          		<td>
			            		<input name="password" type="password" id="password" class="text-input small-input" />
			                	<?=form_error('password');?>
			            	</td>
			            </tr>
			            <tr>
			            	<td align="left"><label>Re-password:</label></td>
			          		<td>
			            		<input name="confirm_password" type="password" id="confirm_password" class="text-input small-input" />
			                	<?=form_error('confirm_password');?>
			            	</td>
			            	</tr>
			            </tr>
						<tr>
				        	<td align="left"><label>Address email:</label></td>
				          	<td>
				            	<input name="user_email" type="text" id="user_email" class="text-input small-input" value="<?php echo set_value('user_email');?>" size="100" />
				                <?=form_error('user_email');?>
				            </td>
				            </tr>
						</tr>
						<tr>
			           		<td align="left"><label>Fullname:</label></td>
			          		<td>
			            		<input name="user_fullname" type="text" class="text-input small-input" id="user_fullname" value="<?php echo set_value('fullname');?>" size="100" /> 
			            	</td>
			            </tr>
			            <tr>
			           		<td align="left"><label>Actvie/No active:</label></td>
			          		<td>
			            		<input type="checkbox" name="active" checked="checked"  />
			                	<?=form_error('active');?>
			            	</td>
			            </tr>
			            <tr>
			           		<td align="left"><label>Group permission:</label></td>
			          		<td>
			            		<select name="user_group" id="user_group">
			                    	<?php foreach($groups as $group) : ?>
			                    	<option value="<?=$group->user_group_id;?>" <?php echo set_select('group', $group->user_group_id);?>><?=$group->user_group_name;?></option>
			                        <?php endforeach;?>
			                    </select>
			            	</td>
			            </tr>
					</tbody>
                </table>
             </fieldset>
			</form>
   		</div><!--End div 1-->
   </div>
    <div class="list-button">
		<a class="button" href="javascript:void(0);" onClick="$('#frm_add').submit();"><?=t_lang('btn_save')?></a>
        <a class="button" href="<?=$txt['url_cancel']?>"><?=t_lang('btn_cancel')?></a>
	</div>
</div>
