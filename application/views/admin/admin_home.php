<div class="workplace">
    
    <div class="row-fluid">        
    	<div class="span12">
            <div class="widgetButtons">   
            	<div class="bb"><a href="<?= URL_ADMIN ?>menu" class="tipb" title="Menu Manager"><span class="ibw-list"></span></a></div>                     
                <div class="bb"><a href="<?= URL_ADMIN ?>user_system" class="tipb" title="List members"><span class="ibw-user"></span></a></div>
                <div class="bb"><a href="<?= URL_ADMIN ?>book" class="tipb" title="Book Manager"><span class="ibw-documents"></span></a></div>
                <div class="bb"><a href="<?= URL_ADMIN ?>blog" class="tipb" title="Blog Manager"><span class="ibw-brush"></span></a></div>
                <div class="bb"><a href="<?= URL_ADMIN ?>slider" class="tipb" title="Slider Manager"><span class="ibw-picture"></span></a></div>
                <div class="bb"><a href="<?= URL_ADMIN ?>setting" class="tipb" title="Settings"><span class="ibw-settings"></span></a></div>
            </div>
        </div>
    </div>            
    <div class="dr"><span></span></div>
    <div class="row-fluid">
    	<div class="span12">
    		<div class="head clearfix">
                <div class="isw-grid"></div>
                <h1>Latest Order</h1>                   
            </div>
            <div class="block-fluid">
                <table cellpadding="0" cellspacing="0" width="100%" class="table">
                        <thead>
                            <tr>
                                <th></th>                                      
                                <th width="5%">Invoice</th>
                                <th width="15%">Transaction ID</th>
                                <th width="10%">Total</th>                
                                <th width="25%">Status</th>
                                <th width="25%">Created date</th>
                                <th width="35%">View</th>
                            </tr>
                        </thead>
                        <?php $orders = $this->db->limit(5)->get('order')->result(); ?>
                        <tbody>
                            <?php foreach ($orders as $order) : ?>
                                <tr>
                                	<td><?=$order->id?></td> 
                                    <td><?=$order->invoice?></td>                                  
                                    <td><?=$order->transaction?></td>
                                    <td>$<?=number_format($order->total, 2, '.', ',')?></td>     
                                    <td><?=$order->status?></td> 
                                    <td><?=$order->created_date?></td>
                                    <td>
                                    	<a href="<?=URL_ADMIN?>order/view/<?=$order->id?>">View</a>
                                    </td>
                                </tr>
                            <?php endforeach ?>                            
                        </tbody>
                </table>
            </div>
    	</div>
    </div>

</div>
