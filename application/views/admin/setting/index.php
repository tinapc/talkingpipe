<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1>Setting System</h1>
    </div>
    <form id="validation" method="POST" enctype="multipart/form-data">
    <div class="row-fluid">
       
        <div class="span6">
                <div class="head clearfix">
                    <div class="isw-settings"></div>
                    <h1>Global variable</h1>
                </div>
                <div class="block-fluid">                        

                    <div class="row-form clearfix">
                        <div class="span3">Site title:</div>
                        <div class="span9"><input type="text" name="site_title" value="<?=$this->setting->get_setting('site_title')?>"/></div>
                    </div>                         

                    <div class="row-form clearfix">
                        <div class="span3">Site description:</div>
                        <div class="span9"><textarea name="site_desc"><?=$this->setting->get_setting('site_desc')?></textarea></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Admin email:</div>
                        <div class="span9"><input type="text" name="email" value="<?=$this->setting->get_setting('email')?>"/></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Default language:</div>
                        <div class="span9">
                            <select name="default_language">
                                <?php foreach($this->config->item('language_support') as $key => $value) : ?>
                                <option value="<?=$value?>" <?php if($this->setting->get_setting('default_language') == $value) echo "selected=selected";?>><?=$key?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row-form clearfix">
                        <div class="span3">Slogon header:</div>
                        <div class="span9"><input type="text" name="slogon" value="<?=$this->setting->get_setting('slogon')?>"/> </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Facebook Page like:</div>
                        <div class="span9"><input type="text" name="page_like_box" value="<?=$this->setting->get_setting('page_like_box')?>"/> </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Facebook link:</div>
                        <div class="span9"><input type="text" name="page_fb" value="<?=$this->setting->get_setting('page_fb')?>"/> </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Google+ link:</div>
                        <div class="span9"><input type="text" name="page_google" value="<?=$this->setting->get_setting('page_google')?>"/> </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Twitter link:</div>
                        <div class="span9"><input type="text" name="page_tw" value="<?=$this->setting->get_setting('page_tw')?>"/> </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Logo Paypal</div>
                        <div class="span9">
                            <input type="file" name="logo" ><br>
                            <?php if($this->setting->get_setting('logo') !== NULL) : ?>
                                <img src="<?=base_url() . $this->setting->get_setting('logo')?>" width="100" height="100">
                            <?php endif ?>
                            <input type="hidden" name="oldLogo" value="<?=$this->setting->get_setting('logo')?>">
                        </div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">Main banner</div>
                        <div class="span9">
                            <input type="file" name="main_banner" ><br>
                            <?php if($this->setting->get_setting('main_banner') !== NULL) : ?>
                                <img src="<?=base_url() . $this->setting->get_setting('main_banner')?>" width="100" height="100">
                            <?php endif ?>
                            <input type="hidden" name="oldMainBanner" value="<?=$this->setting->get_setting('main_banner')?>">
                        </div>
                    </div>                                                                                            
                </div>
        </div>            
        <div class="span6">
            <div class="head clearfix">
                <h1>Paypal Config</h1>
            </div>
            <div class="block-fluid">                       
                <div class="row-form clearfix">
                    <div class="span4">Paypal Business Email:</div>
                    <div class="span6">
                        <input type="text" name="paypal_business_email" value="<?=$this->setting->get_setting('paypal_business_email')?>"/>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span4">Paypal production:</div>
                    <div class="span6">
                        <select name="paypal_production">
                            <?php if($this->setting->get_setting('paypal_production') == 1) : ?>
                                <option value="1" selected="selected">Live</option>
                            <?php else :?>
                                <option value="1">Live</option>
                            <?php endif?>
                            <?php if($this->setting->get_setting('paypal_production') == 0) : ?>
                                <option value="0" selected="selected">Test</option>
                            <?php else :?>
                                <option value="0">Test</option>
                            <?php endif?>
                        </select>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span4">Paypal partner:</div>
                    <div class="span6">
                        <input type="text" name="pf_partner" value="<?=$this->setting->get_setting('pf_partner')?>"/>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span4">Paypal Vendor:</div>
                    <div class="span6">
                        <input type="text" name="pf_vendor" value="<?=$this->setting->get_setting('pf_vendor')?>"/>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span4">Paypal User:</div>
                    <div class="span6">
                        <input type="text" name="pf_user" value="<?=$this->setting->get_setting('pf_user')?>"/>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span4">Paypal Password:</div>
                    <div class="span6">
                        <input type="password" name="pf_pwd" value="<?=$this->setting->get_setting('pf_pwd')?>"/>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span4">Discount rate cart (%):</div>
                    <div class="span6">
                        <input type="text" name="discount_rate_cart" value="<?=$this->setting->get_setting('discount_rate_cart')?>"/>
                        <span class="help">Default value is 0%</span>
                    </div>
                </div>
                <div class="row-form clearfix">
                    <div class="span4">Ship fee:</div>
                    <div class="span6">
                        <input type="text" name="ship_fee" value="<?=$this->setting->get_setting('ship_fee')?>"/>
                    </div>
                </div>
            </div>
        </div>           
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                    <div class="isw-cloud"></div>
                    <h1>Content partial</h1>
            </div>
            <div class="block-fluid">                        

                    <div class="row-form clearfix">
                        <div class="span3">Footer partial:</div>
                        <div class="span9">
                            <textarea name="content_partial_footer" id="cover_size"><?=$this->setting->get_setting('content_partial_footer')?></textarea>
                        </div>
                    </div>
                    
            </div>   
        </div>
    </div>
    <div class="footer tar" style="text-align:left">
        <button class="btn">Save change</button>
    </div>   
    </form>
</div>

<script type="text/javascript">

    tinyMCE.init({
    // General options
    mode : "exact",
    elements : "content_partial_footer",
    theme : "advanced",
    skin : "o2k7",
    relative_urls : false,
    convert_urls: false,
    plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,pastetext,pasteword,bullist,numlist,|,formatselect,fontselect,fontsizeselect, forecolor,backcolor,fullscreen",
    theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,fontselect,blockquote,|,undo,redo,|,link,unlink,image,media,|,code,preview",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    file_browser_callback: "ajaxfilemanager",

    /* Example content CSS (should be your site CSS)
    content_css : "css/content.css",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js",
    */
    // Replace values for the template plugin
    template_replace_values : {
        username : "Some User",
        staffid : "991234"
    }
    });

function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    switch (type) {
        case "image":
                break;
        case "media":
                break;
        case "flash":
                break;
        case "file":
                break;
        default:
            return false;
    }
    tinyMCE.activeEditor.windowManager.open({
        url: "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
        width: 782,
        height: 400,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });

}

</script>