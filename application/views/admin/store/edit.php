<div class="workplace">  

    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note']; ?>

    <div class="page-header">
        <h1>Edit</h1>
    </div> 

    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1>Edit</h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span3">Tên cửa hàng:</div>
                        <div class="span9"><input value="<?=$info['title']?>" class="validate[required]" type="text" name="title" id="title"/> 
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Địa chỉ:</div>
                        <div class="span9">
                            <input value="<?=$info['address']?>" class="validate[required]" type="text" name="address" id="address" placeholder="Địa chỉ"/>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Điện thoại:</div>
                        <div class="span9">
                            <input value="<?=$info['phone']?>" class="validate[required]" type="text" name="phone" id="phone" placeholder="Điện thoại"/>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Email:</div>
                        <div class="span9">
                            <input value="<?=$info['email'] ?>" class="validate[required]" type="text" name="email" id="email" placeholder="Email"/>
                        </div>
                    </div>
                    <div class="footer tar">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 

                </form>
            </div>

        </div>
    </div>

</div>
