<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="row-fluid">
		<div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1>Text for <?php if($type=='header')  echo 'Header'; else echo 'Footer';?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">
                	<input type="hidden" value="<?=$text_id?>" name="textid">
                    <div class="row-form clearfix">
                        <div class="span2">Select Page:</div>
                        <div class="span10">
                        	<select name="page" id="page" onchange="change_content(this.value)">
                        		<option value="home">---select page---</option>
                        		<?php foreach ($listpages as $key => $value) : ?>
                        			<option value="<?=$key?>" <?php if($page == $key) echo "selected=selected"; ?>><?=$value?></option>
                        		<?php endforeach ?>
                        		
                        	</select>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span2">Text:</div>
                        <div class="span10">
                        	<textarea id="text" style="height:300px" name="text"><?=$content?></textarea>
                        </div>
                    </div>                              
                    <div class="footer tar" style="text-align:left">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript">
<?php if($type == 'footer') : ?>

    tinyMCE.init({
    // General options
    mode : "exact",
    elements : "text",
    theme : "advanced",
    skin : "o2k7",
    relative_urls : false,
	convert_urls: false,
    plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,pastetext,pasteword,bullist,numlist,|,formatselect,fontselect,fontsizeselect, forecolor,backcolor,fullscreen",
    theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,fontselect,blockquote,|,undo,redo,|,link,unlink,image,media,|,code,preview",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    file_browser_callback: "ajaxfilemanager",

    /* Example content CSS (should be your site CSS)
    content_css : "css/content.css",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js",
    */
    // Replace values for the template plugin
    template_replace_values : {
        username : "Some User",
        staffid : "991234"
    }
    });
function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    switch (type) {
        case "image":
                break;
        case "media":
                break;
        case "flash":
                break;
        case "file":
                break;
        default:
            return false;
    }
    tinyMCE.activeEditor.windowManager.open({
        url: "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
        width: 782,
        height: 400,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });

}
<?php endif ?>


$(document).ready(function(){
	//change_content('home', '<?=$type?>');
});

function change_content(page, type) {
	window.location.href = baseJs + "d_text/?type=<?=$type?>&page=" + page;
}

function save(type) {
	$.ajax({
		type: "POST",
		dataType: "json",
		data: "page=" + $('#page').val() + "&type=" + type + "&text=" + tinyMCE.get('text').getContent(),
		url: "<?=URL_ADMIN?>d_text/save_content",
		success: function(data) {
			if(data.status) {
				alert(data.message);
			}	
		}
	})
}

</script>