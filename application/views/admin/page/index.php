<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="row-fluid">
    	
        <div class="span12">                    
            <div class="head clearfix">
                <div class="isw-grid"></div>
                <h1><?=$this->_page_header?></h1>      
                <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <li><a href="<?=$link_add?>"><span class="isw-plus"></span> Create new page</a></li>
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                            <!-- <li><a href="javascript:void(0)" onclick="delMulti('<?=current_url()?>/delMulti', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li> -->
                        </ul>
                    </li>
                </ul>                    
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="checkall"/></th>
                            <th width="5%">ID</th>
                            <th width="50%">Title</th>
                            <th width="15%">Status</th> 
                            <th width="30%">Action</th>                                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php if(count($body)) : ?>
                        <?php foreach($body as $item) : ?>
                            <tr id="per_<?=$item['id']?>">
                                <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>
                                <td><?=$item['id']?></td>                                 
                                <td><?=$item['page_title']?></td> 
                                <td>
                                    <?php if($item['status'] == 1) : ?>
                                        <a href="<?=URL_ADMIN?>page/change_status/?id=<?=$item['id']?>&type=<?=$type?>&status=<?=$item['status']?>"><i class="icon-ok"></i></a>
                                                    <?php else : ?>
                                        <a href="<?=URL_ADMIN?>page/change_status/?id=<?=$item['id']?>&type=<?=$type?>&status=<?=$item['status']?>"><i class="icon-remove"></i></a>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" onclick="confirm_del('<?=URL_ADMIN?>page/delete/<?=$item['id']?>', 'per_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a>
                                    <a href="<?=$url_edit?>?type=<?=$type?>&id=<?=$item['id']?>" class="icon-edit tip" title="Edit this item"></a>
                                </td>
                            </tr>
                            <?php endforeach ?>
                            <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>                                
    	
    </div>
    <div class="dr"><span></span></div>

</div>
 