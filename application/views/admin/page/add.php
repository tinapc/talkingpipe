<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
		<div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">

                    <div class="row-form clearfix">
                        <div class="span3">Title:</div>
                        <div class="span9"><input value="" class="validate[required]" type="text" name="title" id="title"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Order:</div>
                        <div class="span9"><input value="1" type="text" name="order" id="order"/> <!-- <span>Example: 2010-12-01</span> --></div>
                    </div> 
                    <div class="row-form clearfix">
                        <div class="span3">Public:</div>
                        <div class="span9">
                            <select name="status">
                                <option value="1" selected="selected">Public</option>
                                <option value="0">No Public</option>
                            </select>
                        </div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">Content:</div>
                        <div class="span9"><textarea id="content" style="height:300px" name="content" class="validate[required]"></textarea></div>
                    </div>                              
                    <div class="footer tar">
                        <button class="btn" onclick="$('#frm_add').submit()">Submit</button>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript">

    tinyMCE.init({
    // General options
    mode : "exact",
    elements : "content",
    theme : "advanced",
    skin : "o2k7",
    relative_urls : false,
	convert_urls: false,
    plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,pastetext,pasteword,bullist,numlist,|,formatselect,fontselect,fontsizeselect, forecolor,backcolor,fullscreen",
    theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,fontselect,blockquote,|,undo,redo,|,link,unlink,image,media,|,code,preview",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    file_browser_callback: "ajaxfilemanager",

    /* Example content CSS (should be your site CSS)
    content_css : "css/content.css",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js",
    */
    // Replace values for the template plugin
    template_replace_values : {
        username : "Some User",
        staffid : "991234"
    }
    });

function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    switch (type) {
        case "image":
                break;
        case "media":
                break;
        case "flash":
                break;
        case "file":
                break;
        default:
            return false;
    }
    tinyMCE.activeEditor.windowManager.open({
        url: "<?=base_url();?>tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
        width: 782,
        height: 400,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });

}


</script>