<div class="workplace">  
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="row-fluid">
    	
        <div class="span12">                    
            <div class="head clearfix">
                <div class="isw-grid"></div>
                <h1><?=$this->_page_header?></h1>      
                <ul class="buttons">                                                  
                    <li>
                        <a href="#" class="isw-settings"></a>
                        <ul class="dd-list">
                            <li><a href="<?=$link_add?>"><span class="isw-plus"></span>Add blog</a></li>
                            <!--<li><a href="#"><span class="isw-edit"></span> Edit</a></li>-->
                            <!-- <li><a href="javascript:void(0)" onclick="delMulti('<?=current_url()?>/delMulti', 'checkbox_del')"><span class="isw-delete"></span> Delete</a></li> -->
                        </ul>
                    </li>
                </ul>                    
            </div>
            <div class="block-fluid table-sorting clearfix">
                <table cellpadding="0" cellspacing="0" width="100%" class="table tSortable">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="checkall"/></th>
                            <th width="5%">ID</th>
                            <th width="30%">Title</th>
                            <th width="20%">Photo</th>
                            <th width="20%">Link to</th>
                            <th width="15%">Status</th> 
                            <th width="30%">Action</th>                                   
                        </tr>
                    </thead>
                    <tbody>
                    	<?php if(count($body)) : ?>
                        <?php foreach($body as $item) : ?>
                            <tr id="per_<?=$item['id']?>">
                                <td><input type="checkbox" name="checkbox_del" class="checkbox_del" value="<?=$item['id']?>" /></td>
                                <td><?=$item['id']?></td>
                                <td><?=$item['blog_title']?></td>
                                <td><img src="<?=base_url() . $item['photo']?>" width="50" height="50"></td>                                
                                <td><?=$item['link_to']?></td> 
                                <td>
                                    <?php if($item['status'] == 1) : ?>
                                        <a href="<?=URL_ADMIN?>blog/change_status/?id=<?=$item['id']?>&status=<?=$item['status']?>"><i class="icon-ok"></i></a>
                                                    <?php else : ?>
                                        <a href="<?=URL_ADMIN?>blog/change_status/?id=<?=$item['id']?>&status=<?=$item['status']?>"><i class="icon-remove"></i></a>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" onclick="confirm_del('<?=URL_ADMIN?>blog/delete/<?=$item['id']?>', 'per_<?=$item['id']?>')" class="icon-trash tip" title="Delete this item"></a>
                                    <a href="<?=$url_edit?><?=$item['id']?>" class="icon-edit tip" title="Edit this item"></a>
                                </td>
                            </tr>
                            <?php endforeach ?>
                            <?php endif ?>          
                    </tbody>
                </table>
            </div>
        </div>                                
    	
    </div>
    <div class="dr"><span></span></div>

</div>
 