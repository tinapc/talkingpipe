<link href="<?=theme_path?>css/legal.css" rel="stylesheet" media="screen"/>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<script type='text/javascript' src='<?=base_url()?>themes/admin/js/plugins/validation/jquery.validationEngine.js' charset='utf-8'></script>

<script type="text/javascript">
$(document).ready(function(){
    $("#frm-check").validationEngine({promptPosition : "topLeft", scroll: true});    
})

</script>

<?php 
$states = $this->db->get_where('subregions', array('region_id' => 840))->result();

$this->db->where_in('id', array('704','826','840','356','392','484','276','36','76','124','156','250','410','643','724'));
$countries = $this->db->get('regions')->result();
?>

<!--SECTION/BODY TEXT-->    
<div class="vert10 street5" align="center">
    <section class="vert8 street3 clear">   
        <article class="vert75 street3 text2 floatL" style="font-size:20px">
            <fieldset><legend style="padding-left:0px"><?=$this->lang->line('cart:checkout')?></legend></fieldset>
            <form method="post" id="frm-check">
                <div class="form-block">
                    <h4><?=$this->lang->line('cart:billinginfo')?></h4>
                    <p>
                        <label><?=$this->lang->line('cart:billtofirstname')?></label><br>
                        <input type="text" value="<?=@$_POST['first_name']?>" name="first_name" placeholder="First name">
                        <br><?=form_error('first_name')?>
                    </p>
                    <p>
                        <label><?=$this->lang->line('cart:billtolastname')?></label><br>
                        <input type="text" name="last_name" value="<?=@$_POST['last_name']?>" placeholder="Last name">
                        <br><?=form_error('last_name')?>
                    </p>
                    <div style="clear:both"></div>
                    <p>
                        <label><?=$this->lang->line('cart:billtoaddress')?></label><br>
                        <input type="text" name="address" placeholder="Address" value="<?=@$_POST['address']?>">
                        <br><?=form_error('address')?>
                    </p>
                    <p>
                        <label><?=$this->lang->line('cart:billtocity')?></label><br>
                        <input type="text" name="city" placeholder="City" value="<?=@$_POST['city']?>">
                        <br><?=form_error('city')?>
                    </p>
                    <div style="clear:both"></div>
                    <p>
                        <label><?=$this->lang->line('cart:billtocountry')?></label><br>
                        <select name="country" onchange="getstate(this.value)">
                            <option value="">---Select country---</option>
                            <?php foreach($countries as $country) : ?>
                                <option value="<?=$country->id?>"><?=$country->country?></option>
                            <?php endforeach ?>
                        </select>
                        <br><?=form_error('country')?>
                    </p>
                    <p>
                        <label><?=$this->lang->line('cart:billtostate')?></label><br>
                        <select name="state" id="state">
                            <?php foreach($states as $state) : ?>
                                <option value="<?=$state->name?>"><?=$state->name?></option>
                            <?php endforeach ?>
                        </select>
                        <br><?=form_error('state')?>
                    </p>
                    <p>
                        <label><?=$this->lang->line('cart:billtozipcode')?></label><br>
                        <input type="text" name="zipcode" placeholder="Post code" value="<?=@$_POST['zipcode']?>">
                        <br><?=form_error('zipcode')?>
                    </p>
                    <div style="clear:both"></div>
                    <p>
                        <label><?=$this->lang->line('cart:billtoemail')?></label><br>
                        <input type="text" name="email" placeholder="Email" value="<?=@$_POST['email']?>">
                        <br><?=form_error('email')?>
                    </p>
                    <p>
                        <label><?=$this->lang->line('cart:billtophone')?></label><br>
                        <input type="text" name="phone" placeholder="Phone" value="<?=@$_POST['phone']?>">
                        <br><?=form_error('phone')?>
                    </p>
                </div>
            </form>
            <div style="clear:both"></div>
            <a href="javascript:void(0)" onclick="$('#frm-check').submit()" class="btn" style="margin-left:0px;"><?=$this->lang->line('global:btn_submit')?></a>
        </article>
    </section>
</div>

<style type="text/css">
fieldset legend{font-size: 20px; text-transform: uppercase; font-family: 'Playball',cursive;}
fieldset{border-top: 1px solid #333; margin-bottom: 20px;}

.form-block{font-family: 'Playball',cursive; }
.form-block label{font-size: 15px;}
.form-block input, .form-block select{padding: 3px 10px; width:150px;}
.form-block p{float:left; margin-right: 20px; margin-bottom: 20px;}
span.error{font-size: 14px; color:#000;}

.btn {
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 5px;
    color: #333333;
    display: inline-block;
    padding: 8px 10px;
    text-decoration: none;
    margin-left: 20px;
    float:left;
    font-size: 16px;
}
</style>
<script type="text/javascript">
function getstate(cid){
    jQuery.ajax({
        type: "POST",
        dataType: "html",
        data: {"regionid" : cid},
        url: "<?=base_url()?>cart/getState",
        success: function(data) {
            jQuery('#state').html(data);
        }
    })
}
</script>