<link href="<?=theme_path?>css/legal.css" rel="stylesheet" media="screen"/>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>



<!--SECTION/BODY TEXT-->    
<div class="vert10 street5" align="center">
    <section class="vert8 street3 clear">   
    <article class="vert75 street3 text2 floatL" style="font-size:20px">
        <fieldset><legend style="padding-left:0px"><?=$this->lang->line('cart:yourshoppingcart')?></legend></fieldset>
                <?php if (count($this->cart->contents())) : ?>
                    <table class="tbl_cart table table-condensed tbl_cart">
                        <thead>
                            <th><?=$this->lang->line('cart:text_image')?></th>
                            <th><?=$this->lang->line('cart:text_productname')?></th>
                            <th><?=$this->lang->line('cart:text_qty')?></th>
                            <th><?=$this->lang->line('cart:text_price')?></th>
                            <th><?=$this->lang->line('cart:text_subtotal')?></th>
                            <th><?=$this->lang->line('cart:text_action')?></th>
                        </thead>
                        <tbody>

                            <?php foreach ($this->cart->contents() as $cart) : ?>
                                <tr>
                                    <td><img src="<?=base_url() . $cart['pro_image']?>" width="50" height="50"></td>
                                    <td align="top">
                                        <?=$cart['name']?>                                        
                                    </td>
                                    <td><?= $cart['qty'] ?></td>
                                    <td>$ <?= number_format($cart['price'], 2, '.', ',') ?></td>
                                    <td>$ <?php echo number_format(($cart['price'] * $cart['qty']),2, '.', ',') ?> <?=$this->config->item('icon_rate')?></td>
                                    <td class="action"><a href="javascript:void(0)" onclick="actionDelCart('<?=base_url()?>cart/delete/<?= $cart['rowid'] ?>')" class="showTip" title="Delete this item"><?=$this->lang->line('cart:text_delete')?></a></td>
                                </tr>
                            <?php endforeach ?>
                            <tr>
                                <td colspan="4" align="right"><strong><?=$this->lang->line('cart:text_ship_fee')?></strong></td>
                                <td colspan="1" align="left"><strong>$ <?php echo number_format($this->setting->get_setting('ship_fee'), 2, '.', ',') ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><strong><?=$this->lang->line('cart:text_total')?></strong></td>
                                <td colspan="1" align="left"><strong>$ <?php echo number_format($this->cart->total() + $this->setting->get_setting('ship_fee'), 2, '.', ',') ?> <?=$this->config->item('icon_rate')?></strong></td>
                            </tr>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5" style="padding-top:50px">
                                    <a href="<?= $this->fullPath ?>cart/checkout" style="float:left">
                                        <img src="http://www.american-vintage.net/sitebuilder/images/paypal_logo2-145x62.png" alt="Checkout with Paypal" width="80">
                                    </a>
                                    <a href="<?=base_url()?>" class="btn"><?=$this->lang->line('cart:continue_shopping')?></a>
                                    <!-- <a href="<?=base_url()?>" class="btn">Update cart</a> -->
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                <?php else : ?>
                    <p><?=$this->lang->line('cart:cart_is_empty')?></p>
                <?php endif ?>
 
    </article>  
    </section>
</div>

<style type="text/css">
fieldset legend{font-size: 20px; text-transform: uppercase; font-family: 'Playball',cursive;}
fieldset{border-top: 1px solid #333; margin-bottom: 20px;}
.tbl_cart{width: 100%; font-family: 'Playball',cursive;}
.tbl_cart td{padding: 10px; vertical-align: middle !important}
.tbl_cart tbody td{font-size: 15px; }
.tbl_cart td.action a{color:#f2f2f2; text-decoration: none}
.btn {
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 5px;
    color: #333333;
    display: inline-block;
    padding: 8px 10px;
    text-decoration: none;
    margin-left: 20px;
    float:left;
    font-size: 16px;
}
</style>


<script type="text/javascript">
    function actionDelCart(url) {
        var config = confirm('Are you sure that you want to delete this item?');
        if (config) {
            window.location.href = url;
        }
    }
</script>
<script type="text/javascript">
    $(function() {
        var img = $('ul.photo-thumb li:first').html();
        $('#box-photo-img').html(img);

        $('ul.related-product').carouFredSel({
            responsive: true,
            width: '100%',
            scroll: 2,
            auto: {
                pauseOnHover: 'resume',
                //progress: '#timer1'
            },
            items: {
                width: 300,
                // height: '30%',  //  optionally resize item-height
                visible: {
                    min: 2,
                    max: 6
                }
            },
            prev: '#prev',
            next: '#next'
        });
    });
</script>
