<link href="<?php echo theme_path?>css/main.css" rel="stylesheet" media="screen"/>
<!--SECTION/BODY IMG-->	
<div class="vert10 clear" align="center">
<section class="vert8">
	<img src="<?php echo base_url() . $this->setting->get_setting('main_banner')?>" width="100%" class="vert8 floatR top" alt="TalkingPipe Banner" longdesc="TalkingPipe%20Banner">

	<?php $slider1 = $this->db->get_where('slider_gallery', array('slider_id' => 2))->result() ?>
	<?php $slider2 = $this->db->get_where('slider_gallery', array('slider_id' => 4))->result() ?>
	<?php if(count($slider1)) : ?>
	<aside class="desktop1 floatL vert190">
		<ul class="bxslider vertical-slider">
			<?php foreach($slider1 as $sl1) : ?>  
			<li><img src="<?php echo base_url() . $sl1->image ?>"/></li>
			<?php endforeach ?>
		</ul>
	</aside>
	<?php endif?>

	<?php if(count($slider1)) : ?>
	<aside class="desktop2 floatL vert190">
		<ul class="bxslider vertical-slider">
			<?php foreach($slider1 as $sl1) : ?>  
			<li><img src="<?php echo base_url() . $sl1->image ?>"/></li>
			<?php endforeach ?>
		</ul>
	</aside>
	<?php endif?>


	<?php if(count($slider2)) : ?>
	<aside class="desktop1 floatL vert190 top2">
		<ul class="bxslider vertical-slider">
			<?php foreach($slider2 as $sl1) : ?>  
			<li><img src="<?php echo base_url() . $sl1->image ?>"/></li>
			<?php endforeach ?>
		</ul>
	</aside>
	<?php endif?>

	<?php if(count($slider2)) : ?>
	<aside class="desktop2 floatL vert190 top2">
		<ul class="bxslider vertical-slider">
			<?php foreach($slider2 as $sl1) : ?>  
			<li><img src="<?php echo base_url() . $sl1->image ?>"/></li>
			<?php endforeach ?>
		</ul>
	</aside>
	<?php endif?>

	<aside class="desktop floatL vert190 top2">
		<img src="<?php echo base_url() . $this->setting->get_setting('logo')?>" class="vert6" align="center"  />
	</aside>

</section>
</div>