<link href="<?=theme_path?>css/blogs.css" rel="stylesheet" media="screen"/>


<!--SECTION/BODY IMG-->	
<div class="desktop vert10 street9" align="center">
	<section class="vert8 street9">
		<?php if(count($blogs)) :?>
		<?php foreach($blogs as $blog) : ?>
			<div id="top" class="desktop vert8 street3 clearfix:after">
				<section>
					<img src="<?=base_url() . $blog['photo']?>" width="219" class="desktop vert3 street3 circle" alt="<?=$blog['blog_title']?>" longdesc="<?=$blog['blog_title']?>">	
				</section>
				<article>
				<h2><?=$blog['blog_title']?></h2>	
				<div><?=$blog['intro']?></div>
				<a href="<?=$blog['link_to']?>"><button class="css_button_top">Read!</button></a>
				</article>
			</div>
		<?php endforeach ?>
		<?php endif ?>
	</section>
</div> 

<div id="mobileBlogs" class="mobile vert10">
<a href="http://postubiquitous.talkingpipe.com"><button class="css_button_top">Post Ubiquitous!</button></a>
<a href="http://neurodecolonization.talkingpipe.com"><button class="css_button_middle">Neuro-Decolonization!</button></a>
<a href="http://guestcopy.talkingpipe.com"><button class="css_button_bottom">Guest Copy!</button></a>
</div>
