<link href="<?=theme_path?>css/contact.css" rel="stylesheet"/>
<link href="<?=theme_path?>css/contactMain.css" rel="stylesheet" media="screen"/>
<!--SECTION/BODY/CONTACT IMG-->	
<div class="vert10 street50" align="center">
		<section class="vert8">
	<div id="formframe" class="vert10">
		<div id="contact">
					<!-- FORM -->
					<form id="contact-form" class="wrapper"  method="post">
						<div>
						<label>
							<span><?=$this->lang->line('global:contact_name')?></span>
							<input name="name" placeholder="Please enter your name" type="text" tabindex="1" required autofocus>
						</label>
						</div>
						<div>
						<label>
							<span><?=$this->lang->line('global:contact_email')?></span>
							<input name="email" placeholder="Please enter your email address" type="email" tabindex="2" required>
						</label>
						</div>		
						<div>
						<label>
							<span><?=$this->lang->line('global:contact_message')?></span>
							<textarea name="message" placeholder="Include all the details you can" tabindex="5" required></textarea>
						</label>
						</div>
						<div>
							<button name="submit" type="submit" id="contact-submit" data-text="...Sending"><?=$this->lang->line('global:contact_btn_submit')?></button>
						</div>
					</form>
					<!-- /FORM -->
		</div> <!--id contact-->
	</div>
	</section>	
</div>		