<link href="<?php echo theme_path?>css/empire.css" rel="stylesheet" media="screen"/>


<div class="desktop vert10 street9" align="center">
    <section class="vert8 street9">
        <?php if(count($catelogs)) : ?>
        <?php foreach($catelogs as $cate) : ?>
        <div id="top" class="desktop vert8 clearfix:after marginL">
            <section>
                <img src="<?=base_url() . $cate['photo']?>" width="219" class="desktop vert3 street3 circle" alt="<?=$cate['catelog_name']?>" longdesc="<?=$cate['catelog_name']?>">           
            </section>
            <div style="clear:both"></div>
            <article>
                <h2><?=$cate['catelog_name']?></h2>
                <div>
                    <?=$cate['catelog_desc']?>
                </div>
                <a href="<?=base_url()?>category/<?=$cate['id']?>-<?=$cate['alias']?>"><button class="css_button_top">Enter!</button></a>
            </article>
        </div>
        <?php endforeach ?>
        <?php endif ?>
    </section>
</div>

<?php if(count($catelogs)) : ?>
    <?php foreach($catelogs as $cate) : ?>
    <div id="mobile" class="mobile vert10">
        <a href="<?=base_url()?>category/<?=$cate['id']?>-<?=$cate['alias']?>"><button class="css_button_top"><?=$cate['catelog_name']?></button></a>
    </div>
    <?php endforeach ?>
<?php endif ?>