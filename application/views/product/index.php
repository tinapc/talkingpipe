<div class="span7">
    <div class="row-fluid list-services haha" style="padding-top:10px">
        <?php foreach ($cates as $cate) : ?>
            <a href="<?= $this->fullPath ?>product-category/<?= $cate['id'] ?>_0-<?= $cate['alias'] ?>" class="a-in-list"><?= $cate['title'] ?></a>

            <?php $products = $proByCate[$cate['id']]; ?>
            <?php if (count($products)) : ?>
                <div class="row-fluid row-list-product">
                    <?php foreach ($products as $pro) : ?>
                        <?php
                        $photoPro = $this->product_model->getRandomPhoto($pro['id']);
                        if ($photoPro != "")
                            $imgPro = base_url() . $photoPro;
                        else
                            $imgPro = $this->no_image;
                        ?>

                        <div class="span6">
                            <div class="item-service">
                                <a href="<?php echo $this->fullPath ?>product/detail/<?= $pro['id'] ?>-<?= $pro['alias'] ?>"><img src="<?php echo $imgPro ?>" title="<?=$pro['title']?>"></a>
                                <p style="height:25px; overflow:hidden" class="name_item">
                                
                                <div class="mark">
                                    <a class="title" href="<?php echo $this->fullPath ?>product/detail/<?= $pro['id'] ?>-<?= $pro['alias'] ?>"><?= $pro['title'] ?></a>                          
                                </div>
                            </div> 
                        </div>
                    <?php endforeach ?>
                </div>
            <?php else : ?>
                <p>Đang cập nhật</p>
            <?php endif ?>
            <div class="clearfix" style="margin-bottom:20px"></div>
        <?php endforeach ?>
    </div>
</div>