<link href="<?=theme_path?>css/art.css" rel="stylesheet" media="screen"/>
<script type="text/javascript" src="<?=theme_path?>js/jquery.elevatezoom.min.js"></script>
<script src="<?=theme_path?>js/zoom.js"></script>

<!--SECTION/BODY IMG-->	
<div class="vert10 desktop clear" align="center">
	<section class="vert8">
		<?php if(count($products)) : ?>
		<div class="vert10 clear">
			<?php foreach($products as $product) : ?>
				
					<div id="topL" class="vert45">
							<div class="desktop vert6 floatL">
							<img style="border-radius:0px; -webkit-border-radius:0px" id="red_<?=$product['id']?>" data-zoom-image="<?=base_url() . $product['big_photo']?>" src="<?=base_url() . $product['photo']?>" style="max-width:280px; max-height:400px" class="desktop vert11" alt="<?=$product['title']?>">
							</div>
							<div class="desktop vert6 floatL">
								<article class="vert5 marginT clear"><p><?=$product['title']?></p><p>$<?=$product['price']?></p><p><?=$this->lang->line('cart:text_ship_include')?></p></article>
								<div class="clear" style="margin-bottom:20px;"></div>
								<a href="<?=base_url()?>cart/add/artist/<?=$product['id']?>">
								<button class="css_button vert5 floatL clear marginT" style="margin-top:0px !important;"><?=$this->lang->line('cart:btn_buynow')?></button>
								</a>
								<a href="javascript:void(0)" onclick="add_to_cart('artist', '<?=$product['id']?>')">
								<button class="css_button vert5 floatL clear marginT" style="margin-top:0px !important;"><?=$this->lang->line('cart:btn_addtocart')?></button>
								</a>
								<a href="<?=base_url()?>cart">
								<button class="css_button vert5 floatL clear marginT" style="margin-top:0px !important;"><?=$this->lang->line('cart:btn_viewcart')?></button>
								</a>
							</div>
					</div>
					<script type="text/javascript">
					$(document).ready(function () {
						$("#red_<?=$product['id']?>").elevateZoom({constrainSize:274, zoomType: "lens", containLensZoom: true, lensShape : "round", lensSize:130, zoomWindowFadeIn: 500,zoomWindowFadeOut: 750, scrollZoom : true});
					
					}); 
					</script>
				
				
			<?php endforeach?>
			</div>
		<?php else : ?>
		<div class="vert10 clear" style="min-height:300px">We are updating...</div>
		<?php endif?>
	</section>
</div>


<?php if(count($products)) : ?>
		<div id="mobileArt" class="vert10 mobile clear">
			<?php foreach($products as $product) : ?>
		
			<div id="mobileBuy" class="mobile vert10">
				<a href="<?=base_url()?>cart/add/artist/<?=$product['id']?>"><button class="css_button"><?=$this->lang->line('cart:btn_buynow')?></button></a>
				<ahref="javascript:void(0)" onclick="add_to_cart('artist', '<?=$product['id']?>')"><button class="css_button"><?=$this->lang->line('cart:btn_addtocart')?></button></a>
				<a href="<?=base_url()?>cart"><button class="css_button"><?=$this->lang->line('cart:btn_viewcart')?></button></a>
			</div>
			<div id="one" class="vert5 mobile clear" align="center">
				<img src="<?=base_url() . $product['big_photo']?>"  src="<?=base_url() . $product['photo']?>" width="500" class="mobile vert5"  alt="<?=$product['title']?>"
			 longdesc="<?=$product['title']?>">
			</div>
			<article class="vert10"><em><?=$product['title']?></em><p>$<?=$product['price']?> Shipping Included</p></article>
			<?php endforeach ?>
		</div>
		<?php else : ?>
			<div class="vert10 clear" style="min-height:300px">We are updating...</div>	
		<?php endif ?>


<script type="text/javascript">
function add_to_cart(type, pid) {
	$.ajax({
		type: "POST",
		dataType: "json",
		url:"<?=base_url()?>cart/addCartAjax/" + type + "/" + pid,
		success:function(data) {
			if(data.status) {
				alert('The Item has been added to cart');
			} else {
				alert('You have a error. Pls try again');
			}
		}
	})
}
</script>
	
	