<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart extends public_controller {

    public function __construct() {
        parent:: __construct();
        $this->load->model('product/product_model');
        $this->load->model('region/region_model');
    }

    public function index() {
        $url_update_cart = site_url('cart/update_cart');
        $this->template->title('Your shopping cart')
                ->set('url_update', $url_update_cart)
                ->build('cart/index');
    }

    public function addCartAjax() {
        $this->config->set_item('compress_output', FALSE);

        $type = $this->uri->segment(3);
        $pid = (int)$this->uri->segment(4);

        if($type == 'artist') {
            $product = $this->db->get_where('product', array('id' => $pid));    
        } else {
            $this->db->where(array('books.id' => $pid));
            $this->db->join('books_language', 'books_language.book_id = books.id', 'left');
            $product = $this->db->get('books');
        }

        if ($product->num_rows() == 0) {
            $mess = array('status' => FALSE);
        } 

        $proInfo = $product->row();

        $cart = array(
            'id' => $proInfo->id,
            'pro_image' => $proInfo->photo,
            'qty' => 1,
            'price' => $proInfo->price,
            'name' => $proInfo->title
        );

        if ($this->cart->insert($cart)) {
            $mess = array('status' => TRUE);
        } else {
            $mess = array('status' => FALSE);
        }
        echo json_encode($mess);
    }

    public function add() {
        $this->config->set_item('compress_output', FALSE);

        $type = $this->uri->segment(3);
        $pid = (int)$this->uri->segment(4);

        if($type == 'artist') {
            $product = $this->db->get_where('product', array('id' => $pid));    
        } else {
            $this->db->where(array('books.id' => $pid));
            $this->db->join('books_language', 'books_language.book_id = books.id', 'left');
            $product = $this->db->get('books');
        }

        if ($product->num_rows() == 0) {
            $mess = array('status' => FALSE);
        } 

        $proInfo = $product->row();

        $cart = array(
            'id' => $proInfo->id,
            'pro_image' => $proInfo->photo,
            'qty' => 1,
            'price' => $proInfo->price,
            'name' => $proInfo->title
        );

        $this->cart->insert($cart);
        redirect('cart');
    }

    public function delete() {
        $rowid = $this->uri->segment(3);
        $arr = array(
            'rowid' => $rowid,
            'qty' => 0
        );

        $this->cart->update($arr);
        redirect('cart');
    }

    public function update_cart() {
        $qty = $this->input->post('qty');
        $item = $this->input->post('rowid');

        $data = array();

        $total = count($this->cart->contents());
        //echo $total;

        for ($i = 0; $i < $total; $i++) {
            $data[] = array(
                'rowid' => $item[$i],
                'qty' => (int) $qty[$i]
            );


            $this->cart->update($data);
        } //End for
        //echo "<pre>"; echo print_r($data); echo "</pre>";
        redirect('gio-hang');
    }

    public function payment() {
        $this->template->title('Thanh toán', get_setting('site_name'))
                ->build('cart/payment');
    }
    
    public function checkout() {

        if($_POST) {
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('first_name', 'First name', 'required');
            $this->form_validation->set_rules('last_name', 'Last name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'required|numeric');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('zipcode', 'Zipcode', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');

            if($this->form_validation->run() == TRUE) {
                $bill_info = array(
                    'first_name'    => $this->input->post('first_name'),
                    'last_name'    => $this->input->post('last_name'),
                    'address'    => $this->input->post('address'),
                    'city'    => $this->input->post('city'),
                    'country'    => $this->region_model->getIsoRegion($this->input->post('country')),
                    'state'    => $this->input->post('state'),
                    'zipcode'    => $this->input->post('zipcode'),
                    'email'    => $this->input->post('email'),
                    'phone'    => $this->input->post('phone'),
                );

                $this->session->unset_userdata($bill_info);
                $this->session->set_userdata($bill_info);
                redirect('cart/process');
            }
        }

        $this->template->title('Checkout step 1')
                ->build('cart/checkout');
    }


    public function step2(){
        if ($this->session->userdata('logged_status') == "") redirect('login');

        $this->template->title('Xác nhận thông tin thanh toán')
                ->build('cart/step2');    
    }


    public function process(){
        $this->load->helper('string');

        $total = $this->cart->total() + $this->setting->get_setting('ship_fee');
        $amount = number_format($total, 2, '.', ',');

        $PF_USER = $this->setting->get_setting('pf_user'); 
        $PF_VENDER = $this->setting->get_setting('pf_vendor');  
        $PF_PARTNER =  $this->setting->get_setting('pf_partner');
        $PF_PWD =  $this->setting->get_setting('pf_pwd');
        
        if($this->setting->get_setting('paypal_production') == 0) {
            $PF_HOST_ADD = "https://pilot-payflowpro.paypal.com"; //Test mode
            $PF_MODE = 'TEST';  
        } else {
            $PF_MODE = 'LIVE';
            $PF_HOST_ADD = "https://payflowpro.paypal.com"; //Test mode
        }
        

        $amt = $amount; //Get amount of order
        $tokenID = uniqid('', true); //Create an token id
    

        $postData = "USER=". $PF_USER
                    . "&VENDOR=" . $PF_VENDER
                    . "&PARTNER=" . $PF_PARTNER
                    . "&PWD=" . $PF_PWD

                    . "&SECURETOKENID=" . $tokenID
                    . "&CREATESECURETOKEN=Y"
                    . "&TRXTYPE=S"
                    . "&AMT=" . $amt

                    . "&BILLTOFIRSTNAME=" . $this->session->userdata('first_name')
                    . "&BILLTOLASTNAME=" . $this->session->userdata('last_name')
                    . "&BILLTOSTREET=" . $this->session->userdata('address')
                    . "&BILLTOCITY=" . $this->session->userdata('city')
                    . "&BILLTOSTATE=" . $this->session->userdata('state')
                    . "&BILLTOZIP=" . $this->session->userdata('zipcode')
                    . "&BILLTOCOUNTRY=" . $this->session->userdata('country')
                    . "&EMAIL=" . $this->session->userdata('email')
                    . "&PHONENUM=" . $this->session->userdata('phone')
                    . "&SHIPTOFIRSTNAME=" . $this->session->userdata('first_name')
                    . "&SHIPTOLASTNAME=" . $this->session->userdata('last_name')
                    . "&SHIPTOSTREET=" . $this->session->userdata('address')
                    . "&SHIPTOCITY=" . $this->session->userdata('city')
                    . "&SHIPTOSTATE=" . $this->session->userdata('state')
                    . "&SHIPTOZIP=" . $this->session->userdata('zipcode')
                    . "&SHIPTOCOUNTRY=" . $this->session->userdata('country')
                    . "&SHIPTOEMAIL=" . $this->session->userdata('email')
                    . "&SHIPTOPHONENUM=" . $this->session->userdata('phone')
                    . "&INVNUM=" . random_string('numeric', 6);

        //echo $postData;
        //Curl setup
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $PF_HOST_ADD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $res = curl_exec($ch);

        if(!$res) {
            redirect('');
        }

        parse_str($res, $arr);

        if($arr['RESULT'] != 0) {
            die($arr['RESULT']);
            return "<p>TO order, pls contact us</p>";
            redirect('');
        }
        //print_r($arr);
        //echo '<iframe src="https://payflowlink.paypal.com?mode='.$PF_MODE.'&SECURETOKEN='.$arr['SECURETOKEN'].'&SECURETOKENID='.$tokenID.'" width="490" height="500" scrolling="no" frameborder="0" border="0" allowtransparency="true"></iframe>';

        $aData = array(
            'mode'  => $PF_MODE,
            'securetoken'   => $arr['SECURETOKEN'],
            'tokenId'   => $tokenID
        );

        $this->load->view('cart/process', $aData);
    }


    public function success(){
        // $result = print_r($this->input->post(), TRUE);
        // echo "<pre>";
        // print_r($result);
        // echo "</pre>";
        $received_post[] = $this->input->post();

        $arr = array(
            'invoice'       => $received_post[0]['INVOICE'],
            'transaction'   => $received_post[0]['PNREF'],
            'total'  => $this->cart->total() + $this->setting->get_setting('ship_fee'),
            'status' => 'Completed',
            'created_date' => $received_post[0]['TRANSTIME'],
        );
        $this->db->insert('order', $arr);
        $order_id = $this->db->insert_id();

        foreach ($this->cart->contents() as $cart) {
            $pro = array(
                'order_id'  => $order_id,
                'product_name' => $cart['name'],
                'product_quality' => $cart['qty'],
                'price' => number_format($cart['price'],2, '.', ','),
                'subtotal'  => number_format(($cart['price'] * $cart['qty']),2, '.', ',')
            );

            $this->db->insert('order_info', $pro);
        }

        $payer = array(
            'order_id'  => $order_id,
            'first_name' => $received_post[0]['BILLTOFIRSTNAME'],
            'last_name' => $received_post[0]['BILLTOLASTNAME'],
            'email' => $received_post[0]['BILLTOEMAIL'],
            'phone' => $received_post[0]['PHONE'],
            'address' => $received_post[0]['BILLTOSTREET'].', '.$this->session->userdata('city').', '.$this->session->userdata('state').', '. $this->session->userdata('country'),
            'zipcode' => $received_post[0]['BILLTOZIP'] 
        );

        $this->db->insert('payer', $payer);

        $this->cart->destroy();

        $bill_info = array(
            'first_name'    => '',
                    'last_name'    => '',
                    'address'    => '',
                    'city'    => '',
                    'country'    => '',
                    'state'    => '',
                    'zipcode'    => '',
                    'email'    => '',
                    'phone'    => '',   
        );

        $this->session->unset_userdata($bill_info);

        $this->load->view('cart/success_1');
    }

    public function complete(){
        $this->template->title('Payment completed')
            ->build('cart/complete');
    }

    public function order_cancel(){

    }

    public function getState(){
        $id = $this->input->post('regionid');
        $qr = $this->db->select('id, name')->get_where('subregions', array('region_id' => $id))->result();
        if (count($qr)) {
            echo '<option value="">------------------</option>';
            foreach ($qr as $r) {
                echo '<option value="' . $r->name . '">' . $r->name . '</option>';
            }
        } else {
            echo '<option value="">------------------</option>';
        }
    }
    

}

/* End file */