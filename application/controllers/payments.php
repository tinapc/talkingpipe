<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends public_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('string');
	}

	public function do_purchase($type, $id2)
	{
		//Get product info
		if($type == 'artist') {
			$product = $this->db->get_where('product', array('id' => $id2));	
		} else {
			$this->db->where(array('books_language.book_id' => $id2));
			$this->db->join('books_language', 'books_language.book_id = books.id', 'left');
			$product = $this->db->get('books');
		}
		
		if($product->num_rows() == '' ) {
			redirect();
		} else {

			$productInfo = $product->row();
			if($type == 'artist') {
				$product_item = $productInfo->title;
				$product_price = $productInfo->price;
			} else {
				$product_item = $productInfo->title;
				$product_price = $productInfo->price;
			}

			$config['business']                 = $this->setting->get_setting('paypal_business_email');
	        $config['cpp_header_image']         = ''; //Image header url [750 pixels wide by 90 pixels high]
	        $config['return']                   = 'http://localhost/dev.talking.com/payments/notify_payment';
	        $config['cancel_return']            ='http://localhost/dev.talking.com';
	        $config['notify_url']               = 'process_payment.php'; //IPN Post
	        if($this->setting->get_setting('paypal_production') == 0) {
	        	$config['production']               = FALSE; //Its false by default and will use sandbox
	    	} else {
	    		$config['production']               = TRUE; //Its false by default and will use sandbox
	    	}
	        $config['discount_rate_cart']       = $this->setting->get_setting('discount_rate_cart'); //This means 20% discount
	        $config["invoice"]                  = random_string('numeric', 6); //The invoice id
	               
	        $this->load->library('paypal',$config);
	               
	        #$this->paypal->add(<name>,<price>,<quantity>[Default 1],<code>[Optional]);
	                
	        $this->paypal->add($product_item,$product_price, 1); //First item
	        //$this->paypal->add('Pants',40);           //Second item
	        //$this->paypal->add('Blowse',10,10,'B-199-26'); //Third item with code
	                
	        $this->paypal->pay(); //Proccess the payment
		}
		
	}

	public function notify_payment(){
		$received_post[] = $this->input->post();
		// echo "<pre>";
		// print_r($received_post[0]);
		// echo "</pre>";
		
		$data = array(
			'invoice'	=> $received_post[0]['invoice'],
			'total'		=> $received_post[0]['payment_gross'],
			'status'	=> $received_post[0]['payment_status'],
			'created_date' => $received_post[0]['payment_date']
		);

		//Store to order table
		$this->db->insert('order', $data);
		$order_id = $this->db->insert_id();

		$data_pro = array(
			'order_id' => $order_id,
			'product_name' => $received_post[0]['item_name1'],
			'product_quality' => $received_post[0]['quantity1'],
			'price'	=> $received_post[0]['mc_gross_1'],
			'subtotal' => $received_post[0]['mc_gross_1'] * $received_post[0]['quantity1']
		);

		$this->db->insert('order_info', $data_pro);

		//Info payer
		$payer = array(
			'order_id'	=> $order_id,
			'first_name' => $received_post[0]['first_name'],
			'last_name'	 => $received_post[0]['last_name'],
			'email'	 => $received_post[0]['payer_email'],
			'address' => $received_post[0]['address_country'] .',' .$received_post[0]['address_state'].','.$received_post[0]['address_city'] .','. $received_post[0]['address_street'],
			'zipcode' => $received_post[0]['address_zip']
		);

		$this->db->insert('payer', $payer);

		//Send an email to this payer
		$this->load->library('parser');
		$this->loadMail();
        $this->email->from($this->setting->get_setting('email'), 'Payment successful');
        $this->email->to($payer['email']);
        //$this->email->to($to);
        $this->email->subject('Payment successful');
        $this->email->message($this->parser->parse('payment/send_email_payment_success', array('payer_email' =>$payer['email'])));
        $this->email->set_newline("\r\n");
        $this->email->send();

        $this->template->title('Payment successful')
        	->set('payer_email', $payer['email'])
        	->build('payment/success');
	}

}

/* End of file payments.php */
/* Location: ./application/controllers/payments.php */