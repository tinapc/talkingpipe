<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends admin_controller {

	protected $tbl_blog = 'blog';
	protected $tbl_blog_lang = 'blog_language';

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->breadcrumb = array(
            'Admin panel' => '',
            'Blogs' => ''
        );

		//Get all blogs
		$this->db->order_by('id', 'desc');
		$this->db->join($this->tbl_blog_lang, $this->tbl_blog.'.id = '.$this->tbl_blog_lang. '.blog_id');
		$this->db->where($this->tbl_blog_lang. '.lang_code', $this->admin_lang);
		$sql = $this->db->get($this->tbl_blog)->result_array();
		$this->_page_header = 'Blogs';

		$this->template->title()
			->set('body', $sql)
			->set('link_add', URL_ADMIN.'blog/add')
			->set('url_edit', URL_ADMIN.'blog/edit/')
			->set('link_del', URL_ADMIN.'blog/delete/')
			
			->build('admin/blog/index');	
	}

	public function add(){
		$this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Blog' => URL_ADMIN . 'blog',
            'Add blog' => ''
        );

        if ($this->input->post()) {

        	//upload photo
        	if (!empty($_FILES['photo']['name'])) {
	            $config['upload_path'] = './uploads/image/';
	            $config['allowed_types'] = 'gif|jpg|png';
	            $config['remove_spaces'] = TRUE;
	            $this -> load -> library('upload', $config);

	            if (!$this -> upload -> do_upload('photo')) {
	                $photo = NULL;
	            } else {
	                $up = $this -> upload -> data();
	                $photo = 'uploads/image/' . $up['file_name'];
	                $imageWidth = $up['image_width'];
	                // if ($imageWidth > 200) {
	                //     $this -> load -> library('image_lib');
	                //     $config['image_library'] = 'GD2';
	                //     $config['source_image'] = $up['full_path'];
	                //     $config['quality'] = 75;
	                //     $config['maintain_ratio'] = TRUE;
	                //     $config['master_dim'] = 'auto';
	                //     $config['width'] = 200;
	                //     $config['height'] = 250;

	                //     $this -> image_lib -> initialize($config);
	                //     $this -> image_lib -> resize();
	                // }
	                //@unlink($oldLogo);
	            }
	        } else {
	            $photo = NULL;
	        }
            
	        $_data = array(
	        	'link_to'	=> $this->input->post('link_to'),
	        	'status'	=> $this->input->post('status'),
	        	'order'	=> $this->input->post('order'),
	        	'photo'	=> $photo,
	        );

	        $this->db->insert($this->tbl_blog, $_data);
	        $blogid = $this->db->insert_id();

	        foreach (config_item('language_support') as $key => $value) {
		        $_data2 = array(
		        	'blog_id'	=> $blogid,
		        	'blog_title' => $this->input->post('title'),
		        	'intro'		=> $this->input->post('intro'),
		        	'lang_code'	=> $value
		        );

		        $this->db->insert($this->tbl_blog_lang, $_data2);
	    	}
            $this->message('success', "New record has been added succussful");
            redirect(URL_ADMIN . 'blog');
        }

        $this->_page_header = 'Add blog';

        $this->template->title()
                ->build('admin/blog/add');
	}


	/*
	* Edit blog
	* @Return bool
	*
	*/
	public function edit($id){
		if(!$id && $id =='') redirect(URL_ADMIN.'blog');

		$this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Blog' => URL_ADMIN . 'blog',
            'Edit blog' => ''
        );

        if ($this->input->post()) {

        	//Old photo
        	$oldPhoto = $this->input->post('oldphoto');

        	//upload photo
        	if (!empty($_FILES['photo']['name'])) {
	            $config['upload_path'] = './uploads/image/';
	            $config['allowed_types'] = 'gif|jpg|png';
	            $config['remove_spaces'] = TRUE;
	            $this -> load -> library('upload', $config);

	            if (!$this -> upload -> do_upload('photo')) {
	                $photo = $oldPhoto;
	            } else {
	                $up = $this -> upload -> data();
	                $photo = 'uploads/image/' . $up['file_name'];
	                $imageWidth = $up['image_width'];
	                // if ($imageWidth > 200) {
	                //     $this -> load -> library('image_lib');
	                //     $config['image_library'] = 'GD2';
	                //     $config['source_image'] = $up['full_path'];
	                //     $config['quality'] = 75;
	                //     $config['maintain_ratio'] = TRUE;
	                //     $config['master_dim'] = 'auto';
	                //     $config['width'] = 200;
	                //     $config['height'] = 250;

	                //     $this -> image_lib -> initialize($config);
	                //     $this -> image_lib -> resize();
	                // }
	                @unlink($oldPhoto);
	            }
	        } else {
	            $photo = $oldPhoto;
	        }
            
	        $_data = array(
	        	'link_to'	=> $this->input->post('link_to'),
	        	'status'	=> $this->input->post('status'),
	        	'order'	=> $this->input->post('order'),
	        	'photo'	=> $photo,
	        );

	        $this->db->update($this->tbl_blog, $_data, array('id' => $id));
		    $_data2 = array(
		      	'blog_title' => $this->input->post('title'),
		       	'intro'		=> $this->input->post('intro'),
		    );

		    $this->db->update($this->tbl_blog_lang, $_data2, array('blog_id' => $id, 'lang_code' => $this->admin_lang));

            $this->message('success', "New record has been updated");
            redirect(URL_ADMIN . 'blog');
        }

        $this->_page_header = 'Edit blog';

        //Get blog info
        $this->db->where(array('blog_language.lang_code' => $this->admin_lang, 'blog_id' => $id));
        $this->db->join('blog_language', 'blog_language.blog_id = blog.id', 'left');
        $sql = $this->db->get('blog')->row_array();	

        $this->template->title()
        		->set('blog', $sql)
                ->build('admin/blog/edit');

	}
	/*
	* Delete blog
	* @Return string
	*
	*/
	public function delete(){
		$id = (int)$this->uri->segment(4);

		$photo = $this->db->select('photo')->get_where($this->tbl_blog, array('id' => $id))->row();
		if($photo->photo !== NULL) @unlink($photo->photo);
		$this->db->delete($this->tbl_blog_lang, array('blog_id' => $id));
		$this->db->delete($this->tbl_blog, array('id' => $id));

		echo json_encode(array('status' => TRUE));
	}


	public function change_status() {
        $id = (int) $this->input->get('id');
        $status = (int) $this->input->get('status');
        if ($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update($this->tbl_blog, array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN . 'blog');
    }

}

/* End of file blog.php */
/* Location: ./application/controllers/admin/blog.php */