<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();

        $this->load->model('user/user_model', 'user');

        $this->lang->load('admin');
    }
	
    function index()
    {
        if($this->session->userdata('logined') == TRUE) {

            $data = array (
                'user_id' 		=> '',
                'username' 		=> '',
                'group' 		=> '',
                'sess_avatar'   => '',
                'logined'		=> FALSE
            );

            $this->session->unset_userdata($data);

            //$this->session->set_flashdata('error', t_lang('logout_success'));

            redirect('/admin/login', 'refresh');

        }
    }
}
/* End of file logout.php */
/* Local: application/controllers/admin/logout.php */