<?php
if (!defined('BASEPATH'))
	exit('No direct script access allow');

class Setting extends  admin_controller {
	public function __construct() {
		parent::__construct();

		$this -> check_login();
		//$this -> permission_view('setting');
		$this ->load-> model('setting/setting_model', 'setting');
		//$this->load->model('language/site_lang_model', 'm_lang');
	}

	public function index() {
		$this->breadcrumb = array(
			'Dashboard' => URL_ADMIN,
			'Setting'	=> "",
		);

		$act = $this -> input -> post('h_act');
		$oldLogo = $this -> input -> post('oldLogo');
		$oldMainBanner = $this->input->post('oldMainBanner');
		$uploadPath = './uploads/image/';
		$storeLogo = 'uploads/image/';

		if ($_POST) {

			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['remove_spaces'] = TRUE;

			if (!empty($_FILES['logo']['name'])) {

				$this -> load -> library('upload', $config);

				if (!$this -> upload -> do_upload('logo')) {
					$logo_url = $oldLogo;
				} else {
					$up = $this -> upload -> data();
					$logo_url = $storeLogo . $up['file_name'];

					$imageWidth = $up['image_width'];
					if ($imageWidth > 400) {
						$this -> load -> library('image_lib');
						$config['image_library'] = 'GD2';
						$config['source_image'] = $up['full_path'];
						$config['quality'] = 75;
						$config['maintain_ratio'] = TRUE;
						$config['master_dim'] = 'auto';
						$config['width'] = 400;
						$config['height'] = 400;

						$this -> image_lib -> initialize($config);
						$this -> image_lib -> resize();
					}

					@unlink($oldLogo);
				}
			} else {
				$logo_url = $oldLogo;
			}

			if (!empty($_FILES['main_banner']['name'])) {

				$this -> load -> library('upload', $config);

				if (!$this -> upload -> do_upload('main_banner')) {
					$book_image = $oldMainBanner;
				} else {
					$up = $this -> upload -> data();
					$book_image = $storeLogo . $up['file_name'];

					// $imageWidth = $up['image_width'];
					// if ($imageWidth > 400) {
					// 	$this -> load -> library('image_lib');
					// 	$config['image_library'] = 'GD2';
					// 	$config['source_image'] = $up['full_path'];
					// 	$config['quality'] = 75;
					// 	$config['maintain_ratio'] = TRUE;
					// 	$config['master_dim'] = 'auto';
					// 	$config['width'] = 400;
					// 	$config['height'] = 400;

					// 	$this -> image_lib -> initialize($config);
					// 	$this -> image_lib -> resize();
					// }

					@unlink($oldMainBanner);
				}
			} else {
				$book_image = $oldMainBanner;
			}

			//Update for logo
			$this -> db -> where('key', 'logo');
			$this -> db -> set('value', $logo_url);
			$this -> db -> update('config_site');

			//Update for book image
			$this -> db -> where('key', 'main_banner');
			$this -> db -> set('value', $book_image);
			$this -> db -> update('config_site');

			foreach ($_POST as $key => $val) {
				//Update table config_system
				$this -> db -> where('key', $key);
				$this -> db -> set('value', $val);
				$this -> db -> update('config_site');

			}
			//print_r($_POST);
			$this -> message('info', t_lang('update_success'));
			//redirect(URL_ADMIN . 'setting');

		}

		$txt = array('action_form' => URL_ADMIN . 'setting');

		$this -> template -> title()
		-> set('link_action', URL_ADMIN .'setting/index')
		-> build('admin/setting/index');
	}

}

// End class
