<?php
if( !defined('BASEPATH') ) exit ( 'No direct script access allowed' );

class Page extends admin_controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->breadcrumb = array(
            'Admin panel' => '',
            'Pages' => ''
        );

		$type = $this->input->get('type');
		$this->db->order_by('id', 'desc');
		$this->db->join('page_language', 'pages.id = page_language.page_id');
		$this->db->where('page_language.lang_code', $this->admin_lang);
		$sql = $this->db->get('pages')->result_array();
		$this->_page_header = 'Pages manager';

		$this->template->title()
			->set('body', $sql)
			->set('link_add', URL_ADMIN.'page/add')
			->set('url_edit', URL_ADMIN.'page/edit/')
			->set('link_del', URL_ADMIN.'page/delete/')
			
			->set('type', $type)
			->build('admin/page/index');
	}

	public function add()
	{
		$type = '';

		if($_POST)
		{
			$arr = array(
				'type'		=> $type,
				'status'	=> (int)$this->input->post('status'),
				'order'		=> (int)$this->input->post('order')
			);
			if( $this->db->insert('pages', $arr) )
			{

				$page_id = $this->db->insert_id();
				
				foreach($this->config->item('language_support') as $key => $value) {
					$page_detail = array(
						'page_title'		=> $this->input->post('title'),
						'page_title_alias'	=> $this->util->alias($this->input->post('title')),
						'page_content'		=> $this->input->post('content'),	
						'page_id'			=> $page_id,
						'lang_code' 		=> $value
					);

					$this->db->insert('page_language', $page_detail);
				}

	   			$this->message('success', 'Added successfuly');
				redirect(URL_ADMIN.'page');
			} else {
				$this->message('error', 'Added faild');
				redirect(URL_ADMIN.'page');
			}
		}
		$this->_page_header = 'Create new page';

		$this->template->title()
			->build('admin/page/add');

	}

	public function edit()
	{
		$type = $this->input->get('type');
		$id = $this->input->get('id');

		if($_POST)
		{
			$arr = array(
				'status'	=> (int)$this->input->post('status'),
				'order'		=> (int)$this->input->post('order')
			);
			if( $this->db->update('pages', $arr, array('id' => $id)))
			{
	   			$page_update = array(
	   				'page_title'		=> $this->input->post('title'),
	   				'page_title_alias'	=> $this->util->alias($this->input->post('title')),
					'page_content'		=> $this->input->post('content'),	
	   			);

	   			$this->db->where(array('page_id' => $id, 'lang_code' => $this->admin_lang));
	   			$this->db->update('page_language', $page_update);

	   			$this->message('success', 'Updated has been successfuly');
				redirect(URL_ADMIN.'page');
			} else {
				$this->message('error', 'Error');
				redirect(URL_ADMIN.'page');
			}
		}
		$this->_page_header = 'Edit page';

		$this->db->where(array('pages.id' => $id, 'page_language.lang_code' =>$this->admin_lang));
		$this->db->join('page_language', 'pages.id = page_language.page_id');
		$sql = $this->db->get('pages')->row_array();

		$this->template->title()
			->set('info', $sql)
			->build('admin/page/edit');

	}

	public function change_status(){
        $id = (int)$this->input->get('id');
        $status = (int)$this->input->get('status');
        $type = $this->input->get('type');
        if($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('pages', array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN.'page');
    }

	public function delete()
	{
		$id = $this->uri->segment(4);

		if ( $this->db->delete('pages', array( 'id' => $id )) )
		{
			$this->db->delete('page_language', array('page_id' => $id));
			$info = array('status' => TRUE);
		} else {
			$info = array('status' => FALSE);
		}

		echo json_encode($info);

	}
}

/* End file */
/* Local: ./application/controller/admin/page.php */