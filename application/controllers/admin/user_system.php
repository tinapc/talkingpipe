<?php 
if( !defined('BASEPATH') ) 
    exit ( 'No direct script access allowed' );
/*
 * User_system Class
 * Exxtends Admin_controller
 */

class User_system extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        
        //Set title page
        $this->_page_header = 'Administrator';
    }
    
    public function index(){
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'User System' => URL_ADMIN . 'user_system',
        );
        
        $body = $this->db->get_where('user', array('username !=' => 'admin'))->result_array();
        
        $this->template->title()
                ->set('body', $body)
                ->set('url_change_status', 'user_system/change_status/')
                ->build('admin/user/index');
    }
    
    public function add() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'User System' => URL_ADMIN . 'user_system',
            'Create new' => ''
        );
        
        if($_POST) {
            $_data = array(
                'username'      => $this->input->post('username'),
                'password'      => md5($this->input->post('password')),
                'email'         => $this->input->post('email'),
                'fullname'      => $this->input->post('fullname'),
                'phone'         => $this->input->post('phone'),
                'group'         => $this->input->post('group'),
                'avatar'        => '',
                'status'        => $this->input->post('status'),
                'created_date'  => time()
            );
            
            if($this->db->insert('user', $_data)) {
                $this->session->set_flashdata('success', "User has been created");
                redirect(URL_ADMIN.'user_system');
            }else{
                $this->session->set_flashdata('error', "You have a error");
                redirect(URL_ADMIN.'user_system');
            }
        }
        
        $this->template->title()->build('admin/user/add');
    }
    
    public function edit(){
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'User System' => URL_ADMIN . 'user_system',
            'Edit'  => ''
        );
        
        //Get current $id user
        $id = $this->uri->segment(4);
                
        if($_POST) {
            $oldpass = $this->input->post('oldpass');
            $newpass = $this->input->post('password');
            $oldAvatar = $this->input->post('old_avatar');
            
            if($newpass !== '') {
                $set_newpass = md5($newpass);
            } else {
                $set_newpass = $oldpass;
            }
            
            if (!empty($_FILES['avatar']['name'])) {
                $config['upload_path'] = './uploads/avatar/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('avatar')) {
                    //echo $this->upload->display_errors();
                    $avatar = $oldAvatar;
                } else {
                    if ($oldAvatar !== "")
                        @unlink($oldAvatar);
                    $up = $this->upload->data();
                    $avatar = 'uploads/avatar/' . $up['file_name'];

                    $imageWidth = $up['image_width'];
                    if ($imageWidth > 75) {
                        $this->load->library('image_lib');
                        $config['image_library'] = 'GD2';
                        $config['source_image'] = $up['full_path'];
                        $config['quality'] = 75;
                        $config['maintain_ratio'] = TRUE;
                        $config['master_dim'] = 'height';
                        $config['width'] = 75;
                        $config['height'] = 75;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                    }
                }
            } else {
                $avatar = $this->input->post('old_avatar');
            }
            
            $_data = array(
//                'username'      => $this->input->post('username'),
                'password'      => $set_newpass,
                'email'         => $this->input->post('email'),
                'fullname'      => $this->input->post('fullname'),
                'phone'         => $this->input->post('phone'),
                'group'         => $this->input->post('group'),
                'avatar'        => $avatar,
                'status'        => $this->input->post('status'),
            );
            
            if($this->db->update('user', $_data, array('id' => $id))) {
                $this->session->set_flashdata('success', "User has been updated");
                redirect(URL_ADMIN.'user_system');
            }else{
                $this->session->set_flashdata('error', "You have a error");
                redirect(URL_ADMIN.'user_system');
            }
        }
        
        //Get info user to assign to view edit.php
        $infoUser = $this->db->get_where('user', array('id' => $id))->row_array();
        
        $this->template->title()->set('infoUser', $infoUser)->build('admin/user/edit');
    }
    
    public function delete() {
        //Get current $id user
        $id = $this->uri->segment(4);
        
        $query = $this->db->select('avatar')->get_where('user', array('id' => $id))->row();
        if($query->avatar !== '') unlink($query->avatar);
        
        if($this->db->delete('user', array('id' => $id))) {
            $json = array('status' => TRUE);
        }else{
            $json = array('status' => FALSE);
        }
        
        echo json_encode($json);
    }
    
    public function change_status() {
        $id = (int) $this->uri->segment(4);
        $status = (int) $this->uri->segment(5);
        if ($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('user', array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN . 'user_system');
    }
}
/*
 * End file
 */