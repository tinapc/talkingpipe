<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class D_text extends admin_controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$type = $this->input->get('type') ? $this->input->get('type') : 'header';
		$page = $this->input->get('page') ? $this->input->get('page') : 'home';

		$listpages = array(
			'home'	=> 'Front',
			'empire' => 'Empire',
			'blog' => 'Blog',
			'book' => 'Book',
			'product' => 'Product'
		);

		if($this->input->post()) {
			// Checking if this page was added then update else insert
			$check = $this->db->select('id')->get_where('text_for_page', array('page' => $page, 'type' => $type));
			if($check->num_rows() > 0 ) {
				$textidup = (int)$this->input->post('textid');
				$_data1 = array(
					'text' => $this->input->post('text')
				);

				$this->db->where(array('text_id' => $textidup, 'lang_code' => $this->admin_lang));
				$this->db->update('text_for_page_language', $_data1);
			} else {
				$arr = array(
					'type'	=> $type,
					'page'	=> $page
				);

				$this->db->insert('text_for_page', $arr);
				$textid = $this->db->insert_id();

				foreach (config_item('language_support') as $key => $value) {
					$_data = array(
						'text_id'	=> $textid,
						'text'	=> $this->input->post('text'),
						'lang_code'	=> $value
					);

					$this->db->insert('text_for_page_language', $_data);
				}
			}
		}

		if($page && $page !== '') {
			$this->db->select('text_for_page_language.text, text_for_page.id');
			$this->db->where(array('text_for_page.page' => $page, 'text_for_page.type' => $type, 'text_for_page_language.lang_code' => $this->admin_lang));
			$this->db->join('text_for_page_language', 'text_for_page.id = text_for_page_language.text_id', 'left');
			$info = $this->db->get('text_for_page');

			if($info->num_rows() > 0) {
				$info = $info->row();
				$text = $info->text;
				$textid = $info->id;
			} else {
				$text = '';
				$textid = '';
			}
		} else {
			$text = '';
			$textid = '';	
		}

		$this->template->title()
			->set('type', $type)
			->set('page', $page)
			->set('content', $text)
			->set('text_id', $textid)
			->set('listpages', $listpages)
			->build('admin/d_text/index');	
	}

	public function get_content(){
		$page = $this->input->post('page');
		$type = $this->input->post('type');

		$this->db->select('text_for_page_language.text');
		$this->db->where(array('text_for_page.page' => $page, 'text_for_page.type' => $type, 'text_for_page_language.lang_code' => $this->admin_lang));
		$this->db->join('text_for_page_language', 'text_for_page.id = text_for_page_language.text_id', 'left');
		$info = $this->db->get('text_for_page');

		if($info->num_rows() > 0) {
			$info = $info->row();
			$text = $info->text;
		} else {
			$text = 'dsadsadsad';
		}

		echo json_encode(array('status' => TRUE, 'text' => $text));
	}

}

/* End for file d_text.php */
/* Location: ./application/controllers/admin/d_text.php */