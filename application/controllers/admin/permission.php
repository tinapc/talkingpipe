<?php
class Permission extends admin_controller {
	function __construct()
	{
		parent::__construct();
	}
	
	function error()
	{
		$data = array();
		$this->template->title('Access denied')
						->set('warning', t_lang('denied_access'))
						->build('admin/no_access');
	}
}