<?php
if( !defined('BASEPATH') ) exit ( 'No direct script access allowed' );

class Menu extends admin_controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->breadcrumb = array(
            'Admin panel' => '',
            'Menu' => ''
        );
		$this->_page_header = 'Menu manager';

		$sql = $this->db->order_by('id', 'desc')->get('menus')->result_array();

		$this->template->title($this->_page_header)
			->set('body', $sql)
			->set('link_add', URL_ADMIN.'menu/add')
			->set('link_edit', URL_ADMIN.'menu/edit/')
			->set('link_del', URL_ADMIN.'menu/delete/')
			->set('link_add_item', URL_ADMIN.'menu/view_item/')
			->build('admin/menu/index');
	}

	public function add()
	{
		$this->_page_header = 'Add menu';
		if($_POST)
		{
			//print_r($_POST);
			$this->form_validation->set_rules('menu_name', 'Menu name', 'required');

			if($this->form_validation->run() == TRUE)
			{
				// if($this->input->post('link') !== "") {
				// 	$link = $this->input->post('link');
				// 	$link_cat = "";
				// } else {
				// 	$link = "";
				// 	$link_cat = $this->input->post('link_cat');
				// }

				$arr = array(
					'menu_name'		=> $this->input->post('menu_name'),
					// 'status'		=> (int)$this->input->post('status'),
					// 'order'			=> (int)$this->input->post('order'),
					// 'link'			=> $link,
					// 'link_cat'		=> $link_cat
				);

				if( $this->db->insert('menus', $arr) )
				{
					$this->message('info', 'Added successfuly');
					redirect(URL_ADMIN.'menu');
				} else {
					$this->message('info', 'Added faild');
					redirect(URL_ADMIN.'menu');
				}
			}

		}

		$this->template->title('Add menu')
			->set('link_action', URL_ADMIN.'menu/add')
			->set('link_cancel', URL_ADMIN.'menu')
			->build('admin/menu/add');

	}

	public function edit()
	{
		$this->_page_header = 'Edit menu';
		$id = $this->uri->segment(4);

		if($_POST)
		{
			$this->form_validation->set_rules('menu_name', 'Menu name', 'required');


			if($this->form_validation->run() == TRUE) {

				$arr = array(
					'menu_name'		=> $this->input->post('menu_name'),
				);


				if( $this->db->update('menus', $arr, array( 'id' => $id )) )
				{
					$this->message('info', 'Updated successfuly');
					redirect(URL_ADMIN.'menu');
				} else {
					$this->message('info', 'Updated faild');
					redirect(URL_ADMIN.'menu');
				}
			}

		}

		$items = $this->db->get_where('menus', array('id' => $id))->row_array();


		$this->template->title()
			->set('link_action', URL_ADMIN.'menu/edit/'. $id)
			->set('link_cancel', URL_ADMIN.'menu')
			->set('items', $items)
			->build('admin/menu/edit');
	}

	public function delete()
	{
		$id = $this->uri->segment(4);

		if ( $this->db->delete('menus', array( 'id' => $id )) )
		{
			$this->message('info', 'Deleted successfuly');
			$json = array('status' => true);
		} else {
			$this->message('info', 'Deleted successfuly');
			$json = array('status' => false);
		}

		echo json_encode($json);
	}

	public function delete_item()
	{
		$id = $this->uri->segment(4);

		if ( $this->db->delete('menus_item', array( 'id' => $id )) )
		{
			$this->db->delete('menu_item_language', array('menu_item_id' => $id));
			$this->message('info', 'Deleted successfuly');
			$json = array('status' => true);
		} else {
			$this->message('info', 'Deleted successfuly');
			$json = array('status' => false);
		}

		echo json_encode($json);
	}


	/*************
	------------View list item in a menu
	---------------------------------------*/
	public function view_item(){
		
		$this->breadcrumb = array(
            'Admin panel' => '',
            'Menu' => URL_ADMIN .'menu',
            'Item' => ''
        );

		$menuid = (int)$this->uri->segment(4);

		// If isset POST then add new item
		if($this->input->post()) {
			$type = $this->input->post('type_url');

			switch ($type) {
				case 'site_url':
					$menu_link = base_url().$this->input->post('item_link');
					break;
				case 'external_url':
					$menu_link = $this->input->post('item_link');
					break;
				case 'page':
					$menu_link = base_url().$this->input->post('link_with_page');
					break;
			}

			$item = array(
				'menu_id'		=> $menuid,
				'menu_order'	=> (int)$this->input->post('item_order'),
				'menu_status'	=> (int)$this->input->post('status'),
				'menu_link'		=> $menu_link,
			);
			if($this->db->insert('menus_item', $item)) {
				$item_id = $this->db->insert_id();
				foreach($this->config->item('language_support') as $key => $value) {
					$item_info = array(
						'menu_item_id'		=> $item_id,
						'item_name'			=> $this->input->post('item_name'),
						'lang_code'			=> $value
					);
					$this->db->insert('menu_item_language', $item_info);	
				}
				redirect(URL_ADMIN.'menu/view_item/'. $menuid);
			}
		}


		$this->db->where(array('menus_item.menu_id' => $menuid, 'menu_item_language.lang_code' => $this->admin_lang));
		$this->db->join('menu_item_language', 'menus_item.id = menu_item_language.menu_item_id');
		$results = $this->db->get('menus_item')->result_array();

		//Menu info
		$sql = $this->db->get_where('menus', array('id' => $menuid))->row();
		$this->_page_header = 'List items in '. $sql->menu_name;

		$this->template->title($this->_page_header)
			->set('body', $results)
			->set('link_add', URL_ADMIN.'menu/add')
			->set('link_edit', URL_ADMIN.'menu/edit_item/')
			->set('link_del', URL_ADMIN.'menu/delete/')
			->set('link_add_item', URL_ADMIN.'menu/view_item/')
			->build('admin/menu/view_item');

	}

	public function change_status_item(){
        $id = (int)$this->input->get('id');
        $status = (int)$this->input->get('status');
        $cur_menu_view = $this->input->get('menu_id');
        if($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('menus_item', array('menu_status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN.'menu/view_item/' . $cur_menu_view);
    }

    public function edit_item(){
    	$itemid = $this->uri->segment(4);
    	$menuid = $this->uri->segment(5);

    	$this->breadcrumb = array(
            'Admin panel' => '',
            'Menu' => URL_ADMIN .'menu/view_item/' . $menuid,
            'Item' => ''
        );

    	// if submit has been handle
    	if($this->input->post()) {
    		$item = array(
				'menu_order'	=> (int)$this->input->post('item_order'),
				'menu_status'	=> (int)$this->input->post('status'),
				'menu_link'		=> $this->input->post('item_link'),
			);
			if($this->db->update('menus_item', $item, array('id' => $itemid))) {
				$item_info = array(
					'item_name'	=> $this->input->post('item_name'),
				);

				$this->db->update('menu_item_language', $item_info, array('menu_item_id' => $itemid, 'lang_code' => $this->admin_lang));	
				redirect(URL_ADMIN.'menu/view_item/'. $menuid);
			}	
    	}


        $this->db->where(array('menus_item.id' => $itemid, 'menu_item_language.lang_code' => $this->admin_lang));
		$this->db->join('menu_item_language', 'menus_item.id = menu_item_language.menu_item_id');
		$results = $this->db->get('menus_item')->row_array();

		//Menu info
		$this->_page_header = 'Edit item '. $results['item_name'];

		$this->template->title($this->_page_header)
			->set('body', $results)
			->build('admin/menu/edit_item');
    }
}

/* End file */
/* Local: ./application/controller/admin/page.php */