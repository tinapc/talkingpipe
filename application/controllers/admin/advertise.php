<?php
if( !defined('BASEPATH') ) exit ( 'No direct script access allowed' );

class Advertise extends admin_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('advertise/advertise_model');
	}

	public function index()
	{
		$this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Banner'   => URL_ADMIN.'advertise',
        );

		$sql = $this->advertise_model->getList(null, null, null, null);
		$this->_page_header = 'Quảng cáo';

		$this->template->title()
			->set('body', $sql)
			->set('link_add', URL_ADMIN.'advertise/add')
			->set('link_edit', URL_ADMIN.'advertise/edit/')
			->set('link_del', URL_ADMIN.'advertise/delete/')
			->set('link_delMul', URL_ADMIN.'advertise/deleteMul/')
			->set('link_change_status', URL_ADMIN.'advertise/change_status/')
			->build('admin/advertise/index');
	}

	public function add(){
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Banner'   => URL_ADMIN.'advertise',
            'Add' => ''
        );

        if($_POST) {

                if($this->advertise_model->add($_POST)) {
                    $this->message('success', "New record has been added succussful");
                    redirect(URL_ADMIN.'advertise');
                } else {
                    $this->message('error', "Error");
                    redirect(URL_ADMIN.'advertise');  
                }   

            
        }

        $this->_page_header = 'Create new';

        $this->template->title()
                ->build('admin/advertise/add');   
    }


	public function edit()
	{
		$this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Banner'   => URL_ADMIN.'advertise',
            'Edit' => ''
        );

        $id = (int)$this->uri->segment(4);

        if($_POST) {
            if($this->advertise_model->edit($_POST, $id)) {
                $this->message('success', "The record has been updated succussful");
                redirect(URL_ADMIN.'advertise');
            } else {
                $this->message('error', "Error");
                redirect(URL_ADMIN.'advertise');  
            }   
        }

        $this->_page_header = 'Edit';

        $info = $this->advertise_model->read($id);
        $this->template->title()
                ->set('info',$info)
                ->build('admin/advertise/edit');
	}

	public function delete()
	{
		$id = $this->uri->segment(4);

		if ( $this->advertise_model->delete($id))
		{
			$info = array('status' => TRUE);
		} else {
			$info = array('status' => FALSE);
		}

		echo json_encode($info);
	}

	public function change_status(){
        $id = (int)$this->uri->segment(4);
        $status = (int)$this->uri->segment(5);
        if($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('banner', array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN.'advertise');
    }
}

/* End file */
/* Local: ./application/controller/admin/page.php */