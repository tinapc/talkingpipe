<?php 
if( !defined('BASEPATH') ) 
    exit ( 'No direct script access allowed' );
/*
 * Contact Class
 * Exxtends Admin_controller
 */

class Contact extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        
        //Set title page
        $this->_page_header = 'Liên hệ';
    }
    
    public function index(){
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Liên hệ' => URL_ADMIN . 'contact',
        );
        
        $body = $this->db->get('contact')->result_array();
        
        $this->template->title()
                ->set('body', $body)
                //->set('url_change_status', 'user_system/change_status/')
                ->build('admin/contact/index');
    }
    
    public function view(){
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Contact' => URL_ADMIN . 'contact',
            'View' => ''
        );
              
        //Get info user to assign to view edit.php
        $id = $this->uri->segment(4);
        $infoUser = $this->db->get_where('contact', array('id' => $id))->row_array();
        
        $this->template->title()->set('infoUser', $infoUser)->build('admin/contact/view');
    }
    
    public function group(){
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Nhóm Liên hệ' => URL_ADMIN . 'contact/group',
        );
        
        $body = $this->db->get('contact_group')->result_array();
        
        $this->template->title()
                ->set('body', $body)
                //->set('url_change_status', 'user_system/change_status/')
                ->build('admin/contact/group');
    }
    
    public function add() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Contact' => URL_ADMIN . 'contact',
            'Contact group' => URL_ADMIN . 'contact/group',
            'Create new' => ''
        );
        
        if($_POST) {
            $_data = array(
                'email'         => $this->input->post('email'),
                'desc'      => $this->input->post('desc'),
            );
            
            if($this->db->insert('contact_group', $_data)) {
                $this->session->set_flashdata('success', "Tạo mới thành công");
                redirect(URL_ADMIN.'contact/group');
            }else{
                $this->session->set_flashdata('error', "Có lỗi xảy ra");
                redirect(URL_ADMIN.'contact/group');
            }
        }
        
        $this->template->title()->build('admin/contact/add');
    }
    
    public function edit(){
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Contact' => URL_ADMIN . 'contact',
            'Contact group' => URL_ADMIN . 'contact/group',
            'Edit' => ''
        );
        
        //Get current $id user
        $id = $this->uri->segment(4);
                
        if($_POST) {
            $_data = array(
                'email'         => $this->input->post('email'),
                'desc'      => $this->input->post('desc'),
            );
            
            if($this->db->update('contact_group', $_data, array('id' => $id))) {
                $this->session->set_flashdata('success', "Cập nhật thành công");
                redirect(URL_ADMIN.'contact/group');
            }else{
                $this->session->set_flashdata('error', "Có lỗi xảy ra");
                redirect(URL_ADMIN.'contact/group');
            }
        }
        
        //Get info user to assign to view edit.php
        $infoUser = $this->db->get_where('contact_group', array('id' => $id))->row_array();
        
        $this->template->title()->set('infoUser', $infoUser)->build('admin/contact/edit');
    }
    
    public function delete() {
        //Get current $id user
        $id = $this->uri->segment(4);
              
        if($this->db->delete('contact_group', array('id' => $id))) {
            $json = array('status' => TRUE);
        }else{
            $json = array('status' => FALSE);
        }
        
        echo json_encode($json);
    }
    
    public function delete_contact() {
        //Get current $id user
        $id = $this->uri->segment(4);
              
        if($this->db->delete('contact', array('id' => $id))) {
            $json = array('status' => TRUE);
        }else{
            $json = array('status' => FALSE);
        }
        
        echo json_encode($json);
    }
    
    public function change_status() {
        $id = (int) $this->uri->segment(4);
        $status = (int) $this->uri->segment(5);
        if ($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('contact_group', array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN . 'contact_group');
    }
}
/*
 * End file
 */