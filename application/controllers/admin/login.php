<?php

class Login extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();

        $this->load->model('user/user_model', 'user');

        $this->lang->load('admin');
        define('URL_ADMIN', $this->config->item('index_url').'admin/');
        define('themes_admin', base_url().'themes/admin/');
    }

    function index()
    {
        $data['action'] = $this->config->item('index_url').'/admin/login/do_login';
        $this->load->view('admin/account/login', $data);
    }

    public function do_login() {

        $data = array();

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

        //$this->form_validation->set_message('required','The field is required');

        if($this->form_validation->run() == TRUE ) {

            $username = $this->input->post('username');
            $mk = md5($this->input->post('password'));
            $q = $this->user->login($username);
            if($q) {

                $result = $q;

                if($result->password !== $mk) {

                    $this->session->set_flashdata('error', t_lang('pass_invalid'));
                    redirect('/admin/login', 'refresh');

                } else {


                        $data = array (
                            'user_id' => $result->id,
                            'username' => $result->email,
                            'sess_group'    => $result->group,
                            'fullname' => $result->fullname,
                            'sess_memPhone' => $result->phone,
                            'sess_avatar' => $result->avatar,
                            'logined' => TRUE
                        );

                        $this->session->set_userdata($data);

                        redirect('/admin/home', 'refresh');

                }

            } else {
                $this->session->set_flashdata('error', t_lang('acc_not_exists'));
                redirect('/admin/login');
            }

        } else {
            $this->session->set_flashdata('error', t_lang('user_pass_required'));
            redirect('/admin/login','refresh');
        }

    }
}
/* End file login.php */
/* Local: application/admin/login.php */
