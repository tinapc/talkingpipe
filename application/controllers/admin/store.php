<?php

/** Class Store
 * Extends Admin_Controller
 * Manager member
 * */
class Store extends admin_controller {

    function __construct() {
        parent::__construct();
       // $this->load->model('service/service_model');
    }

    public function index() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Store' => URL_ADMIN . 'store',
        );

       $query = $this->db->get('stores')->result_array();
       
        $this->template->title()
                ->set('body', $query)
                ->set('url_edit', URL_ADMIN . 'store/edit/')
                ->build('admin/store/index');
    }

    public function add() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Store' => URL_ADMIN . 'store',
            'Add new' => ''
        );

        if ($_POST) {
            $_data = array(
                'title'     => $this->input->post('title'),
                'address'   => $this->input->post('address'),
                'phone'     => $this->input->post('phone'),
                'email'     => $this->input->post('email')
            );
            if ($this->db->insert('stores', $_data)) {
                $this->message('success', "Add new has been added succussful");
                redirect(URL_ADMIN . 'store');
            } else {
                $this->message('error', "Error");
                redirect(URL_ADMIN . 'store');
            }
        }

        $this->template->title()
             ->build('admin/store/add');
    }

    public function edit() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Store' => URL_ADMIN . 'store',
            'Edit' => ''
        );

        $id = (int) $this->uri->segment(4);

        if ($_POST) {
            $_data = array(
                'title'     => $this->input->post('title'),
                'address'   => $this->input->post('address'),
                'phone'     => $this->input->post('phone'),
                'email'     => $this->input->post('email')
            );
            if ($this->db->update('stores', $_data, array('id' =>  $id))) {
                $this->message('success', "Updated has been succussful");
                redirect(URL_ADMIN . 'store');
            } else {
                $this->message('error', "Error");
                redirect(URL_ADMIN . 'store');
            }
        }

        $info = $this->db->get_where('stores', array('id' => $id))->row_array();
        $this->template->title()
                ->set('info', $info)
                ->build('admin/store/edit');
    }

    public function delete() {
        $id = (int) $this->uri->segment(4);
        if ($this->db->delete('store', array('id' => $id))) {
            $info = array('status' => TRUE);
        } else {
            $info = array('status' => FALSE);
        }

        echo json_encode($info);
    }

    public function delMulti() {
        $listID = $this->input->post('listID');
        $listID = explode(',', $listID);

        foreach ($listID as $id) {
           $this->db->delete('store', array('id' => $id));
        }
        $this->session->set_flashdata('success', 'Your request has been finished');
        $mess = array('status' => true, 'message' => $this->lang->line('delete_success'));

        echo json_encode($mess);
    }

}

/**End file member.php **/
/* File local: application/controller/admin/store.php */