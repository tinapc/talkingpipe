<?php

/** Class Product
 * Extends Admin_Controller
 * Manager member
 * */
class Product extends admin_controller {

    function __construct() {
        
        parent::__construct();
        $this->load->model('product/product_model');
        $this->upload_path = 'uploads/product/';
        
    }

    public function category() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Category' => URL_ADMIN . 'product/category',
        );

        $this->_page_header = 'List categories';

        $this->db->select('*');
        $this->db->where('catelog_language.lang_code', $this->admin_lang);
        $this->db->join('catelog_language', 'catelog.id = catelog_language.catelog_id');
        $sql = $this->db->get('catelog')->result_array();

        $this->template->title()
                ->set('body', $sql)
                ->set('link_add', URL_ADMIN . 'product/add_cate')
                ->set('link_edit', URL_ADMIN . 'product/edit_cate/')
                ->set('link_delete', URL_ADMIN . 'product/delete_cate/')
                ->set('url_change_home_status', 'product/change_homepage_cate/')
                ->build('admin/product/category');
    }

    public function add_cate() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Category' => URL_ADMIN . 'product/category',
            'Add category' => ''
        );

        if ($_POST) {

            if ($this->product_model->checkCategory($this->input->post('title'))) {
                $this->session->set_flashdata('error', "This catelog has already exist");
                redirect(URL_ADMIN . 'product/add_cate');
            } else {
                $this->product_model->add_cate($_POST);
                $this->message('success', "New record has been added succussful");
            }
            redirect(URL_ADMIN . 'product/category');
        }

        $this->_page_header = 'Add new category';

        $this->template->title()
                ->build('admin/product/add_cate');
    }

    public function edit_cate() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Manager Category' => URL_ADMIN . 'product/category',
            'Edit' => ''
        );

        $id = (int) $this->uri->segment(4);

        if ($_POST) {
            $this->product_model->edit_cate($_POST, $id);
            $this->message('success', "The record has been updated succussful");
            redirect(URL_ADMIN . 'product/category');
        }

        $this->_page_header = 'Edit Category';
        $info = $this->product_model->read_category($id);
        $this->template->title()
                ->set('info', $info)
                ->build('admin/product/edit_cate');
    }

    public function change_status_cate() {
        $id = (int) $this->uri->segment(4);
        $status = (int) $this->uri->segment(5);
        if ($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('catelog', array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN . 'product/category');
    }

    public function change_homepage_cate() {
        $id = (int) $this->uri->segment(4);
        $status = (int) $this->uri->segment(5);
        if ($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('service_category', array('home_page' => $restatus), array('id' => $id));
        redirect(URL_ADMIN . 'product/category');
    }

    public function delete_cate() {
        $id = (int) $this->uri->segment(4);
        if ($this->product_model->delete_cate($id)) {
            $info = array('status' => TRUE);
        } else {
            $info = array('status' => FALSE);
        }

        echo json_encode($info);
    }

    public function delCateMulti() {
        $listID = $this->input->post('listID');
        $listID = explode(',', $listID);

        foreach ($listID as $id) {
            $this->product_model->delete_cate($id);
        }
        $this->session->set_flashdata('success', 'Your request has been finished');
        $mess = array('status' => true, 'message' => $this->lang->line('delete_success'));

        echo json_encode($mess);
    }

    /**/////////////FOR PRODUCT/////////////////////
    ///////////////////////////////////////////////*/

    public function index() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Product' => URL_ADMIN . 'product',
        );

        $this->_page_header = 'Products';
        $services = $this->product_model->getList(null, null, array('id' => 'desc'), null);
        $this->template->title()
                ->set(array('body' => $services))
                ->set('url_add', URL_ADMIN . 'product/add')
                ->set('url_edit', URL_ADMIN . 'product/edit/')
                ->set('url_del', URL_ADMIN . 'product/delete/')
                ->set('url_delMul', URL_ADMIN . 'product/delMulPro')
                ->set('url_change_status', 'product/change_status_pro/')
                ->build('admin/product/index');
    }

    public function add() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Products' => URL_ADMIN . 'product',
            'Add new' => ''
        );

        if ($_POST) {
            //$relate = implode(",", $_POST['related_pro']);
            // print_r( $relate);
            if ($this->product_model->checkCodePro($this->input->post('title'))) {
                $this->session->set_flashdata('error', "This product was already exist");
                redirect(URL_ADMIN . 'product/add');
            } else {

                if ($this->product_model->add($_POST)) {
                    $this->message('success', "New product has been added succussful");
                    redirect(URL_ADMIN . 'product');
                } else {
                    $this->message('error', "Error");
                    redirect(URL_ADMIN . 'product');
                }
            }
        }

        $this->_page_header = 'Create new';

        $this->db->where('catelog_language.lang_code', $this->admin_lang);
        $this->db->join('catelog_language', 'catelog_language.catelog_id = catelog.id', 'left');
        $cates = $this->db->get('catelog')->result_array();

        $this->template->title()
                ->set('cates', $cates)
                ->build('admin/product/add');
    }

    public function edit() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Product' => URL_ADMIN . 'product',
            'Edit' => ''
        );

        $id = (int) $this->uri->segment(4);

        if ($_POST) {
            if ($this->product_model->edit($_POST, $id)) {
                $this->message('success', "The record has been updated succussful");
                redirect(URL_ADMIN . 'product');
            } else {
                $this->message('error', "Error");
                redirect(URL_ADMIN . 'product');
            }
        }

        //create a random code to know what is color for this product
        $info = $this->product_model->read_pro($id);
        $this->_page_header = 'Edit product - ' . $info['title'];
        
        $this->db->where('catelog_language.lang_code', $this->admin_lang);
        $this->db->join('catelog_language', 'catelog_language.catelog_id = catelog.id', 'left');
        $cates = $this->db->get('catelog')->result_array();

        $this->template->title()
                ->set('info', $info)
                ->set('cates', $cates)
                ->build('admin/product/edit');
    }

    public function delete() {
        $id = (int) $this->uri->segment(4);
        if ($this->product_model->delete($id)) {
            $info = array('status' => TRUE);
        } else {
            $info = array('status' => FALSE);
        }

        echo json_encode($info);
    }

    public function delMulti() {
        $listID = $this->input->post('listID');
        $listID = explode(',', $listID);

        foreach ($listID as $id) {
            $this->product_model->delete($id);
        }
        $this->session->set_flashdata('success', 'Your request has been finished');
        $mess = array('status' => true, 'message' => $this->lang->line('delete_success'));

        echo json_encode($mess);
    }

    public function change_status_pro() {
        $id = (int) $this->uri->segment(4);
        $status = (int) $this->uri->segment(5);
        if ($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('product', array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN . 'product');
    }

    /* //////////////////////////////AJAX HANDLE/////////////////////// */

    public function getSubCate() {
        $id = $this->input->post('id');
        $child_id = $this->input->post('child');
        $qr = $this->db->select('id, title')->get_where('service_category', array('parent' => $id))->result();
        if (count($qr)) {
            foreach ($qr as $r) {
                if($child_id !='') {
                    if($child_id == $r->id) {
                        echo '<option value="' . $r->id . '" selected="selected">' . $r->title . '</option>';
                    } else {
                        echo '<option value="' . $r->id . '">' . $r->title . '</option>';
                    }
                } else {
                    echo '<option value="' . $r->id . '">' . $r->title . '</option>';
                }
                //echo '<option value="' . $r->id . '">' . $r->title . '</option>';
            }
        } else {
            return;
        }
    }

    public function upload_image_color() {
        //$upload = $_FILES['file_color']['name'];
        $nameColor = $this->input->post('color');

          if ($this->db->insert('product_color', array('color' => $nameColor, 'status' => 0, 'code' => $this->input->post('code_insert_to_color')))) {
            $colorId = $this->db->insert_id();

            $sizeData = array(
                'size_35' => $this->input->post('size35'),
                'size_36' => $this->input->post('size36'),
                'size_37' => $this->input->post('size37'),
                'size_38' => $this->input->post('size38'),
                'size_39' => $this->input->post('size39'),
                'colorid' => $colorId,
            );

            $this->db->insert('product_size', $sizeData);

            //Add photo for this color
            $files = $_FILES;
            $config['upload_path'] = './' . $this->upload_path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            $i = 1;
            foreach ($_FILES as $field => $value) {
                if ($this->upload->do_upload($field)) {
                    $up = $this->upload->data();
                    $image_color = $this->upload_path . $up['file_name'];
                    $imageWidth = $up['image_width'];
                    if ($imageWidth > 800) {
                        $this->load->library('image_lib');
                        $config['image_library'] = 'GD2';
                        $config['source_image'] = $up['full_path'];
                        $config['quality'] = 75;
                        $config['maintain_ratio'] = TRUE;
                        $config['master_dim'] = 'height';
                        $config['width'] = 800;
                        $config['height'] = 800;
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                    }
                } else {
                    $image_color = "";
                }

                $this->db->insert('product_photo', array('photo' => $image_color, 'colorid' => $colorId, 'code_con' => $this->input->post('code_insert_to_color')));
                $i++;
            }


        }

        echo json_encode(array('status' => TRUE));
    }

    public function loadListColor() {
        $code = $this->input->post('code');
        $pid = $this->input->post('pid');

        if ($code !== "") {
            $qr = $this->db->get_where('product_color', array('code' => $code))->result();
        } else {
            $qr = $this->db->get_where('product_color', array('pid' => $pid))->result();
        }

        if (count($qr)) {
            ?>
            <div class="block users scrollBox">
            <?php foreach ($qr as $item) { ?>

                    <div class="item clearfix" id="colordiv_<?= $item->id ?>" style="display:block; height:80px; overflow:hidden">
                        <!-- <div class="image">
                            <a href="#"><img class="img-polaroid" src="<?= base_url() . $item->photo ?>" width="50"/></a>
                        </div> -->
                        <div class="info" style="margin-left:0px">
                            <a href="<?= URL_ADMIN ?>product/viewsize/<?= $item->id ?>" data-toggle="modal" data-target="#modal_size" class="tip" title="Xem kích cỡ của màu này"><?= $item->color ?></a>                                                       
                            <div class="controls">                                    
                                <!-- <a href="#" class="icon-ok"></a> -->
                                <a href="javascript:void(0)" onclick="removeColor('<?= $item->id ?>')" class="icon-remove"></a>
                            </div>                                                                    
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php
        } else {
            //echo "";
        }
    }

    public function removeTempColor() {
        $id = (int) $this->input->post('id');

        //Select all photo with colorid == $id
        $imgOfThisColor = $this->db->select('photo')->get_where('product_photo', array('colorid' => $id))->result();
        if($imgOfThisColor && count($imgOfThisColor)) {
            foreach($imgOfThisColor as $pic) {
                if($pic->photo !== '') unlink($pic->photo);
            }
        }
        $this->db->delete('product_photo', array('colorid' => $id));

        $slImg = $this->db->select('photo')->get_where('product_color', array('id' => $id))->row();
        if ($slImg->photo != "")
            @unlink($slImg->photo);
        $this->db->delete('product_size', array('colorid' => $id));

        if ($this->db->delete('product_color', array('id' => $id))) {
            $info = array('status' => TRUE);
        } else {
            $info = array('status' => FALSE, 'message' => 'Có lỗi xảy ra');
        }

        echo json_encode($info);
    }

    public function removeTempPhoto() {
        $id = (int) $this->input->post('id');
        $slImg = $this->db->select('photo')->get_where('product_photo', array('id' => $id))->row();
        if ($slImg->photo !== "")
            @unlink($slImg->photo);
        //$this->db->delete('product_size', array('colorid' => $id));

        if ($this->db->delete('product_photo', array('id' => $id))) {
            $info = array('status' => TRUE);
        } else {
            $info = array('status' => FALSE, 'message' => 'Có lỗi xảy ra');
        }

        echo json_encode($info);
    }

    public function viewsize() {
        $cid = $this->uri->segment(4);
        $data['qr'] = $this->db->get_where('product_size', array('colorid' => $cid))->row();
        $this->load->view('admin/product/viewsize', $data);
    }

    public function update_size_color() {
        $sizeData = array(
            'size_35' => $this->input->post('size35'),
            'size_36' => $this->input->post('size36'),
            'size_37' => $this->input->post('size37'),
            'size_38' => $this->input->post('size38'),
            'size_39' => $this->input->post('size39'),
        );

        if ($this->db->update('product_size', $sizeData, array('colorid' => $this->input->post('colorid')))) {
            $json = array('status' => TRUE);
        } else {
            $json = array('status' => FALSE);
        }
        echo json_encode($json);
    }

    public function upload_photo_pro() {
        $code = $this->input->post('code_insert_to_color');
        $files = $_FILES;
        $config['upload_path'] = './' . $this->upload_path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        $i = 1;
        foreach ($_FILES as $field => $value) {
            if ($this->upload->do_upload($field)) {
                $up = $this->upload->data();
                $image_color = $this->upload_path . $up['file_name'];
                $imageWidth = $up['image_width'];
                if ($imageWidth > 800) {
                    $this->load->library('image_lib');
                    $config['image_library'] = 'GD2';
                    $config['source_image'] = $up['full_path'];
                    $config['quality'] = 75;
                    $config['maintain_ratio'] = TRUE;
                    $config['master_dim'] = 'height';
                    $config['width'] = 800;
                    $config['height'] = 800;
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }
            } else {
                $image_color = "";
            }

            $this->db->insert('product_photo', array('photo' => $image_color, 'code_con' => $code));
            $i++;
        }
    }

    public function loadListPhoto() {
        $code = $this->input->post('code');
        $pid = $this->input->post('pid');

        if ($code !== "") {
            $qr = $this->db->get_where('product_photo', array('code_con' => $code))->result();
        } else {
            $qr = $this->db->get_where('product_photo', array('pid' => $pid))->result();
        }

        if (count($qr)) {
            ?>

            <?php foreach ($qr as $item) { ?>

                <div class="item clearfix" id="photodiv_<?= $item->id ?>" style="display:block; height:80px; overflow:hidden">
                    <div class="image">
                        <a href="<?= base_url() . $item->photo ?>" class="fancybox" rel="group">
                            <img class="img-polaroid" src="<?= base_url() . $item->photo ?>" width="80" height="80"/>
                        </a>
                    </div>
                    <div class="info">

                        <div class="controls">                                    
                            <!-- <a href="#" class="icon-ok"></a> -->
                            <a href="javascript:void(0)" onclick="removePhoto('<?= $item->id ?>')" class="icon-remove"></a>
                        </div>                                                                    
                    </div>
                </div>

            <?php } ?>

            <script type="text/javascript">
                $(document).ready(function() {
                    $(".scroll").mCustomScrollbar();
                })
            </script>
        <?php
        } else {
            echo '<p>Hiện chưa có hình ảnh cho sản phẩm này</p>';
        }
    }

}

/**End file member.php **/
/* File local: application/controller/admin/member.php */