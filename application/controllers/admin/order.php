<?php
/*
 * Cart class
 * Extends Admin_controller
 * @Author: Nguyen Duc Hung
 * @Date: 28/09/2013
 */

class Order extends admin_controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Order' => URL_ADMIN . 'cart',
        );
        
        $bilings = $this->db->get('order')->result_array();
        $this->_page_header = 'List orders';
        
        $this->template->title()
                ->set('billings', $bilings)
                ->set('url_delMul', 'order/delete/')
                ->build('admin/order/index');
        
    }
    
    public function view() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Order' => URL_ADMIN . 'order',
            'Order detail' => ''
        );
        $id = (int)$this->uri->segment(4);
        $order = $this->db->get_where('order', array('id' => $id))->row_array();
        $order_info = $this->db->get_where('order_info', array('order_id' => $id))->result_array();
        $payer = $this->db->get_where('payer', array('order_id' => $id))->row_array();
        
        $this->template->title()
                ->set(array('order' => $order, 'order_info' => $order_info, 'payer' => $payer))
                ->build('admin/order/view');
    }

    function changeStatusCart() {
        $status = $this->input->post('status');
        $id = $this->input->post('id');

        $this->db->update('billing', array('status' => $status), array('id' => $id));
        echo json_encode(array('status' => TRUE));
    }

    public function delete(){
        $order_id = $this->uri->segment(4);

        $this->db->delete('payer', array('order_id' => $order_id));
        $this->db->delete('order_info', array('order_id' => $order_id));

        if($this->db->delete('order', array('id' => $order_id))) {
            redirect(URL_ADMIN.'order');
        } else {
            redirect(URL_ADMIN.'order');
        }
    }
}