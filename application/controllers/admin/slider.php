<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends admin_controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->input->post()) {
			$arr = array(
				'slider_name'	=> $this->input->post('slider_name')
			);
			$oldid = $this->input->post('old_id');

			if($oldid !== '') {
				if($this->db->update('sliders', $arr, array('id' => $oldid))) {
					redirect(URL_ADMIN .'slider');
				}	
			} else {
				if($this->db->insert('sliders', $arr)) {
					redirect(URL_ADMIN .'slider');
				}	
			}
			
		}


		$this->breadcrumb = array(
            'Admin panel' => '',
            'Sliders' => ''
        );
		$this->_page_header = 'List sliders';

		$sql = $this->db->order_by('id', 'desc')->get('sliders')->result_array();

		$this->template->title($this->_page_header)
			->set('body', $sql)
			->set('link_add', URL_ADMIN.'slider/add')
			->set('link_edit', URL_ADMIN.'slider/edit/')
			->set('link_del', URL_ADMIN.'slider/delete/')
			->set('link_add_item', URL_ADMIN.'slider/view_item/')
			->build('admin/slider/index');	
	}

	public function delete(){
		$sliderid = (int)$this->uri->segment(4);

		$images = $this->db->get_where('slider_gallery', array('slider_id' => $sliderid))->result();

		if(count($images)) {
			foreach($images as $image) {
				@unlink($image->image);
			}
		}
		$this->db->delete('slider_gallery', array('slider_id' => $sliderid));
		if($this->db->delete('sliders', array('id' => $sliderid))) {
			$json = array('status' => TRUE);
		} else {
			$json = array('status' => FALSE);
		}

		echo json_encode($json);
	}

	public function delete_photo(){
		$sliderid = (int)$this->uri->segment(4);

		$images = $this->db->get_where('slider_gallery', array('id' => $sliderid))->row();
		if($images->image !=='') @unlink($images->image);

		if($this->db->delete('slider_gallery', array('id' => $sliderid))) {
			$json = array('status' => TRUE);
		} else {
			$json = array('status' => FALSE);
		}

		echo json_encode($json);
	}

	public function edit(){
		$id = $this->uri->segment(4);
		$info = $this->db->get_where('sliders', array('id' => $id))->row();
		$json = array('sliderid' => $info->id, 'slider_name' => $info->slider_name);
		echo json_encode($json);
	}

	public function view_item(){
		$sliderid = $this->uri->segment(4);
		$gallerys = $this->db->get_where('slider_gallery', array('slider_id' => $sliderid))->result_array();
		$sql = $this->db->select('slider_name')->get_where('sliders', array('id' => $sliderid))->row();
		//If submit has been press then upload photo and store
		if($this->input->post()) {
			$files = $_FILES;
	        $config['upload_path'] = './uploads/slider/';
	        $config['allowed_types'] = 'gif|jpg|png';
	        $config['remove_spaces'] = TRUE;
	        $this->load->library('upload', $config);

	        $i = 1;
	        foreach ($_FILES as $field => $value) {
	            if ($this->upload->do_upload($field)) {
	                $up = $this->upload->data();
	                $photo = 'uploads/slider/'. $up['file_name'];
	                $imageWidth = $up['image_width'];
	                if ($imageWidth > 304) {
	                    $this->load->library('image_lib');
	                    $config['image_library'] = 'GD2';
	                    $config['source_image'] = $up['full_path'];
	                    $config['quality'] = 75;
	                    $config['maintain_ratio'] = TRUE;
	                    $config['master_dim'] = 'height';
	                    $config['width'] = 304;
	                    $config['height'] = 300;
	                    $this->image_lib->initialize($config);
	                    $this->image_lib->resize();
	                }
	            } else {
	                $photo = "";
	            }

	            if($photo !=='') {
	            	$this->db->insert('slider_gallery', array('image' => $photo, 'slider_id' => $sliderid));
	            }
	            

	            $i++;
	        }

	        redirect(URL_ADMIN .'slider/view_item/' . $sliderid);
		}


		$this->_page_header = 'List photos of '. $sql->slider_name;
		$this->breadcrumb = array(
            'Dashboard' => '',
            'Sliders' => URL_ADMIN .'slider',
            $sql->slider_name		=>''
        );
		$this->template->title($this->_page_header)
			->set('body', $gallerys)
			->set('link_add', URL_ADMIN.'slider/add')
			->set('link_edit', URL_ADMIN.'slider/edit/')
			->set('link_del', URL_ADMIN.'slider/delete/')
			->set('link_add_item', URL_ADMIN.'slider/view_item/')
			->build('admin/slider/view_item');
	}

}

/* End of file slider.php */
/* Location: ./application/controllers/admin/slider.php */