<?php

class Home extends admin_controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->breadcrumb = array(
            'Admin panel' => '',
            'Dashboard' => URL_ADMIN,
        );

        //$newest_members = $this->db->limit(10)->order_by('mid', 'desc')->get('member')->result_array();
        $bilings = $this->db->limit(10)->order_by('id', 'desc')->get('billing')->result_array();
        
        $this->template->title('Welcome to Admin Console', $this->config->item('site_cms'))
                ->set('billings', $bilings)
                ->build('admin/admin_home');
    }

    public function s_lang()
    {
        $lang = $this->input->get('l');
        $cur_url = $this->input->get('url');
        if($lang)
        {
            //$this->config->set_item('language', $lang);

            $cookie = array(
                'name'   => 'language_admin',
                'value'  =>  $lang,
                'expire' => '86500000',
                'path'   => $this->config->item('cookie_path'),
                'prefix' => $this->config->item('cookie_prefix'),
            );

            set_cookie($cookie);
        }

        redirect($cur_url);
    }

}
