<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book extends admin_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('book/book_model');
	}

	public function index()
	{
		$this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Books' => URL_ADMIN . 'book',
        );

        $this->_page_header = 'Books';
        
        $this->db->where('books_language.lang_code', $this->admin_lang);
        $this->db->join('books_language', 'books.id = books_language.book_id', 'left');
        $books = $this->db->get('books')->result_array();

        $this->template->title()
                ->set(array('body' => $books))
                ->set('url_add', URL_ADMIN . 'book/add')
                ->set('url_edit', URL_ADMIN . 'book/edit/')
                ->set('url_del', URL_ADMIN . 'book/delete/')
                ->set('url_delMul', URL_ADMIN . 'book/delMulPro')
                ->set('url_change_status', 'book/change_status_pro/')
                ->build('admin/book/index');	
	}

     public function add() {
        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Books' => URL_ADMIN . 'book',
            'Add new' => ''
        );

        if ($_POST) {
            if ($this->book_model->checkCodePro($this->input->post('title'))) {
                $this->session->set_flashdata('error', "This book was already exist");
                redirect(URL_ADMIN . 'book/add');
            } else {

                if ($this->book_model->add($_POST)) {
                    $this->message('success', "New book has been added succussful");
                    redirect(URL_ADMIN . 'book');
                } else {
                    $this->message('error', "Error");
                    redirect(URL_ADMIN . 'book');
                }
            }
        }

        $this->_page_header = 'Create new';

        $this->template->title()
                ->build('admin/book/add');
    }

    function edit(){


        $this->breadcrumb = array(
            'Dashboard' => URL_ADMIN,
            'Books' => URL_ADMIN . 'book',
            'Edit' => ''
        );

        $id = (int) $this->uri->segment(4);

        if ($_POST) {
            if ($this->book_model->edit($_POST, $id)) {
                $this->message('success', "The record has been updated succussful");
                redirect(URL_ADMIN . 'book');
            } else {
                $this->message('error', "Error");
                redirect(URL_ADMIN . 'book');
            }
        }

        $this->_page_header = 'Edit book';
        
        $this->db->where(array('books_language.lang_code' =>$this->admin_lang, 'books.id' => $id));
        $this->db->join('books_language', 'books_language.book_id = books.id', 'left');
        $info = $this->db->get('books')->row_array();

        $this->template->title()
                ->set('info', $info)
                ->build('admin/book/edit');
    }


    public function change_status_pro() {
        $id = (int) $this->uri->segment(4);
        $status = (int) $this->uri->segment(5);
        if ($status == 1) {
            $restatus = 0;
        } else {
            $restatus = 1;
        }

        $this->db->update('books', array('status' => $restatus), array('id' => $id));
        redirect(URL_ADMIN . 'book');
    }

    public function delete() {
        $id = (int) $this->uri->segment(4);
        if ($this->book_model->delete($id)) {
            $info = array('status' => TRUE);
        } else {
            $info = array('status' => FALSE);
        }

        echo json_encode($info);
    }

}

/* End of file book.php */
/* Location: ./application/controllers/admin/book.php */