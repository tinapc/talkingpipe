<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book extends public_controller {
	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->text_for_header = $this->text->get_text('book', 'header', $this->front_lang);
        $this->text_for_footer = $this->text->get_text('book', 'footer', $this->front_lang);

		$this->db->where(array('books_language.lang_code' => $this->front_lang, 'books.status' => 1));
		$this->db->join('books_language', 'books_language.book_id = books.id', 'left');
		$books = $this->db->get('books')->result_array();

		$this->template->title('Books')
			->set('books', $books)
			->build('book/index');	
	}

}

/* End of file book.php */
/* Location: ./application/controllers/book.php */