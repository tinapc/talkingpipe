<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

/**---------------------------
* Talking
* @category		Home controller extends Public_Controller
* @Author		Nguyen Duc Hung - tinapc@gmail.com
* @Date			6/8/2012
* @Version		v1.0
-----------------------------*/


class Home extends public_controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
    	
        $this->text_for_header = $this->text->get_text('home', 'header', $this->front_lang);
        $this->text_for_footer = $this->text->get_text('home', 'footer', $this->front_lang);

        $this->template->title($this->setting->get_setting('site_title'))
            ->set_metadata('description', $this->setting->get_setting('site_desc'))
            ->set_metadata('keywords', $this->setting->get_setting('site_desc'))
    		->build('home');
    }


    public function s_lang()
    {
        $lang = $this->input->get('l');
        $cur_url = $this->input->get('url');
        if($lang)
        {
            $this->config->set_item('language', $lang);

            $cookie = array(
                'name'   => 'language_front',
                'value'  =>  $lang,
                'expire' => '86500000',
                'path'   => $this->config->item('cookie_path'),
                'prefix' => $this->config->item('cookie_prefix'),
            );

            set_cookie($cookie);
        }

        redirect($cur_url);
    }

}
/* End of file home.php */
/* Local: ./application/controllers/home.php */
