<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ajax extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function receive_news(){
        $email = $this->input->post('email');
        $check = $this->db->select('email')->get_where('receive_news', array('email' => $email));
        if($check->num_rows() > 0 ) {
            $json = array('status' => FALSE, 'message' => 'Địa chỉ Email này đã được đăng ký');    
        } else {
            if($this->db->insert('receive_news', array('email' => $email, 'created_date' => time()))) {
                $json = array('status' => TRUE, 'message' => 'Yêu cầu của bạn đã được xử lý');
            } else {
                $json = array('status' => FALSE, 'message' => 'Có lỗi xảy ra');
            }
        }
                
        echo json_encode($json);
    }
}

