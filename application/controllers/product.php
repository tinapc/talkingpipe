<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Product
 * Extends from public_controller
 * Author: Nguyen Duc Hung
 * Email: tinapc@gmail.com
 * */

class Product extends public_controller {

    public function __construct() {
        parent:: __construct();

        $this->load->model('product/product_model');
    }


    public function index() {
        
        $this->text_for_header = $this->text->get_text('empire', 'header', $this->front_lang);
        $this->text_for_footer = $this->text->get_text('empire', 'footer', $this->front_lang);

        $this->db->where('catelog_language.lang_code', $this->front_lang);
        $this->db->where('catelog.status', 1);
        $this->db->join('catelog_language', 'catelog.id = catelog_language.catelog_id', 'left');
        $sql = $this->db->get('catelog')->result_array();

        $this->template->title()
                ->set('catelogs', $sql)
                ->build('product/category');
    }

    public function category($uri) {

        $this->text_for_header = $this->text->get_text('product', 'header', $this->front_lang);
        $this->text_for_footer = $this->text->get_text('product', 'footer', $this->front_lang);

        if(!$uri || !is_numeric($uri)) redirect('category');
        
        $this->db->where('cate_id', (int)$uri);
        $sql = $this->db->get_where('product', array('status' => 1))->result_array();

        $this->template->title($this->product_model->getAttributeCategory('title',$uri))
                ->set('products', $sql)
                ->build('product/product');
    }

    public function detail() {
        $pid = (int) $this->uri->segment(3);
        $info = $this->product_model->read_pro($pid);
        $colors = $this->db->get_where('product_color', array('pid' => $pid))->result_array();
        
        $photoPro = $this->product_model->getRandomPhoto($info['id']);

        //Related product
        $this->db->where_in('id', explode(',',$info['related_product']));
        $this->db->where('status', 1);
        $relateds = $this->db->get('product')->result_array();

        //Sp mua cung
        $this->db->where_in('id', explode(',',$info['muacung']));
        $this->db->where('status', 1);
        $muacungs = $this->db->get('product')->result_array();

        if ($photoPro != "")
            $imgPro = base_url() . $photoPro;
        else
            $imgPro = $this->no_image;

        $this->_desc_seo_fb = $info['desc_seo'];
        $this->_image_fb = $imgPro;
        $this->product_name_to_modal = $info['title'];
        $this->product_material_to_modal = $info['material'];
        $this->_phone_contact_header = $info['phone_contact'];
        $this->_email_contact_header = $info['email_contact'];
        $this->product_cover_size_to_modal = $info['cover_size'];

        $this->template->title($info['title'])
                ->set(array('infoPro' => $info, 'colors' => $colors, 'onlyPro' => $photoPro, 'relateds' => $relateds, 'muacungs' => $muacungs))
                ->set('mata_description', $info['desc_seo'])
                ->set('mata_keyword', $info['keyword_seo'])
                ->build('product/detail');
    }

    /*     * **************AjAX HANDLE**************************** */

    function load_photo_thumb(){
        $colorid = (int)$this->input->post('colorid');
        $photos = $this->db->get_where('product_photo', array('colorid' => $colorid))->result_array();
        if (count($photos)) {
            foreach ($photos as $photo) { ?>
                <li>
                    <a href="<?php echo base_url() . $photo['photo'] ?>" class="fancybox" rel="gallery1"><img src="<?php echo base_url() . $photo['photo'] ?>" alt="" /></a>
                </li>
            <?php } ?>
            <script type="text/javascript">
                $(function(){
                    var img = $('ul.photo-thumb li:first').html();
                    $('#box-photo-img').html(img);
                });
            </script>

        <?php }
    }

    function get_size_of_color() {
        $colorid = (int) $this->input->post('colorid');
        $sizes = $this->db->get_where('product_size', array('colorid' => $colorid))->result();

        if (count($sizes)) {
            ?>
            <p class="list-a-size" style="border-bottom:1px solid #ccc">
                <label>Size:</label>
                <?php foreach ($sizes as $size) { ?>
                    <?php if ($size->size_35 != 0) : ?>
                        <a href="javascript:void(0)" onclick="getQualityBySize('size_35', '<?= $colorid ?>');
                                $('.list-a-size a.active').removeClass('active');
                                $(this).addClass('active');">35</a>
                    <?php endif ?>
                    <?php if ($size->size_36 != 0) : ?>
                        <a href="javascript:void(0)" onclick="getQualityBySize('size_36', '<?= $colorid ?>');
                                $('.list-a-size a.active').removeClass('active');
                                $(this).addClass('active');">36</a>
                    <?php endif ?>
                    <?php if ($size->size_37 != 0) : ?>
                        <a href="javascript:void(0)" onclick="getQualityBySize('size_37', '<?= $colorid ?>');
                                $('.list-a-size a.active').removeClass('active');
                                $(this).addClass('active');">37</a>
                    <?php endif ?>
                <?php if ($size->size_38 != 0) : ?>
                        <a href="javascript:void(0)" onclick="getQualityBySize('size_38', '<?= $colorid ?>');
                                $('.list-a-size a.active').removeClass('active');
                                $(this).addClass('active');">38</a>
                <?php endif ?>
                <?php if ($size->size_39 != 0) : ?>
                        <a href="javascript:void(0)" onclick="getQualityBySize('size_39', '<?= $colorid ?>');
                                $('.list-a-size a.active').removeClass('active');
                                $(this).addClass('active');">39</a>
                <?php endif ?>
            <?php } ?>
            </p>
        <?php
        }
    }

    function get_size_of_color_2() {
        $colorid = (int) $this->input->post('colorid');
        $size = $this->input->post('size');
        $sizes = $this->db->select($size)->get_where('product_size', array('colorid' => $colorid))->row();
        echo json_encode(array('size' => $sizes->$size));
    }

    public function check_stock() {
        $color = (int) $this->input->post('colorid');
        $size = $this->input->post('size');
        $ql = (int) $this->input->post('ql');

        $sql = $this->db->select($size)->get_where('product_size', array('colorid' => $color))->row();
        if ($sql->$size < $ql) {
            $json = array('status' => TRUE);
        } else {
            $json = array('status' => FALSE);
        }

        echo json_encode($json);
    }

    public function search() {
        $key = $this->input->get('key');
        $this->db->like('title', $key);
        $this->db->or_like('material', $key);
        $this->db->or_like('detail', $key);
        $this->db->where('status', 1);
        $sql = $this->db->get('product')->result_array();

        $this->template->title('Kết quả tìm kiếm')
                ->set('products', $sql)
                ->build('product/search');
    }

}
