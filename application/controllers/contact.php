<?php

/*
 * Class Contact
 * Exentends Public_controller
 */

class Contact extends public_controller {

    function __construct() {
        parent::__construct();

    }

    public function index() {
        $status = FALSE;
        $this->load->library('parser');    

        if ($_POST) {
            $_data = array(
                'name_contact' => $this->input->post('name'),
                'message_contact' => nl2br($this->input->post('message')),
                'email_contact' => $this->input->post('email'),
            );

            $this->db->insert('contact', $_data);
                        
            $this->loadMail();
            $this->email->from($_data['email_contact'], $_data['name_contact']);
            $this->email->to($this->setting->get_setting('email'));
            //$this->email->to($to);
            $this->email->subject('Contact information from TalkingPipe.Com');
            $this->email->message($this->parser->parse('contact/contact_via_email', $_data));
            $this->email->set_newline("\r\n");
            if ($this->email->send()) {
                redirect('contact/thank/?r=1');
            } else {
                redirect('contact/thank/?r=0');
            }
        }

        $this->template->title('Contact')
                ->set('title', 'Contact')
                ->build('contact/index');
    }

    public function thank(){
        $this->template->title('Contact Thank')
                ->build('contact/thank');    
    }
    
    public function get_map(){
        $id = $this->uri->segment(3);
        $address1 = $this->db->get_where('stores', array('id' => $id))->row();
        //print_r($address);
        $ok = urlencode($address1->address);
        //here is the google api url
        
        $data['latlng'] = get_latlng($ok);
        $data['info'] = $address1;
        $this->load->view('map_contact', $data);
        
    }

}

/*
 * End file
 * Local: application/controllers/contact.php
 */
