<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends public_controller {

	public function index()
	{
		$this->text_for_header = $this->text->get_text('blog', 'header', $this->front_lang);
        $this->text_for_footer = $this->text->get_text('blog', 'footer', $this->front_lang);

		$this->db->where(array('blog.status' => 1, 'blog_language.lang_code' => $this->front_lang));
		$this->db->join('blog_language', 'blog.id = blog_language.blog_id', 'left');
		$blogs = $this->db->get('blog')->result_array();

		$this->template->title('Blog')
			->set('blogs', $blogs)
			->build('blog/index');
	}

}

/* End of file blog.php */
/* Location: ./application/controllers/blog.php */