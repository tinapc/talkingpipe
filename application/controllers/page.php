<?php 

/**
* Page class
* Extends public_controller
* Author: Nguyen Duc Hung
*/

class Page extends public_controller
{
	public function __construct(){
		parent:: __construct();
	}

	public function view() {
		$id = $this->uri->rsegment(3);
		
		$this->db->where(array('pages.id' => $id, 'page_language.lang_code' => $this->front_lang));
		$this->db->join('page_language', 'pages.id = page_language.page_id');
		$info = $this->db->get('pages')->row_array();

		$this->template->title($info['page_title'])
			->set('info', $info)
			->build('page/view');
	}
}