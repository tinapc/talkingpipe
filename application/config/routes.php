<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
//$route['404_override'] = 'errors/page_missing';
$route['admin/(dashboard)'] = 'admin/home';
$route['admin'] = 'admin/home';
//$route['admin/([a-zA-Z_-]+)/(:any)'] = '$1/admin/$2';


// Page member
$route['register'] = 'member/register/$1';
$route['login'] = 'member/login/$1';
$route['account'] = 'member/account/$1';
$route['success'] = 'member/register_success';
$route['active/(:any)'] = 'member/active/$1';
$route['logout'] = 'member/logout';

//page service
$route['service/(:num)(:any)'] = 'service/detail/$1';
$route['service-category/(:num)(:any)'] = 'service/category/$1/$2';
$route['service'] = 'service/index/$1';

//page product
$route['category/(:num)(:any)'] = 'product/category/$1';
//$route['category'] = 'product/category';
$route['category'] = 'product/index';


//Page pages
$route['page/(:num)(:any)'] = 'page/view/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */