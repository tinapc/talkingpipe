<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends admin_controller {

	public function __construct(){
		parent::__construct();

		$this->load->helper('file');
		$this->load->model('tool_model');
	}

	public function general_sitemap()
	{
		$dir_file = FCPATH .'sitemap.xml';
		if(!is_file($dir_file)) {
			fopen($dir_file, 'w'); //if this file is not exits then create new with write mod
		}

		if($this->input->post('vl_hidden')) {
		
			//Data to write into xml file
			$data = "<?xml version='1.0' encoding='UTF-8'?> \n";
			$data .= "<urlset \n";
	        $data .= "xmlns='http://www.sitemaps.org/schemas/sitemap/0.9' \n";
	      	$data .= "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' \n";
	        $data .= "xsi:schemaLocation='http://www.sitemaps.org/schemas/sitemap/0.9 \n";
	        $data .= "http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'>\n";

	        $data .= "<url>\n";
	  		$data .="<loc>".base_url()."</loc>\n";
	  		$data .="<lastmod>".date('Y-m-d h:i:s')."</lastmod>\n";
			$data .="</url>\n";
			$data .= "<url>\n";
	  		$data .="<loc>".base_url()."blog</loc>\n";
	  		$data .="<lastmod>".date('Y-m-d h:i:s')."</lastmod>\n";
			$data .="</url>\n";
			$data .= "<url>\n";
	  		$data .="<loc>".base_url()."category</loc>\n";
	  		$data .="<lastmod>".date('Y-m-d h:i:s')."</lastmod>\n";
			$data .="</url>\n";
			$data .= "<url>\n";
	  		$data .="<loc>".base_url()."book</loc>\n";
	  		$data .="<lastmod>".date('Y-m-d h:i:s')."</lastmod>\n";
			$data .="</url>\n";
			$data .= "<url>\n";
	  		$data .="<loc>".base_url()."cart</loc>\n";
	  		$data .="<lastmod>".date('Y-m-d h:i:s')."</lastmod>\n";
			$data .="</url>\n";

			//gernerl category
			$cats = $this->tool_model->get_category();
			if(count($cats)) {
				foreach($cats as $cat) {
					$data .= "<url>\n";
			  		$data .="<loc>".base_url()."category/".$cat->id."-".$cat->alias."</loc>\n";
			  		$data .="<lastmod>".date('Y-m-d h:i:s')."</lastmod>\n";
					$data .="</url>\n";	
				}
			}

			//General pages
			$pages = $this->tool_model->get_pages();
			if(count($pages)) {
				foreach ($pages as $page) {
					$data .= "<url>\n";
			  		$data .="<loc>".base_url()."page/".$page->id."-".$page->page_title_alias."</loc>\n";
			  		$data .="<lastmod>".date('Y-m-d h:i:s')."</lastmod>\n";
					$data .="</url>\n";
				}
			}

	        $data .= "</urlset>";

			if(!write_file($dir_file, $data)) {

			}
		}

		$filecontent = read_file($dir_file);
		$this->_page_header = 'Tool - General sitemap';

		$this->template->title()
				->set('file_content', $filecontent)
                ->build('general_sitemap');
	}

	/*
	|----------------------------------------------
	| BACKUP DATABASE FUNCTION
	|----------------------------------------------
	*/
	public function backup(){

		if($this->input->post()) {
			if($this->input->post('file_name') == '') {
				$filename = 'my_backup';
			} else {
				$filename = $this->input->post('file_name');
			}

			// Load the DB utility class
			$this->load->dbutil();

			// Backup your entire database and assign it to a variable
			$prefs = array(
                'tables'      => array(),  // Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'gzip',             // gzip, zip, txt
                'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );

			$backup =& $this->dbutil->backup($prefs);

			// Load the file helper and write the file to your server
			$this->load->helper('file');
			
			$path_file = FCPATH . 'backupdatabase/';
			if(!is_dir($path_file)) {
				mkdir($path_file, '0777');
			}
			$file = $filename.'_'.time().'.sql.gz';

			write_file($path_file . $file, $backup);

			// Load the download helper and send the file to your desktop
			$this->load->helper('download');
			force_download($file, $backup); 
		}

		$this->_page_header = 'Backup database';

		$this->template->title()
                ->build('backup_db');
	}


}

/* End of file tool.php */
/* Location: ./application/modules/tool/controllers/tool.php */