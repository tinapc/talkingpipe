<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tool_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	// Get category
	function get_category(){
		$sql = $this->db->get('catelog');
		return $sql->result();
		$sql->free_result();
	}

	// Get pages
	function get_pages(){
		$this->db->where(array('page_language.lang_code' => 'en'));
		$this->db->select('pages.id, page_language.page_title_alias');
		$this->db->join('page_language', 'pages.id = page_language.page_id', 'left');
		$sql = $this->db->get('pages')->result();
		return $sql;

		$sql->free_result();
	}	

}

/* End of file tool_model.php */
/* Location: ./application/modules/tool/models/tool_model.php */