<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
		<div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">
                	<input type="hidden" value="action_frm" name="vl_hidden">
                    <div class="row-form clearfix">
                    	<div class="span2">Backup file name:</div>
                        <div class="span10">
                        	<input name="file_name">
                        </div>
                    </div>                              
                    <div class="footer tar" style="text-align:left">
                        <button class="btn" onclick="$('#frm_add').submit()">Backup</button>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>
