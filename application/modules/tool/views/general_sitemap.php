<div class="workplace"> 
    <?php echo $template['partials']['loading_ajax']; ?>
    <?php echo $template['partials']['note'];?>

    <div class="page-header">
        <h1><?=$this->_page_header?></h1>
    </div> 

    <div class="row-fluid">
		<div class="span12">
            <div class="head clearfix">
                <div class="isw-documents"></div>
                <h1><?=$this->_page_header?></h1>
            </div>

            <div class="block-fluid">                        
                <form id="validation" method="POST" id="frm_add" enctype="multipart/form-data">
                	<input type="hidden" value="action_frm" name="vl_hidden">
                    <div class="row-form clearfix">
                        <div class="span12">
                        	<div style="max-height:300px; overflow:auto"><?=highlight_code($file_content)?></div>
                        </div>
                    </div>                              
                    <div class="footer tar" style="text-align:left">
                        <button class="btn" onclick="$('#frm_add').submit()">Update sitemap</button>
                        <a class="btn" href="<?=base_url()?>sitemap.xml" target="_blank">View file on the browser</a>
                    </div>                 
                                
                </form>
            </div>

        </div>
    </div>

</div>
